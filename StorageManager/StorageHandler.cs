﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Web;

namespace StorageManager
{
    public static class StorageHandler
    {
        private static CloudStorageAccount storageAccount = CloudStorageAccount.Parse(StorageConfiguration.ConnectionString);
        private static CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();


        public static bool TryUploadFileAsBytes(string containerName, byte[] pdfBytes, string fileName, string contentType, out string path)
        {
            CloudBlockBlob blob = GetBlockBlob(containerName, fileName, contentType);
            blob.UploadFromByteArray(pdfBytes, 0, pdfBytes.Length);
            path = blob.Uri.ToString();
            return !string.IsNullOrEmpty(path);
        }

        public static bool TryUploadFile(string containerName, string filePath, string fileName, string contentType, out string path)
        {
            CloudBlockBlob blob = GetBlockBlob(containerName, fileName, contentType);
            blob.UploadFromFile(filePath, FileMode.Open);
            path = blob.Uri.ToString();
            return !string.IsNullOrEmpty(path);
        }

        public static bool TryUploadImage(string containerName, HttpPostedFileBase photo, string imageName, out string path)
        {
            CloudBlockBlob blob = GetBlockBlob(containerName, imageName, photo.ContentType);
            photo.InputStream.Seek(0, SeekOrigin.Begin);
            blob.UploadFromStream(photo.InputStream);
            path = blob.Uri.ToString();
            return !string.IsNullOrEmpty(path);
        }

        private static CloudBlockBlob GetBlockBlob(string containerName, string fileName, string contentType)
        {
            CloudBlobContainer container = blobStorage.GetContainerReference(containerName);
            if (container.CreateIfNotExists())
            {
                // configure container for public access
                var permissions = container.GetPermissions();
                permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                container.SetPermissions(permissions);
            }
            var blob = container.GetBlockBlobReference(fileName);
            blob.Properties.ContentType = contentType;
            return blob;
        }
    }
}
