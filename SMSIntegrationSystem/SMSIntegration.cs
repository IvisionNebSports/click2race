﻿using System;
using System.Net;

namespace SMSIntegrationSystem {
    public class SMSIntegration {

        public const string URL = "http://chotasandesh.com:9123/CsRestApi/gbt/submit-tsms";

        public static Tuple<bool, string> SendSms(String userName, String passWord, String senderId, String msisdn, String msg) {
            try {
                Console.WriteLine(WebUtility.UrlEncode(msg));
                String queryParam = "?username=" + userName +
                                    "&password=" + passWord +
                                    "&from=" + senderId +
                                    "&msisdn=" + msisdn +
                                    "&msg=" + WebUtility.UrlEncode(msg);
                Console.WriteLine(queryParam);
                String response = new System.Net.WebClient().DownloadString(URL + queryParam);
                Console.WriteLine(response);
                return Tuple.Create<bool, string>(true, "Success!");
            } catch (Exception exception) {
                return Tuple.Create<bool, string>(false, exception.Message);
            }
        }


    }
}
