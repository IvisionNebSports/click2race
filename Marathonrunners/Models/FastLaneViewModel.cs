﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Marathonrunners.Validation;

namespace Marathonrunners.Models {

    public class FastLaneViewModel  
    {

        [Required(ErrorMessage = "Please select the Title")]
        [Display(Name = "Title*")]
        public int Title { get; set; }

        [Required(ErrorMessage = "Please enter your First Name")]
        [Display(Name = "First Name*")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your Date Of Birth")]
        [Display(Name = "Date of Birth*")]
        [DisplayFormat(DataFormatString = "{0:yy-mm-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Please enter your Last Name")]
        [Display(Name = "Last Name*")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your Email Address")]
        [Display(Name = "Email Address*")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please enter your Address")]
        [Display(Name = "Address*")]
        public string Address { get; set; }

        [Required(ErrorMessage = "City is required")]
        [Display(Name = "City*")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter the Pin Code")]
        [Display(Name = "Pincode*")]
        [DataType(DataType.PostalCode)]
        public int PostalCode { get; set; }

        [Required(ErrorMessage = "Please enter your Mobile No.")]
        [Display(Name = "Mobile No.*")]
        [DataType(DataType.PhoneNumber)]
        public long MobileNum { get;  set; }

        [Required(ErrorMessage = "Please enter Emergency Contact No.")]
        [Display(Name = "Emergency Contact No.*")]
        
        public long EmergencyContactNumber { get; set; }

        [Required(ErrorMessage = "Please select your T-Shirt Size")]
        [Display(Name = "T-Shirt Size*")]
        public string ShirtSize { get; set; }

        [Required(ErrorMessage = "Event Id missing")]
        public string EventId { get; set; }

        [Display(Name = "Legal*")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "You must agree to the Terms and condition!")]
        public bool AgreesToTerms { get; set; }	

        [Required(ErrorMessage = "Please select the Event Category")]
        [Display(Name = "Event Category*")]
        public string EventCategory { get; set; }

        [Required(ErrorMessage = "Please select your Blood Group")]
        [Display(Name = "Blood Group*")]
        public string BloodGroup { get; set; }

       

        [Required(ErrorMessage = "Please enter your Team Name")] //organization name 
        [Display(Name = "Team Name*")]
        public string OrganizationName { get; set; }

        public bool KeepMePosted { get; set; }

        [Required(ErrorMessage = "Please select Yes/No option")]
        [Display(Name = "Do you suffer from any chronic diseases?")]
        public string ChronicDDropdown { get; set; }
        [Required(ErrorMessage = "Please Enter the disease")]
        [Display(Name = "Name of the disease")]
        public string ChronicDisease { get; set; }

        [Required(ErrorMessage = "Please enter your Identity Proof")]
        [Display(Name = "Identity Proof*")]
        public string IdentityProof { get; set; }

        [Required(ErrorMessage = "Please enter your Id Number")]
       // [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Your Id number seems to be wrong!")]
        [Display(Name = "Id Number*")]
        public string IdNumber { get; set; }
        [Required(ErrorMessage = "Please choose an image to upload")]
        [Display(Name = "Photo Id Proof*")]
        public string PhotoIdProof { get; set; }
    }


    public class DonationViewModel {

        [Required(ErrorMessage = "Please select the Title")]
        [Display(Name = "Title*")]
        public int Title { get; set; }

        [Required(ErrorMessage = "Please enter your First Name")]
        [Display(Name = "First name*")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your Last Name")]
        [Display(Name = "Last name*")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your Email Address")]
        [Display(Name = "Email Address*")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please enter your Address")]
        [Display(Name = "Address*")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please enter your Mobile No.")]
        [Display(Name = "Mobile No.*")]
        [DataType(DataType.PhoneNumber)]
        public long MobileNum { get; set; }

        [Required(ErrorMessage = "Please enter the amount")]
        [Display(Name = "Amount (INR)*")]
        public int Amount { get; set; }


        [Required(ErrorMessage = "Event Name missing")]
        public string Event { get; set; }
    }
}