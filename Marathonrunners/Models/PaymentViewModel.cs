﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;

namespace Marathonrunners.Models {
    public class PaymentViewModel {
        public string EmailId { get; set; }
        public string EventId { get; set; }
        public string EventTitle { get; set; }
        public string EventType { get; set; }
        public string Amount { get; set; }
        public DateTime EventDate { get; set; }
        public bool Student { get; set; }
        public int Donation { get; set; }
        public string DonationTo { get; set; }
        public string PromoCode { get; set; }
        public string SubCategory { get; set; }
        public string FedDetails { get; set; }

    }


    public class RandonneurPaymentViewModel : PaymentViewModel {
        public string RaceNumber { get; set; }
    }

    public class ContourPaymentViewModel : PaymentViewModel {
        public string BibCollectionPlace { get; set; }
    }
    public class RFLPaymentViewModel {
        public string EventId { get; set; }
        public string EventTitle { get; set; }
        public string EventType { get; set; }
        public string Amount { get; set; }
        public DateTime EventDate { get; set; }
        //RFL Specific
        public int EventCost { get; set; }
        public bool Premium { get; set; }
        public string PremiumEmail { get; set; }
        public int? Tshirt { get; set; }
        public int? Tpbglr { get; set; }
        public int? Tpmys { get; set; }
    }

    public class TransactionViewModel {
        public string EventId { get; set; }
        public DateTime EventDate { get; set; }
        public string Key { get; set; }
        public string TxnId { get; set; }
        public string Amount { get; set; }
        public string ProductInfo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string EmailId { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        public string Country { get; set; }
        public long Phone { get; set; }
        public string Surl { get; set; }
        public string Curl { get; set; }
        public object Furl { get; set; }
        public string Hash { get; set; }
        public string ServiceProvider { get; set; }

    }

}