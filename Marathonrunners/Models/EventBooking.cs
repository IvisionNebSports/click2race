﻿using System.Collections.Generic;
using Marathonrunners.Core.Entities;
using Marathonrunners.Models;

namespace marathonrunners.Models {
    public class EventBooking {
        public PaymentViewModel Event { get; set; }
        public List<UserInfo> Users { get; set; }

        public List<List<AdditionalItemSelection>> AdditionalItems { get; set; }
        public bool IsFastLane { get; set; }
        //public int GroupCount { get; set; }
    }

    public class AdditionalItemSelection {
        public int ItemIndex { get; set; }
        public int Quantity { get; set; }
    }
}
