﻿namespace marathonrunners.Models {
    public enum Fields {
        ShirtSize,
        OrganizationName,
        Donation,
        DateOfBirth,
        MobileNum,
        Address,
        City,
        PostalCode,
        BloodGroup,
        EmergencyContact,
        PromoCode,
        APRSubCategory,
        BibCollection,
        EmergencyContactName,
        NameOnBib,
        ChronicDDropdown,
        title,
        IdentityProof,
        IdNumber,
        PhotoIdProof 
    }
}
