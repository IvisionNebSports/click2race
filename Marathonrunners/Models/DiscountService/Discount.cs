﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace marathonrunners.Models.DiscountService {
    public class Discount {
        public string Name { get; set; }

        public int DiscountTypeId { get; set; }

        public bool UsePercentage { get; set; }

        public double DiscountPercentage { get; set; }

        public double DiscountAmount { get; set; }

        public DateTime? StartDateUtc { get; set; }

        public DateTime? EndDateUtc { get; set; }

        public bool RequiresCouponCode { get; set; }

        public string CouponCode { get; set; }

    }
}