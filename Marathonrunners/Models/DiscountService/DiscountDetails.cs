﻿using System;
using System.Collections.Generic;

namespace marathonrunners.Models.DiscountService {
    public static class DiscountDetails {

        public static IDictionary<string, Discount> discountDetails = new Dictionary<string, Discount>();

        public static Discount GetDiscountDetails(string promoCode) {
            if (string.IsNullOrEmpty(promoCode)) return null;
            if (discountDetails.ContainsKey(promoCode))
                return discountDetails[promoCode];
            var queryRetVal = GetDiscountInstance(promoCode);
            if (queryRetVal != null) {
                discountDetails[promoCode] = queryRetVal;
            }
            return queryRetVal;
        }

        private static Discount GetDiscountInstance(string promoCode) {
            switch (promoCode) {
                case "PEDALX2": {
                        return new Discount() {
                            Name = promoCode,
                            UsePercentage = false,
                            RequiresCouponCode = true,
                            StartDateUtc = new DateTime(2015, 08, 15),
                            EndDateUtc = new DateTime(2015, 09, 15),
                            CouponCode = promoCode,
                            DiscountAmount = 200,
                        };
                    }
                default:
                    return null;
            }
        }
    }
}