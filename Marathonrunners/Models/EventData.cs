﻿using System;
using System.Collections;
using System.Collections.Generic;
using marathonrunners.Models.DiscountService;
using System.Web.Mvc;


namespace marathonrunners.Models {
    //TODO: Map this to the database

    public enum MultipleRegistrationType {
        Allowed,
        SingleEmailIdButDifferentFirstAndLastName,
        SingleEmaild
    }

    public class EventCategory {
        public string EventTitle { get; set; }
        public double Price { get; set; }
        public int NumberOfRegistrtionsAllowed { get; set; } //-1 in case of number is defined by some drop down
        public IList<Fields> HiddenFields { get; set; }
        public IList<Discount> Discount { get; set; }
        public bool IsFreeEvent { get; set; }
        public bool PricePerUser { get; set; }

        public IEnumerable<AdditionalItem> AdditionalItems { get; set; }
        public bool NumberOfUserSelectionDisabled { get; internal set; }
    }

    public class EventDetails {
        public IList<EventCategory> EventCategoryList { get; set; }
        public string EventTitle { get; set; }
        public string DataOfEvent { get; set; } //TODO: Consider usign DateTime?
        public string EventId { get; set; }
        public string Location { get; set; }
        public bool IsFastLaneActive { get; set; }
        public MultipleRegistrationType MultipleRegistrationEnroll { get; set; }
        public bool IsFreeEvent { get; set; }
        public MultipleRegistrationType MultipleRegistrationFastLane { get; set; }
        public bool Student { get; set; }
        public bool IsDonationEvent { get; set; }
        public int MinDonationAmount { get; set; }
        public int MaxDonationAmount { get; set; }
        public string DonationTo { get; set; }
        public string CustomEmailSendingAddress { get; set; }
        public IEnumerable<BibCollectionLocation> BibCollectionLocations { get; set; }
        public bool AcknowledgementMail { get; set; }
        public string AcknowledgementMailId { get; set; }
    }

    public class AdditionalItem {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }
        public int MaxQuantity { get; set; }
    }

    public class BibCollectionLocation {
        public string Location { get; set; }
        public string FullAddress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
    }

    public static class EventData {
        public static EventDetails GetEventDetails(string eventId) {
            switch (eventId)
            {

                case "IDBI_Training_Run2":
                    #region IDBI Adidas Taining Run 2 
                    return new EventDetails
                    {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "12K Run", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.ShirtSize, Fields.IdentityProof,Fields.IdNumber,Fields.PhotoIdProof, Fields.PostalCode,Fields.City,Fields.IdNumber, Fields.BloodGroup, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName}
                            }
                        },
                        EventTitle = "IDBI Federal Life Insurance Mumbai Half Marathon 2017 - Adidas Training Run - 2 ",
                        DataOfEvent = "28-05-2017",
                        EventId = eventId,
                        MultipleRegistrationFastLane = MultipleRegistrationType.SingleEmaild,
                        //Location = "Chembur",
                        IsFastLaneActive = true,
                    };
                    #endregion IDBI Adidas Training Run 2 


                #region SPBM Adidas Promo Run 2017
                case "SPBM_Promo_Run":
                                        
                    return new EventDetails
                    {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "SPBM 5K", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PhotoIdProof, Fields.IdentityProof, Fields.IdNumber,Fields.PostalCode,Fields.City, Fields.BloodGroup, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName,Fields.ShirtSize}
                            },
                             new EventCategory
                            {
                                EventTitle = "SPBM 10K", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PhotoIdProof, Fields.IdentityProof, Fields.IdNumber,Fields.PostalCode,Fields.City, Fields.BloodGroup, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName,Fields.ShirtSize}
                            }
                        },
                        EventTitle = "Shriram Properties Bengaluru Marathon - Adidas Training Run 1 ",
                        DataOfEvent = "03-06-2017",
                        EventId = eventId,
                        MultipleRegistrationFastLane = MultipleRegistrationType.SingleEmaild,
                        Location = "Cubbon Park",
                        IsFastLaneActive = true,
                    };
                #endregion SPBM Adidas Promo Run 2017


                case "SPBM_Luxor2017":
                    #region SPBM Luxor
                    return new EventDetails
                    {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Luxor-5K Run", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber, Fields.ShirtSize, Fields.PostalCode,Fields.City, Fields.BloodGroup, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName}
                            },
                             new EventCategory
                            {
                                EventTitle = "Luxor-10K Run", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber,Fields.ShirtSize, Fields.PostalCode,Fields.City, Fields.BloodGroup, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName}
                            }
                        },
                        EventTitle = "Shriram Properties Bengaluru Marathon - Run in Shriram Luxor ",
                        DataOfEvent = "28-05-2017",
                        EventId = eventId,
                        MultipleRegistrationFastLane = MultipleRegistrationType.SingleEmaild,
                        Location = "Hennur Main Road, Chikkagubbi, Beside United International School, Bengaluru",
                        IsFastLaneActive = true,
                    };
                    #endregion SPBM Luxor





                case "Hyd_StadRun2017":
                                        #region "HUM 12 Hour Stadium Run."
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Team Relay",
                                //IsFreeEvent = true,
                                Price = 3000,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber,Fields.City, Fields.PostalCode, Fields.Donation, Fields.title, Fields.ShirtSize,Fields.DateOfBirth, Fields.PromoCode, Fields.BibCollection,Fields.BloodGroup,Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown}
                            }
                        },
                        EventTitle = "Hyderabad 12 Hours Stadium Run ",
                        DataOfEvent = "16 July, 2017",  
                        EventId = eventId,
                        Location = "Gachibowli Stadium ",
                        IsFastLaneActive = true,


                    };
                #endregion HUM 12 Hour Stadium Run.


                case "HUM5K_2017":
                    #region Hyderabad Ultra Marathon
                    return new EventDetails
                    {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "5K Run", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.IdNumber,Fields.PostalCode,Fields.City, Fields.BloodGroup, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName}
                            }
                        },
                        EventTitle = "Hyderabad Ultra Marathon Launch",
                        DataOfEvent = "14-05-2017",
                        EventId = eventId,
                        MultipleRegistrationFastLane = MultipleRegistrationType.SingleEmaild,
                        IsFastLaneActive = true,
                    };
                    #endregion Hyderabad Ultra Marathon


                case "IDBILIMHM_Training_Run":
                    #region IDBI Adidas Taining Run 2017
                    return new EventDetails
                    {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Adidas Training Run - 1", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PostalCode,Fields.City,Fields.IdNumber, Fields.BloodGroup, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName}
                            }
                        },
                        EventTitle = "IDBI Federal Life Insurance Mumbai Half Marathon ",
                        DataOfEvent = "14-05-2017",
                        EventId = eventId,
                        MultipleRegistrationFastLane = MultipleRegistrationType.SingleEmaild,
                        Location = "Chembur",
                        IsFastLaneActive = true,
                    };
                    #endregion IDBI Adidas Training Run 2017


                case "IDBI_Promo_Run":
                    #region IDBI Adidas Promo Run 2017
                    return new EventDetails
                    {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Adidas Promo Run 2", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.IdNumber,Fields.PostalCode,Fields.City, Fields.BloodGroup, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName}
                            }
                        },
                        EventTitle = "IDBI Federal Life Insurance Mumbai Half Marathon ",
                        DataOfEvent = "07-05-2017",
                        EventId = eventId,
                        MultipleRegistrationFastLane = MultipleRegistrationType.SingleEmaild,
                        Location = "Pune",
                        IsFastLaneActive = true,
                    };
                    #endregion IDBI Adidas Promo Run 2017


                case "spbm2017":
                    #region SPBM2017
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                         {
                             new EventCategory
                             {
                                 EventTitle = "42K Full Marathon",
                                 Price = 1550,
                                 NumberOfRegistrtionsAllowed = 1,
                                 PricePerUser = true,
                                 HiddenFields = new List<Fields> {Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                             },
                             new EventCategory
                             {
                                 EventTitle = "21K Half Marathon",
                                 Price = 1350,
                                 NumberOfRegistrtionsAllowed = 1,
                                 PricePerUser = true,
                                 HiddenFields = new List<Fields> {Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                             },
                             new EventCategory
                             {
                                 EventTitle = "5K Run",
                                 Price = 650,
                                 NumberOfRegistrtionsAllowed = 1,
                                 PricePerUser = true,
                                 HiddenFields = new List<Fields> {Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                             }
                         },
                        EventTitle = "Shriram Properties Bengaluru Marathon",
                        DataOfEvent = "15-10-2017",
                        EventId = eventId,
                        //Location = "Sree Kanteerava Stadium",
                        MultipleRegistrationFastLane = MultipleRegistrationType.Allowed,
                        IsFastLaneActive = true,
                    };
                    #endregion SPBM2017

                case "spbm_stad_run17":
                    #region "spbm 48 Hour Stadium Run."
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Team Relay",
                                //IsFreeEvent = true,
                                Price = 3000,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> { Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber,Fields.Donation, Fields.title, Fields.ShirtSize,Fields.DateOfBirth, Fields.PromoCode, Fields.BibCollection,Fields.BloodGroup,Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown}
                            },
                            new EventCategory
                            {
                                EventTitle = "12 Hour Individual",
                                //IsFreeEvent = true,
                                Price =2000,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber,Fields.Donation, Fields.title, Fields.ShirtSize,Fields.DateOfBirth, Fields.PromoCode, Fields.BibCollection,Fields.BloodGroup,Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName}
                            }
                        },
                        EventTitle = "48 Hours Bengaluru Stadium Run ",
                        DataOfEvent = "28-07-2017", //29th July 2017 to 30th July 2017.
                        EventId = eventId,
                        Location = "Sri Kanteerava Stadium, Bengaluru ",
                        IsFastLaneActive = true,


                    };
                #endregion spbm 48 Hour Stadium Run.
                case "idbimhm_2017":
                    #region IDBI Mumbai half marathon
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                         {
                             new EventCategory
                             {
                                 EventTitle = "21K Half Marathon",
                                 Price = 1350,
                                 NumberOfRegistrtionsAllowed = 1,
                                 PricePerUser = true,
                                 HiddenFields = new List<Fields> {Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                             },
                             new EventCategory
                             {
                                 EventTitle = "10K Run",
                                 Price = 950,
                                 NumberOfRegistrtionsAllowed = 1,
                                 PricePerUser = true,
                                 HiddenFields = new List<Fields> {Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                             },
                             new EventCategory
                             {
                                 EventTitle = "5K Run",
                                 Price = 650,
                                 NumberOfRegistrtionsAllowed = 1,
                                 PricePerUser = true,
                                 HiddenFields = new List<Fields> {Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                             }
                         },
                        EventTitle = "IDBI Federal Life Insurance Mumbai Half Marathon",
                        DataOfEvent = "20-08-2017",
                        EventId = eventId,
                        Location = "Oval Stadium",
                        MultipleRegistrationFastLane = MultipleRegistrationType.Allowed,
                        IsFastLaneActive = true,
                    };
                #endregion IDBI Mumbai half marathon 
                case "SPBM_2017":
                    #region Shriram Properties Bengaluru Marathon 2017
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "SPBM 2017 Inaugural Run", // Keep this as unique
                                IsFreeEvent = true,
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.BibCollection,Fields.BloodGroup,Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown,Fields.OrganizationName}
                            }
                        },
                        EventTitle = "Shriram Properties Bengaluru Marathon",
                        DataOfEvent = "23-04-2017",
                        EventId = eventId,
                        MultipleRegistrationFastLane = MultipleRegistrationType.SingleEmaild,
                        //Location = "yet to confirm",
                        IsFastLaneActive = true,
                    };
                #endregion Shriram Properties Bengaluru Marathon 2017
                case "mumb_reg17":
                    #region Mumbai registration
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Team Relay",
                               // IsFreeEvent = true,
                                Price = 3000,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber,Fields.title, Fields.Donation, Fields.PromoCode, Fields.BibCollection, Fields.ShirtSize,Fields.BloodGroup,Fields.DateOfBirth,Fields.EmergencyContact,Fields.EmergencyContactName,Fields.NameOnBib,Fields.Address,Fields.ChronicDDropdown}
                            }
                        },
                        EventTitle = "Mumbai Stadium Run",
                        DataOfEvent = "11-06-2017",
                        EventId = eventId,

                        IsFastLaneActive = true,
                    };
                #endregion Mumbai registration
                case "b10k_2017":
                    #region Bengaluru 10k Challenge 2017
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "10K",
                                //IsFreeEvent = true,
                                Price =850, //early bird closed
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                            },
                            new EventCategory
                            {
                                EventTitle = "5K",
                                Price = 650,
                                NumberOfRegistrtionsAllowed = 1,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.PhotoIdProof,Fields.IdentityProof,Fields.IdNumber,Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection }
                            }
                        },
                        EventTitle = "Bengaluru 10k Challenge",
                        DataOfEvent = "09-07-2017",
                        EventId = eventId,
                        Location = "yet to confirm",
                        IsFastLaneActive = true,
                        CustomEmailSendingAddress = "Team SPBM <team@bengalurumarathon>"
                    };
                #endregion Bengaluru 10k Challenge 2017
                case "sfa_2017":
                    #region STRIDES FOR AUTISM 2017
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "10K",
                                //IsFreeEvent = true,
                                Price =800,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                            },
                            new EventCategory
                            {
                                EventTitle = "5K",
                                Price = 600,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection }
                            }
                        },
                        EventTitle = "STRIDES FOR AUTISM",
                        DataOfEvent = "02-04-2017",
                        EventId = eventId,
                        Location = "Cubbon park,Assembly at Bandstand ,Bangalore",
                        IsFastLaneActive = true,
                    };
                #endregion STRIDES FOR AUTISM 2017
                case "nebrun_2017":
                    #region NEBTestRun

                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "10 KM Run",
                                Price = 10,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields> {Fields.OrganizationName, Fields.Donation, Fields.PromoCode, Fields.BibCollection}
                            },
                            new EventCategory
                            {
                                EventTitle = "5 KM Hope Run",
                                Price = 5,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields> {Fields.OrganizationName, Fields.Donation, Fields.PromoCode,Fields.BibCollection}
                            }
                        },
                        EventTitle = "NEB Test RUN",
                        DataOfEvent = "09-08-2017",
                        EventId = "nebrun_2017",
                        Location = "Koramangala Indoor Stadium, Bengaluru, Karnataka",
                        IsFastLaneActive = true
                    };

                #endregion
                case "tc_2017":
                    #region Trivandrum Cyclathon 2017
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                                 {
                            new EventCategory
                            {
                                EventTitle = "Champions Ride 40 km",
                                Price = 1000,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.OrganizationName, Fields.PromoCode, Fields.ShirtSize, Fields.BibCollection, Fields.NameOnBib},
                            },
                             new EventCategory
                            {
                                EventTitle = "Trivandrum Fitness Ride 14 km",
                                Price = 500,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.ShirtSize, Fields.BibCollection, Fields.NameOnBib }
                            },
                             new EventCategory
                            {
                                EventTitle = "Stark Kids Fun Ride 2.5 km",
                                Price = 250,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.ShirtSize, Fields.BibCollection, Fields.NameOnBib }
                            }
                        },
                        EventTitle = "Trivandrum Cyclathon 2017",
                        DataOfEvent = "08-04-2017",
                        EventId = eventId,
                        Location = "Manaveeyam Veedhi, Trivandrum",
                        IsFastLaneActive = true,
                        MultipleRegistrationFastLane =
                                     MultipleRegistrationType.SingleEmailIdButDifferentFirstAndLastName,
                        MultipleRegistrationEnroll = MultipleRegistrationType.SingleEmaild,
                    };
                #endregion Trivandrum Cyclathon 2017
                case "psnrun_2015":
                    #region PSSNRUN2015

                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "10 KM Charity Run",
                                Price = 400,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields> {Fields.OrganizationName, Fields.Donation, Fields.PromoCode}
                            },
                            new EventCategory
                            {
                                EventTitle = "5 KM Charity Run",
                                Price = 300,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields> {Fields.OrganizationName, Fields.Donation, Fields.PromoCode}
                            },
                            new EventCategory
                            {
                                EventTitle = "2.5 KM Charity Run and Walk",
                                Price = 250,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields> {Fields.OrganizationName, Fields.Donation, Fields.PromoCode}
                            },
                            new EventCategory
                            {
                                EventTitle = "1 KM Fun for Children upto 10 Years",
                                Price = 100,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields> {Fields.OrganizationName, Fields.Donation, Fields.PromoCode}
                            }
                        },
                        EventTitle = "PSN RUN",
                        DataOfEvent = "26-07-2015",
                        EventId = "psnrun_2015",
                        Location = "Prestige Shantiniketan, Whitefield Road, Bengaluru, Karnataka 560048",
                        IsFastLaneActive = true
                    };

                #endregion
                case "tracs_2016_26102016":
                    #region Tracs2016

                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Full Marathon",
                                Price = DateTime.Now > new DateTime(2016, 9, 8) ? 1100 : 1000,
                                PricePerUser = true,
                                NumberOfRegistrtionsAllowed = 5,
                                HiddenFields = new List<Fields> { Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection, Fields.APRSubCategory}
                            },
                            new EventCategory
                            {
                                EventTitle = "Half Marathon",
                                Price = DateTime.Now > new DateTime(2016, 9, 8) ? 1000 : 900,
                                PricePerUser = true,
                                NumberOfRegistrtionsAllowed = 5,
                                HiddenFields = new List<Fields> { Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection, Fields.APRSubCategory}
                            },
                            new EventCategory
                            {
                                EventTitle = "10K",
                                Price = DateTime.Now > new DateTime(2016, 9, 8) ? 900 : 800,
                                PricePerUser = true,
                                NumberOfRegistrtionsAllowed = 5,
                                HiddenFields = new List<Fields> { Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection, Fields.APRSubCategory}
                            }
                        },
                        EventTitle = "Trivandrum Marathon 2016",
                        DataOfEvent = "30-10-2016",
                        EventId = eventId,
                        Location = "Trivandrum",
                        IsFastLaneActive = true,
                        AcknowledgementMail = true,
                        AcknowledgementMailId = "reg.tracs@gmail.com"
                    };
                #endregion
                case "bull_2015":
                    #region Bull2015
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                                   {
                            new EventCategory
                            {
                                EventTitle = "16 KM",
                                Price = 900,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode}
                            },
                            new EventCategory
                            {
                                EventTitle = "8 KM",
                                Price = 600,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode}
                            }
                        },
                        EventTitle = "The Bull Run",
                        DataOfEvent = "23-08-2015",
                        EventId = "bull_2015",
                        Location = "Nandi Hills Base, Karahalli Cross",
                        IsFastLaneActive = true
                    };
                #endregion
                case "run_2015":
                    return new EventDetails() {
                        EventCategoryList = new List<EventCategory>()
                        { new EventCategory(){EventTitle = "5 KM- Children",Price = 100, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode, Fields.OrganizationName}},
                            new EventCategory(){EventTitle = "5 KM- Adult",Price = 300, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                            new EventCategory(){EventTitle = "10 KM- Children",Price = 100, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode, Fields.OrganizationName}},
                            new EventCategory(){EventTitle = "10 KM- Adult",Price = 300, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                        },
                        EventTitle = "The Run 4 Life",
                        DataOfEvent = "08-11-2015",
                        EventId = "run_2015",
                        Location = "Durbar Hall Ground, Ernakulam",
                        IsFastLaneActive = true
                    };
                case "spice_2015":
                    #region Spice 2015
                    return new EventDetails() {
                        EventCategoryList = new List<EventCategory>()
                        //{ new EventCategory(){EventTitle = "Full Marathon(42.195km)",Price = 1100, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                        //  new EventCategory(){EventTitle = "Half Marathon(21.1km)",Price = 900, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                          { new EventCategory(){EventTitle = "Family Fun Run(5km)",Price = 500, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode, Fields.OrganizationName}},
                         //   new EventCategory(){EventTitle = "Federal Bank Family Run (5Km)",Price = 250, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode, Fields.OrganizationName}},
                        },
                        EventTitle = "Spice Coast Marathon",
                        DataOfEvent = "15-11-2015",
                        EventId = "spice_2015",
                        Location = "Cochin",
                        IsFastLaneActive = true
                    };
                #endregion
                case "spice_fed_2015":
                    #region Spice Federal 2015
                    return new EventDetails() {
                        EventCategoryList = new List<EventCategory>()
                           //{ new EventCategory(){EventTitle = "Full Marathon(42.195km)",Price = 1100, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                           //  new EventCategory(){EventTitle = "Half Marathon(21.1km)",Price = 900, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                           // { new EventCategory(){EventTitle = "Family Fun Run(5km)",Price = 500, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode, Fields.OrganizationName}},
                           { new EventCategory(){EventTitle = "Federal Bank Family Run (5Km)",Price = 250, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode, Fields.OrganizationName}},
                        },
                        EventTitle = "Spice Coast Marathon",
                        DataOfEvent = "15-11-2015",
                        EventId = "spice_fed_2015",
                        Location = "Cochin",
                        IsFastLaneActive = true
                    };
                #endregion
                case "mysore_2015":
                    return new EventDetails() {
                        EventCategoryList = new List<EventCategory>()
                        { new EventCategory(){EventTitle = "Challenge 21k",Price = 900, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                          new EventCategory(){EventTitle = "Tough 10k",Price = 800, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                          new EventCategory(){EventTitle = "Easy 5k",Price = 500, NumberOfRegistrtionsAllowed=1,HiddenFields = new List<Fields>(){Fields.Donation, Fields.PromoCode}},
                        },
                        EventTitle = "Chamundi Hill Challenge",
                        DataOfEvent = "22-11-2015",
                        EventId = "mysore_2015",
                        Location = "Mysore",
                        IsFastLaneActive = true
                    };
                case "coorg1_2015":
                    #region Coorg12015
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                                    {
                            new EventCategory
                            {
                                EventTitle = "Drive Only",
                                Price = 1000,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields>
                                    {
                                        Fields.ShirtSize,
                                        Fields.OrganizationName,
                                        Fields.Donation,
                                        Fields.PromoCode
                                    }
                            },
                            new EventCategory
                            {
                                EventTitle = "Club Package",
                                Price = 4999,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields>
                                    {
                                        Fields.ShirtSize,
                                        Fields.OrganizationName,
                                        Fields.Donation,
                                        Fields.PromoCode
                                    }
                            },
                            new EventCategory
                            {
                                EventTitle = "Premium Package",
                                Price = 9999,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields>
                                    {
                                        Fields.ShirtSize,
                                        Fields.OrganizationName,
                                        Fields.Donation,
                                        Fields.PromoCode
                                    }
                            }
                        },
                        EventTitle = "4X4 Offroader Workshop",
                        DataOfEvent = "24-07-2015",
                        EventId = "coorg1_2015",
                        Location = " COORG ADVENTURE CLUB, VIRAJPET",
                        IsFastLaneActive = true
                    };
                #endregion
                case "pedal_2015":
                    #region Pedal2015
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                                    {
                            new EventCategory
                            {
                                EventTitle = "Team Race",
                                Price = DateTime.Now > new DateTime(2015, 8, 11) ? 1000 : 800,
                                NumberOfRegistrtionsAllowed = 2,
                                HiddenFields = new List<Fields> {Fields.Donation},
                                Discount = GeneratePromoCode("PEDALX2")
                            },
                            new EventCategory
                            {
                                EventTitle = "Fun Ride",
                                Price = 200,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.ShirtSize, Fields.Donation, Fields.PromoCode}
                            }
                        },
                        EventTitle = "Embassy Pedal for the Planet",
                        DataOfEvent = "23-08-2015",
                        EventId = "pedal_2015",
                        Location = "Embassy Manyata Business Park",
                        IsFastLaneActive = true,
                        MultipleRegistrationFastLane =
                                        MultipleRegistrationType.SingleEmailIdButDifferentFirstAndLastName,
                        MultipleRegistrationEnroll = MultipleRegistrationType.SingleEmaild,
                        CustomEmailSendingAddress = "Embassy Pedal for the Planet <pedalfortheplanet@embassyindia.com>"
                    };
                #endregion
                case "pedal_cxo_2015":
                    #region PedalCXO2015
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                {
                            new EventCategory
                            {
                                EventTitle = "CXO Race",
                                Price = DateTime.Now > new DateTime(2015, 7, 27) ? 700 : 500,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode}
                            },
                            new EventCategory
                            {
                                EventTitle = "Fun Ride-CXO",
                                Price = 200,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.ShirtSize, Fields.Donation, Fields.PromoCode}
                            }
                        },
                        EventTitle = "Embassy Pedal for the Planet",
                        DataOfEvent = "23-08-2015",
                        EventId = "pedal_cxo_2015",
                        Location = "Embassy Manyata Business Park",
                        IsFastLaneActive = true,
                        MultipleRegistrationFastLane =
                    MultipleRegistrationType.SingleEmailIdButDifferentFirstAndLastName,
                        MultipleRegistrationEnroll = MultipleRegistrationType.SingleEmaild,
                        CustomEmailSendingAddress = "Embassy Pedal for the Planet <pedalfortheplanet@embassyindia.com>"
                    };
                #endregion
                case "bang1_2015":
                    #region Bang12015
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                                    {
                            new EventCategory
                            {
                                EventTitle = "6th Adidas Training Run",
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.ShirtSize, Fields.Donation, Fields.PromoCode}
                            }
                        },
                        EventTitle = "SPBM 2015",
                        DataOfEvent = "13-09-2015",
                        EventId = "bang1_2015",
                        Location = "AOL Campus, Kanakpura Road",
                        IsFastLaneActive = true,
                        IsFreeEvent = true
                    };
                #endregion Bang12015
                case "bang2_2015":
                    return new EventDetails() {
                        EventCategoryList = new List<EventCategory>(){
                            new EventCategory(){
                                EventTitle = "Taper Down with Yoga",
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,HiddenFields = new List<Fields>() { Fields.ShirtSize , Fields.Donation, Fields.PromoCode}
                            }
                        },
                        EventTitle = "SPBM 2015",
                        DataOfEvent = "04-10-2015",
                        EventId = "bang2_2015",
                        Location = "Sree Kanteerava Stadium",
                        IsFastLaneActive = true,
                        IsFreeEvent = true
                    };
                case "training5_2017":
                    return new EventDetails() {
                        EventCategoryList = new List<EventCategory>(){
                            new EventCategory(){
                                EventTitle = "IDBI Federal New Delhi Marathon Adidas Runners X Training Run – 5",
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,HiddenFields = new List<Fields>() { Fields.ShirtSize , Fields.Donation, Fields.PromoCode, Fields.BibCollection}
                            }
                        },
                        EventTitle = "DBI Federal New Delhi Marathon Adidas Runners X Training Run – 5",
                        DataOfEvent = "05-02-2017",
                        EventId = "training5_2017",
                        Location = "Rahgiri",
                        IsFastLaneActive = true,
                        IsFreeEvent = true
                    };


                case "kkh_2015":
                    #region KKH2015
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                                    {
                            new EventCategory
                            {
                                EventTitle = "Donation",
                                Price = 0,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields =
                                    new List<Fields>
                                    {
                                        Fields.ShirtSize,
                                        Fields.Address,
                                        Fields.BloodGroup,
                                        Fields.DateOfBirth,
                                        Fields.EmergencyContact,
                                        Fields.MobileNum,
                                        Fields.OrganizationName,
                                        Fields.PromoCode,
                                        Fields.APRSubCategory,
                                        Fields.BibCollection,
                                        Fields.NameOnBib
                                    }
                            }
                        },
                        EventTitle = "Kaikondrahalli Lake",
                        DataOfEvent = "21-09-2015",
                        EventId = "kkh_2015",
                        Location = "NOT REQUIRED",
                        IsFastLaneActive = true,
                        IsDonationEvent = true,
                        DonationTo = "Kaikondrahalli Lake",
                        MinDonationAmount = 100, // INR
                        MaxDonationAmount = 10000
                    };
                #endregion
                case "apr_2015":
                    #region APR2015
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Individual",
                                Price = 850,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName}
                            },
                            new EventCategory
                            {
                                EventTitle = "Couple",
                                Price = 1450,
                                NumberOfRegistrtionsAllowed = 2,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName}
                            },
                            new EventCategory
                            {
                                EventTitle = "Family",
                                Price = 1950,
                                NumberOfRegistrtionsAllowed = 4,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode,Fields.OrganizationName}
                            }
                        },
                        EventTitle = "APR Marathon",
                        DataOfEvent = "22-11-2015",
                        EventId = eventId,
                        Location = "Adarsh Palm Retreat project office, outer ring road Sarjapur",
                        IsFastLaneActive = true
                    };

                #endregion APR2015
                case "trot_workshop_2016":
                    #region Trot WorkShop 2016
                    return new EventDetails() {
                        EventCategoryList = new List<EventCategory>() { new EventCategory(){
                                EventTitle = "Workshop",
                                Price = 2100,
                                NumberOfRegistrtionsAllowed = 1,HiddenFields = new List<Fields>() {Fields.Donation, Fields.ShirtSize, Fields.EmergencyContact, Fields.PromoCode, Fields.BibCollection }
                            }},
                        EventTitle = "Injury Prevention for Runners 2016",
                        DataOfEvent = "02-07-2016",
                        EventId = eventId,
                        Location = "Hotel Iris, Brigade road, Bangalore.",
                        IsFastLaneActive = true,
                    };
                #endregion Trot WorkShop 2016
                case "tc_2016":
                    #region Trivandrum Cyclathon 2016
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                                 {
                            new EventCategory
                            {
                                EventTitle = "Champions Ride 40 km",
                                Price = 1000,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.OrganizationName, Fields.PromoCode, Fields.ShirtSize, Fields.BibCollection, Fields.NameOnBib},
                            },
                             new EventCategory
                            {
                                EventTitle = "Trivandrum Fitness Ride 14 km",
                                Price = 500,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.ShirtSize, Fields.BibCollection, Fields.NameOnBib }
                            },
                             new EventCategory
                            {
                                EventTitle = "Stark Kids Fun Ride 2.5 km",
                                Price = 250,
                                NumberOfRegistrtionsAllowed = 1,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.ShirtSize, Fields.BibCollection, Fields.NameOnBib }
                            }
                        },
                        EventTitle = "Trivandrum Cyclathon 2016",
                        DataOfEvent = "14-02-2016",
                        EventId = eventId,
                        Location = "Manaveeyam Veedhi, Trivandrum",
                        IsFastLaneActive = true,
                        MultipleRegistrationFastLane =
                                     MultipleRegistrationType.SingleEmailIdButDifferentFirstAndLastName,
                        MultipleRegistrationEnroll = MultipleRegistrationType.SingleEmaild,
                    };
                #endregion Trivandrum Cyclathon 2015
                case "kr_relay_2016":
                    #region Kere Relay 2016
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Adults",
                                Price = 300,
                                NumberOfRegistrtionsAllowed = 3,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                            },
                            new EventCategory
                            {
                                EventTitle = "Family",
                                Price = 300,
                                NumberOfRegistrtionsAllowed = 3,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection }
                            },
                            new EventCategory
                            {
                                EventTitle = "Kids",
                                Price = 300,
                                NumberOfRegistrtionsAllowed = 3,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode,Fields.OrganizationName, Fields.BibCollection }
                            }
                        },
                        EventTitle = "Kere Relay",
                        DataOfEvent = "09-01-2016",
                        EventId = eventId,
                        Location = "Amphitheater, Kaikondrahalli Lake",
                        IsFastLaneActive = true
                    };

                #endregion Kere Relay 2016
                case "cwdr_2016":
                    #region CWDR 2016
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "10KM",
                                Price = 600,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.NameOnBib}
                            },
                            new EventCategory
                            {
                                EventTitle = "5KM",
                                Price = 600,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.NameOnBib }
                            }
                        },
                        EventTitle = "Contours Women Day Run",
                        DataOfEvent = "06-03-2016",
                        EventId = eventId,
                        Location = "Embassy GolfLinks (EGL), Domlur, Bangalore",
                        IsFastLaneActive = true,
                        BibCollectionLocations = new List<BibCollectionLocation>() {
                            new BibCollectionLocation()
                            {
                                Name = "Contorus Women's Fitness Studio",
                                FullAddress = @"No 126, 1st A Cross Rd,
                                                Koramangala 5th Block,
                                                Koramangala, 560095, Bangalore",
                                Location = "Contours Koramangala",
                                Longitude = 77.616625,
                                Latitude = 12.934342,
                                PhoneNumber = "080-42068635"
                            },
                            new BibCollectionLocation()
                            {
                                Name = "Contours Women's Fitness Studio",
                                FullAddress = @"#825, 3rd floor, DAS
                                                Commercial Complex, JSS
                                                Circle, 7th Block Jayanagar, 560082, Bangalore",
                                Location = "Contours Jaynagar",
                                Longitude = 77.576281,
                                Latitude = 12.923868,
                                PhoneNumber = "080-26762008"
                            },
                             new BibCollectionLocation()
                            {
                                Name = "Contours Women's Fitness Studio",
                                FullAddress = @"#804, 27th Main Road, 1st
                                                Sector, HSR Layout,
                                                Above Soch, 560102, Bangalore",
                                Location = "Contours HSR Layout",
                                Longitude = 77.651552,
                                Latitude = 12.916641,
                                PhoneNumber = "080-22585105"
                            },
                              new BibCollectionLocation()
                            {
                                Name = "Contours Women's Fitness Studio",
                                FullAddress = @"#10, Cambridge Road,
                                                Near Frank Anthony Public
                                                School, 560008, Bangalore",
                                Location = "Contours Ulsoor",
                                Longitude = 77.627147,
                                Latitude = 12.972283,
                                PhoneNumber = "080-41736700"
                            },
                                 new BibCollectionLocation()
                            {
                                Name = "Contours Women's Fitness Studio",
                                FullAddress = @"No 363,2nd Floor,Opp
                                                Pride Regalia
                                                Apartments,Hulimavu
                                                Gate,Bannerghatta Road, 560076, Bangalore",
                                Location = "Contours Bannerghatta Road",
                                Longitude = 77.595969,
                                Latitude = 12.879626,
                                PhoneNumber = "080-49597746"
                            },
                                      new BibCollectionLocation()
                            {
                                Name = "Reliance Trends",
                                FullAddress = @"#62, Richmond Rd,
                                                Xavier Layout,
                                                Victoria Layout, 560025, Bangalore",
                                Location = "Reliance Trends Richmond Road",
                                Longitude = 77.6086711,
                                Latitude = 12.9667432,
                                PhoneNumber = "Not available"
                            }
                        }
                    };

                #endregion CWDR 2016 
                case "rfh_2016_closed":
                    #region RFH 2016
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "3K-Fun Run",
                                Price = 300,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection},
                                AdditionalItems = GetRFHAdditionalItems(true)
                            },
                            new EventCategory
                            {
                                EventTitle = "5K-Super Run",
                                Price = 450,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection },
                                AdditionalItems = GetRFHAdditionalItems(true)
                            },
                            new EventCategory
                            {
                                EventTitle = "10K-Challenge",
                                Price = 750,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode,Fields.OrganizationName, Fields.BibCollection },
                                AdditionalItems = GetRFHAdditionalItems()
                            }
                        },
                        EventTitle = "Rupee For Humanity",
                        DataOfEvent = "24-04-2016",
                        EventId = eventId,
                        Location = "Cubbon Park, Bangalore",
                        IsFastLaneActive = true,
                        AcknowledgementMail = true,
                        AcknowledgementMailId = "rupee4humanity@gmail.com"
                    };
                #endregion RFH 2016
                case "trot_2016_closed":
                    #region TROT 2016
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "10Km",
                                Price = 850,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection},
                                AdditionalItems = new List<AdditionalItem>()
                                {
                                    new AdditionalItem() {
                                        Name = "T-Shirt",
                                        Description = "Event T-Shirt",
                                        Cost = 200,
                                        MaxQuantity = 5
                                    }
                                }
                            },
                            new EventCategory
                            {
                                EventTitle = "Half Marathon(21.09Km)",
                                Price = 850,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection },
                               AdditionalItems = new List<AdditionalItem>()
                                {
                                    new AdditionalItem() {
                                        Name = "T-Shirt",
                                        Description = "Event T-Shirt",
                                        Cost = 200,
                                        MaxQuantity = 5
                                    }
                                }
                            },
                            new EventCategory
                            {
                                EventTitle = "Marathon(42.19Km) ",
                                Price = 850,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode,Fields.OrganizationName, Fields.BibCollection },
                               AdditionalItems = new List<AdditionalItem>()
                                {
                                    new AdditionalItem() {
                                        Name = "T-Shirt",
                                        Description = "Event T-Shirt",
                                        Cost = 200,
                                        MaxQuantity = 5
                                    }
                                }
                            }
                        },
                        EventTitle = "The Run Of Raramuri Tribe",
                        DataOfEvent = "30-07-2016",
                        EventId = eventId,
                        Location = "Near KSIT, Kanakapura Road",
                        IsFastLaneActive = true,
                    };
                #endregion TROT 2016
                case "brur_2016_closed":
                    #region BRUR 2016
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "Individual(above 10yrs)",
                                Price = 400,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection, Fields.ShirtSize, Fields.NameOnBib},
                                //AdditionalItems = new List<AdditionalItem>()
                                //{
                                //    new AdditionalItem() {
                                //        Name = "T-Shirt",
                                //        Description = "Event T-Shirt",
                                //        Cost = 400,
                                //        MaxQuantity = 5
                                //    }
                                //}
                            },
                            new EventCategory
                            {
                                EventTitle = "1 Adult + 1 Kid",
                                Price = 400,
                                NumberOfRegistrtionsAllowed = 1,
                                NumberOfUserSelectionDisabled = true,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection, Fields.ShirtSize, Fields.NameOnBib },
                               //AdditionalItems = new List<AdditionalItem>()
                               // {
                               //     new AdditionalItem() {
                               //         Name = "T-Shirt",
                               //         Description = "Event T-Shirt",
                               //         Cost = 400,
                               //         MaxQuantity = 5
                               //     }
                               // }
                            },
                            new EventCategory
                            {
                                EventTitle = "2 Adult + 1 Kid",
                                Price = 400,
                                NumberOfRegistrtionsAllowed = 2,
                                NumberOfUserSelectionDisabled = true,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode,Fields.OrganizationName, Fields.BibCollection, Fields.ShirtSize, Fields.NameOnBib },
                               //AdditionalItems = new List<AdditionalItem>()
                               // {
                               //     new AdditionalItem() {
                               //         Name = "T-Shirt",
                               //         Description = "Event T-Shirt",
                               //         Cost = 200,
                               //         MaxQuantity = 5
                               //     }
                               // }
                            },
                             new EventCategory
                            {
                                EventTitle = "2 Adult + 2 Kid",
                                Price = 400,
                                NumberOfRegistrtionsAllowed = 2,
                                NumberOfUserSelectionDisabled = true,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode,Fields.OrganizationName, Fields.BibCollection, Fields.ShirtSize, Fields.NameOnBib },
                               //AdditionalItems = new List<AdditionalItem>()
                               // {
                               //     new AdditionalItem() {
                               //         Name = "T-Shirt",
                               //         Description = "Event T-Shirt",
                               //         Cost = 200,
                               //         MaxQuantity = 5
                               //     }
                               // }
                            }
                        },

                        EventTitle = "BRUR 2016",
                        DataOfEvent = "20-03-2016",
                        EventId = eventId,
                        Location = "Elim Resort, off Kanakapura Road",
                        IsFastLaneActive = true,
                    };
                #endregion BRUR 2016
                case "sfa_2016":
                    #region STRIDES FOR AUTISM 2016
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "1K",
                                //IsFreeEvent = true,
                                Price =300,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection}
                            },
                            new EventCategory
                            {
                                EventTitle = "5K",
                                Price = 500,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection }
                            }
                        },
                        EventTitle = "STRIDES FOR AUTISM",
                        DataOfEvent = "07-08-2016",
                        EventId = eventId,
                        Location = "Assembly- Kanteerva Stadium, Bangalore & Route - Cubbon Park",
                        IsFastLaneActive = true,
                    };
                #endregion STRIDES FOR AUTISM 2016
                case "climb_2016":
                    #region CLIMB against sexual abuse
                    return new EventDetails {
                        EventCategoryList = new List<EventCategory>
                        {
                            new EventCategory
                            {
                                EventTitle = "1000 Steps",
                                IsFreeEvent = true,
                                Price = 100,
                                NumberOfRegistrtionsAllowed = 5,
                                PricePerUser = true,
                                HiddenFields = new List<Fields> {Fields.Donation, Fields.PromoCode, Fields.OrganizationName, Fields.BibCollection, Fields.ShirtSize}
                            }
                        },
                        EventTitle = "CLIMB",
                        DataOfEvent = "26-06-2016",
                        EventId = eventId,
                        Location = "Chamundi Hill footsteps, Mysuru",
                        IsFastLaneActive = true,
                    };
                #endregion CLIMB against sexual abuse
                default:
                    return null;
            }
        }


        private static IEnumerable<AdditionalItem> GetRFHAdditionalItems(bool tshirtincluded = false) {
            var list = new List<AdditionalItem>();

            if (tshirtincluded) {
                list.Add(new AdditionalItem() {
                    Name = "T-Shirt",
                    Description = "Event T-Shirt",
                    Cost = 200,
                    MaxQuantity = 52
                });
            }
            list.Add(new AdditionalItem() {
                Name = "Timing Chip & Certificate",
                Description = "Event Timing Chip & Timing Certificate",
                Cost = 150,
                MaxQuantity = 1
            });
            return list;
        }

        private static IList<Discount> GeneratePromoCode(string couponCode) {
            return new List<Discount>
            {
                DiscountDetails.GetDiscountDetails(couponCode)
            };
        }
    }
}
