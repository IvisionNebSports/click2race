﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marathonrunners.Models {
    public class InvoiceModel {

        public string InvoiceNumber { get; set; }
        public string ReceipientFullName { get; set; }
        public string ReceiptDate { get; set; }
        public string Amount { get; set; }
        public string InvoicePurpose { get; set; }
    }
}
