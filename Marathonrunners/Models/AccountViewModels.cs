﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Security;
using Marathonrunners.Validation;

namespace Marathonrunners.Models {
    public class ExternalLoginConfirmationViewModel {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ManageUserViewModel {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel {
        [Required]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[Display(Name = "Remember me?")]
        //public bool RememberMe { get; set; }
    }

    public class ForgotPasswordViewModel {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ResetPasswordViewModel {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class RegisterViewModel {
        [Required(ErrorMessage = "Please select the Title")]
        [Display(Name = "Title*")]
        public int Title { get; set; }

        [Required(ErrorMessage = "Please enter your First Name")]
        [Display(Name = "First Name*")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your Last Name")]
        [Display(Name = "Last Name*")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your Email Address")]
        [Display(Name = "Email Address*")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please enter your Date Of Birth")]
        [Display(Name = "Date of Birth*")]
        [DisplayFormat(DataFormatString = "{0:yy-mm-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Please select your Nationality")]
        [Display(Name = "Nationality*")]
        public string Nationality { get; set; }

        public string HiddenNationality { get; set; }

        [Required(ErrorMessage = "Please select your Country")]
        [Display(Name = "Country*")]
        public string Country { get; set; }

        public string HiddenCountry { get; set; }

        [RequiredIf("state_div", "true", ErrorMessage = "State is required")]
        [Display(Name = "State*")]
        public string State { get; set; }

        public string HiddenState { get; set; }

        [RequiredIf("other_state_div", "true", ErrorMessage = "Other State is required")]
        [Display(Name = "OtherState*")]
        public string OtherState { get; set; }

        [RequiredIf("city_div", "true", ErrorMessage = "City is required")]
        [Display(Name = "City*")]
        public string City { get; set; }

        [RequiredIf("other_city_div", "true", ErrorMessage = "Other City is required")]
        [Display(Name = "Other City*")]
        public string OtherCity { get; set; }

        public string HiddenCity { get; set; }

        [Required(ErrorMessage = "Please enter your Postal Code")]
        [Display(Name = "Zip / Postal Code*")]
        public int PostalCode { get; set; }

        [Required(ErrorMessage = "Please enter your Address")]
        [Display(Name = "Address*")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please enter your Mobile No.")]
        [Display(Name = "Mobile No.*")]
        [DataType(DataType.PhoneNumber)]
        public long MobileNum { get; set; }

        [Required(ErrorMessage = "Please select your Occupation")]
        [Display(Name = "Occupation*")]
        public string Occupation { get; set; }

        [Required(ErrorMessage = "Please select your Blood Group")]
        [Display(Name = "Blood Group*")]
        public string BloodGroup { get; set; }

        [Required(ErrorMessage = "Please select your T-Shirt Size")]
        [Display(Name = "T-Shirt Size*")]
        public string ShirtSize { get; set; }

        //[Required(ErrorMessage = "Please select Identity Proof")]
        //[Display(Name = "Identity Proof*")]
        //public string IdentityProof { get; set; }

        //[Required(ErrorMessage = "Please enter corresponding ID No.")]
        //[Display(Name = "ID Number*")]
        //public string IDNum { get; set; }

        //[Required(ErrorMessage = "Please provide a scan copy of ID proof")]
        //[Display(Name = "Photo ID Proof*")]
        //[DataType(DataType.Upload)]
        //public HttpPostedFileBase PhotoIDProof { get; set; }

        //[Required(ErrorMessage = "Please provide your recent photo")]
        //[Display(Name = "Recent Photograph*")]
        //[DataType(DataType.Upload)]
        //public HttpPostedFileBase Photo { get; set; }

        [Display(Name = "Running Club")]
        public string RunningClub { get; set; }

        [Required(ErrorMessage = "Please enter Emergency Contact Name")]
        [Display(Name = "Emergency Contact Name*")]
        public string EmergencyContactName { get; set; }

        [Required(ErrorMessage = "Please enter Emergency Contact No.")]
        [Display(Name = "Emergency Contact No.*")]
        public long EmergencyContactNumber { get; set; }

        [Required(ErrorMessage = "Please select Yes/No option")]
        [Display(Name = "Do you suffer from any chronic diseases?")]
        public string ChronicDDropdown { get; set; }

       // [Required(ErrorMessage = "Please Enter the Disease")]
        [Display(Name = "Name of the disease")]
        public string ChronicDisease { get; set; }

        [Required(ErrorMessage = "Please enter Name to be on your BIB")]
        [Display(Name = "Name on BIB*")]
        public string NameOnBIB { get; set; }

        [MustBeTrue(ErrorMessage = "You must agree to the Terms and condition")]
        [Display(Name = "Legal*")]
        public bool AgreesToTerms { get; set; }

        
        public bool KeepMePosted { get; set; }

    }

    public class AccountActivationViewModel {

        [Required]
        [Display(Name = "Activation Code")]
        public string ActivationCode { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        //[MembershipPassword()]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //[MembershipPassword()]
        public string ConfirmPassword { get; set; }
    }
}
