﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Marathonrunners.Validation;

namespace Marathonrunners.Models {
    public class UpdateViewModelGender {
        [Required(ErrorMessage = "Please select the Title")]
        [Display(Name = "Title")]
        public int Title { get; set; }
    }

    public class UpdateViewModelFirstName {
        [Required(ErrorMessage = "Please enter your First Name")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
    }

    public class UpdateViewModelLastName {
        [Required(ErrorMessage = "Please enter your Last Name")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }
    }

    public class UpdateViewModelEmailId {
        [Required(ErrorMessage = "Please enter your Email Address")]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EmailAddress { get; set; }
    }

    public class UpdateViewModelDateOfBirth {
        [Required(ErrorMessage = "Please enter your Date Of Birth")]
        [Display(Name = "Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:yy-mm-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
    }

    public class UpdateViewModelNationality {
        [Required(ErrorMessage = "Please select your Nationality")]
        [Display(Name = "Nationality")]
        public string Nationality { get; set; }

        public string HiddenNationality { get; set; }
    }

    public class UpdateViewModelUserLocation {
        [Required(ErrorMessage = "Please select your Country")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Please select the State you belong to")]
        [Display(Name = "State")]
        public string State { get; set; }

        [RequiredIf("otherstate", "true", ErrorMessage = "Other State is required")]
        [Display(Name = "OtherState")]
        public string OtherState { get; set; }

        [Required(ErrorMessage = "Please enter the city you belong to")]
        [Display(Name = "City")]
        public string City { get; set; }

        [RequiredIf("othercity", "true", ErrorMessage = "Other City is required")]
        [Display(Name = "Other City")]
        public string OtherCity { get; set; }

        [Required(ErrorMessage = "Please enter your Postal Code")]
        [Display(Name = "Zip / Postal Code")]
        public int PostalCode { get; set; }

        public string HiddenCountry { get; set; }

        public string HiddenState { get; set; }

        public string HiddenCity { get; set; }
    }

    public class UpdateViewModelAddress {
        [Required(ErrorMessage = "Please enter your Address")]
        [Display(Name = "Address")]
        public string Address { get; set; }
    }

    public class UpdateViewModelMobile {
        [Required(ErrorMessage = "Please enter your Mobile No.")]
        [Display(Name = "Mobile No.")]
        [DataType(DataType.PhoneNumber)]
        public long MobileNum { get; set; }
    }

    public class UpdateViewModelOccupation {
        [Required(ErrorMessage = "Please select your Occupation")]
        [Display(Name = "Occupation")]
        public string Occupation { get; set; }
    }

    public class UpdateViewModelBloodGroup {
        [Required(ErrorMessage = "Please select your Blood Group")]
        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }
    }

    public class UpdateViewModelShirtSize {
        [Required(ErrorMessage = "Please select your T-Shirt Size")]
        [Display(Name = "T-Shirt Size")]
        public string ShirtSize { get; set; }
    }

    public class UpdateViewModelIdentityProof {
        [Required(ErrorMessage = "Please select Identity Proof")]
        [Display(Name = "Identity Proof")]
        public string IdentityProof { get; set; }
    }

    public class UpdateViewModelIDNum {
        [Required(ErrorMessage = "Please enter corresponding ID No.")]
        [Display(Name = "ID Number")]
        public string IDNum { get; set; }
    }

    public class UpdateViewModelPhotoIDProof {
        [Required(ErrorMessage = "Please provide a scan copy of ID proof")]
        [Display(Name = "Photo ID Proof")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase PhotoIDProof { get; set; }
    }

    public class UpdateViewModelPhotouserPhoto {
        [Required(ErrorMessage = "Please provide your recent photo")]
        [Display(Name = "Recent Photograph")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase Photo { get; set; }
    }

    public class UpdateViewModelRunningClub {
        [Display(Name = "Running Club")]
        public string RunningClub { get; set; }
    }

    public class UpdateViewModelEmergencyContactName {
        [Required(ErrorMessage = "Please enter Emergency Contact Name")]
        [Display(Name = "Emergency Contact Name")]
        public string EmergencyContactName { get; set; }
    }

    public class UpdateViewModelEmergencyContactNumber {
        [Required(ErrorMessage = "Please enter Emergency Contact No.")]
        [Display(Name = "Emergency Contact No.")]
        public long EmergencyContactNumber { get; set; }
    }

    public class UpdateViewModelChronicDisease {
        [Display(Name = "Do you suffer any chronic disease?")]
        public string ChronicDisease { get; set; }
    }
    public class UpdateViewModelNameOnBIB {
        [Required(ErrorMessage = "Please enter Name to be on your BIB")]
        [Display(Name = "Name on BIB*")]
        public string NameOnBIB { get; set; }
    }
}