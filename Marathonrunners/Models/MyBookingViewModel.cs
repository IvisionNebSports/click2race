﻿using System;

namespace marathonrunners.Models {
    public class MyBookingViewModel {
        public string RegistrationId { get; set; }
        public DateTime? RegistratiotnDateTime { get; set; }
        public string RegisteredEvent { get; set; }
    }
}