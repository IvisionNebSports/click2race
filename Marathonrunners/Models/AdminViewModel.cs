﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Marathonrunners.Models {
    public class AdminViewModel {
        [Display(Name = "Email Id of Recepient")]
        public string EmailId { get; set; }
        [Display(Name = "Full Name")]
        public string Name { get; set; }
        [Display(Name = "Full Address")]
        public string Address { get; set; }
        [Display(Name = "Event Name")]
        public string EventName { get; set; }
        [Display(Name = "Amount")]
        public double Amount { get; set; }
        [Display(Name = "Mobile Number")]
        public long Mobile { get; set; }
        [Display(Name = "Transaction Id")]
        public string TransactionId { get; set; }
        [Display(Name = "Date and Time as String")]
        public string DateAndTime { get; set; }
    }
}