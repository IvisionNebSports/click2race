﻿namespace Marathonrunners.Models {
    public class PayInitViewModel {
        public string EmailId { get; set; }
        public string FullName { get; set; }
        public string EventId { get; set; }
        public string Title { get; set; }
        public string EventType { get; set; }
        public int Fee { get; set; }
        public string DateOfEvent { get; set; }
        public string Location { get; set; }
        public bool Student { get; set; }
        public bool Discount { get; set; }
        public int DiscountAmount { get; set; }
        public string DiscountPurpose { get; set; }
        public int? Donation { get; set; }
        public string DonationTo { get; set; }
        public string FedDetails { get; set; }
    }

    public class BmPayInitViewModel : PayInitViewModel {
        public string BestRunTiming { get; set; }
        public string TimingHyperLink { get; set; }
        public string Distance { get; set; }
        public string EventName { get; set; }
    }
}
