﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using marathonrunners.Models;
using Marathonrunners.Core.Entities;

namespace Marathonrunners.Models {
    public class TransactionSuccessfullModel {
        public string EventId { get; set; }
        public string EventName { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public string Amount { get; set; }
        public string TransactionId { get; set; }
        public DateTime DateAndTime { get; set; }
        public bool IsDonationEvent { get; set; }
        public bool Donation { get; set; }
        public double? DonationAmount { get; set; }
        public string DonationCause { get; set; }
        public double OriginalAmountAsDouble { get; set; }
        public string PromoCode { get; set; }
        public double PromoCodeDiscount { get; set; }
        public List<UserInfo> Participants { get; set; }
        public string APRSubCategory { get; set; }
        public string APRInvoicePath { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }

        public List<AdditionalItem> AdditionalItems { get; set; }
    }

    public class KTMTransactionSuccessfullModel {
        public string EventId { get; set; }
        public string EventName { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public string TotalAmount { get; set; }
        public string EventCost { get; set; }
        public string TransactionId { get; set; }
        public DateTime DateAndTime { get; set; }
        public string EventTshirt { get; set; }
        public string TpBglr { get; set; }
        public string TpMys { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string BloodGroup { get; set; }
        public string Dob { get; set; }
        public string EventDate { get; set; }
    }
}