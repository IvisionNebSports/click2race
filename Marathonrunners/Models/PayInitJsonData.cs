﻿namespace Marathonrunners.Models {
    public class PayInitJsonData {
        public string EventId { get; set; }
        public int EventType { get; set; }
        public string RunTiming { get; set; }
        public string TimingLink { get; set; }
        public double Distance { get; set; }
        public string EventName { get; set; }
        public bool Student { get; set; }
        public int? Donation { get; set; }
        public string FedDetails { get; set; } // Federal bank details for Spice Coast Marathon
    }
}
