﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Marathonrunners.Core;
using Marathonrunners.Logging;
using Ninject;
using Ninject.Web.Common;
using NLog;
using EmailQueueSystem;
using Hangfire;
using JobScheduler;

namespace Marathonrunners {
    public class MvcApplication : NinjectHttpApplication {

        public static IKernel _kernel;
        protected override void OnApplicationStarted() {
            AreaRegistration.RegisterAllAreas();
            _kernel = CreateKernel();
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory(_kernel));
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            EmailQueuer.Instance.Start(_kernel.Get<EmailRepository>());
            TransactionVerification.Instance.Start(_kernel.Get<AccountRepository>(), _kernel.Get<EmailRepository>());
            JobSchedulerManager.Instantiate(_kernel.Get<JobSchedulerRepository>());
            HangfireBootstrapper.Instance.Start();

            //Database.SetInitializer<ApplicationDbContext>(null);
        }
        protected override void OnApplicationStopped() {
            base.OnApplicationStopped();
            EmailQueuer.Instance.Stop();
            HangfireBootstrapper.Instance.Stop();
        }
        protected override IKernel CreateKernel() {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            kernel.Bind(typeof(IAccountRepository)).To(typeof(AccountRepository));
            kernel.Bind<ILogger>().ToMethod(x => {
                var scope = x.Request.ParentRequest.Service.FullName;
                var log = (ILogger)LogManager.GetLogger(scope, typeof(NLogLogger));
                return log;
            });
            kernel.Bind(typeof(IAdminRepository)).To(typeof(AdminRespository));
            kernel.Bind(typeof(IEmailRepository)).To(typeof(EmailRepository));
            kernel.Bind(typeof(IJobSchedulerRepository)).To(typeof(JobSchedulerRepository));
            kernel.Load(new RepositoryModule());
            return kernel;
        }
    }
}
