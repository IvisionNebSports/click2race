﻿using StorageManager;

namespace Marathonrunners.Utils
{
    public static class PdfUploader
    {
        public static string UploadPDFToAzureStorage(byte[] pdfBytes, string fileName)
        {
            string path;
            StorageHandler.TryUploadFileAsBytes(StorageConfiguration.ContainerReceipts, pdfBytes, fileName, StorageConfiguration.ContentTypePDF, out path);
            return path;
        }
    }
}
