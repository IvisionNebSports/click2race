﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EmailQueueSystem;
using marathonrunners.Models;
using marathonrunners.Models.DiscountService;
using Marathonrunners.Core;
using Marathonrunners.Core.Entities;
using Marathonrunners.Models;
using RestSharp;

namespace Marathonrunners.Utils {
    public static class EmailHandler {

        internal static IRestResponse ResetPassword(Microsoft.AspNet.Identity.IdentityMessage message) {
            string body = "Hello,";
            body += "<br /><br />Please click the following link to reset your password";
            body += "<br /><a href = '" + message.Body +
                    "'>Click here to reset your password.</a>";
            body += "<br /><br />Thank you,<br/>Support Team<br/>www.click2race.com";
            var client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v2");
            client.Authenticator =
                new HttpBasicAuthenticator("api", "key-6l4t-hjdo3x4sb2odbeetalq25ypf222");
            var request = new RestRequest();
            request.AddParameter("domain",
                "click2race.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Click2Race <support@click2race.com>");
            request.AddParameter("to", message.Destination);
            request.AddParameter("subject", message.Subject);
            request.AddParameter("html", body);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        internal static List<EmailQueue> GetListOfEmailQueueFromEvents(ControllerContext context, string transactionId, List<EventBooking> events) {
            var listofemails = new List<EmailQueue>();
            foreach (var eventt in events) {
                var eventDetails = EventData.GetEventDetails(eventt.Event.EventId);
                var eventCategory = eventDetails.EventCategoryList.FirstOrDefault(x => x.EventTitle == eventt.Event.EventType);
                var productinfo = eventt.Event.EventTitle + " " + eventt.Event.EventType;
                var amountindoublevalue = eventDetails.IsDonationEvent ? eventt.Event.Donation : Convert.ToDouble(eventt.Event.Amount);
                var amount = amountindoublevalue <= 0.00 ? "Free" : amountindoublevalue + " INR";
                var mailsenderAddress = eventDetails.CustomEmailSendingAddress ?? "Click2Race <support@click2race.com>";
                var promocode = eventt.Event.PromoCode;
                var promoCodeDetails = DiscountDetails.GetDiscountDetails(promocode);
                var promocodeAmount = promoCodeDetails != null ? promoCodeDetails.DiscountAmount : 0.0;
                int count = 0;
                foreach (var user in eventt.Users) {

                    var succeeModel = new TransactionSuccessfullModel() {
                        Name = user.FirstName + " " + user.LastName,
                        Address = user.Address,
                        EventName = productinfo,
                        Amount = amount,
                        OriginalAmountAsDouble = amountindoublevalue,
                        Mobile = user.MobileNumber.ToString(),
                        TransactionId = transactionId,
                        DateAndTime = DateTime.Now,
                        EventId = eventt.Event.EventId,
                        Donation = eventt.Event.Donation != 0,
                        DonationAmount = eventt.Event.Donation,
                        DonationCause = eventt.Event.DonationTo,
                        Participants = eventt.Users,
                        IsDonationEvent = eventDetails.IsDonationEvent,
                        PromoCode = promocode,
                        PromoCodeDiscount = promocodeAmount,
                        APRSubCategory = "",
                        EmergencyContactName = user.EmergencyContactName,
                        EmergencyContactNumber = user.EmergencyContactNumber.ToString()
                    };
                    if (eventt.AdditionalItems.Count > count) {
                        var addtionalItems = eventt.AdditionalItems[count];
                        if (addtionalItems.Count > 0) {
                            List<AdditionalItem> listOfAddItems = new List<AdditionalItem>();
                            foreach (var addtionalItem in addtionalItems) {
                                if (addtionalItem.ItemIndex >= 0) {
                                    var item = eventCategory.AdditionalItems.ToList()[addtionalItem.ItemIndex];
                                    item.MaxQuantity = addtionalItem.Quantity;
                                    listOfAddItems.Add(item);
                                }
                            }
                            succeeModel.AdditionalItems = listOfAddItems;
                        }
                        count++;
                    }
                    if (eventDetails.EventId == "IDBIMHM_2017" || eventDetails.EventId == "SPBM_2017" || eventDetails.EventId == "IDBI_Promo_Run" || eventDetails.EventId == "IDBILIMHM_Training_Run" || eventDetails.EventId == "HUM5K_2017" || eventDetails.EventId == "SPBM_Luxor2017" || eventDetails.EventId == "SPBM_Promo_Run" || eventDetails.EventId == "IDBI_Training_Run2")
                    {
                        var mailHtml = ViewToString.RenderViewToString(context, "successfulMailForFreeEvent", succeeModel);
                        var email = new EmailQueue() {
                            EmailTo = user.EmailId,
                            BCC = eventDetails.AcknowledgementMail ? eventDetails.AcknowledgementMailId : "",
                            EmailFrom = mailsenderAddress,
                            Subject = eventDetails.EventId == "SPBM_2017" ? "Shriram Properties Bengaluru Marathon Inaugural Run - Registration Confirmation" : "Registration Confirmation",
                            Message = mailHtml,
                            EmailDateIn = DateTime.Now,
                            EmailType = EmailTypeEnum.RegistrationConfirmation,
                            EmailVerified = true,
                            EmailStatus = EmailStatusEnum.Queued
                        };
                        listofemails.Add(email);
                    } else {
                        var mailHtml = ViewToString.RenderViewToString(context, "SuccessfulEmailConfirmation", succeeModel);
                        var email = new EmailQueue() {
                            EmailTo = user.EmailId,
                            BCC = eventDetails.AcknowledgementMail ? eventDetails.AcknowledgementMailId : "",
                            EmailFrom = mailsenderAddress,
                            Subject = "Registration Confirmation",
                            Message = mailHtml,
                            EmailDateIn = DateTime.Now,
                            EmailType = EmailTypeEnum.RegistrationConfirmation,
                            EmailVerified = true,
                            EmailStatus = EmailStatusEnum.Queued
                        };
                        listofemails.Add(email);
                    }
                }
            }
            return listofemails;
        }

        internal static void AddMailsToTheSendingQueue(IEmailRepository emailRepo, List<EmailQueue> listOfEmails) {
            foreach (var email in listOfEmails) {
                var emailRef = emailRepo.QueueEmail(email);
                EmailQueuer.Instance.QueueIt(emailRef);
            }
        }
    }
}