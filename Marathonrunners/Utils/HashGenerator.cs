﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Marathonrunners.Utils {
    public static class HashGenerator {

        public static string Generatehash512(string text) {

            byte[] message = Encoding.UTF8.GetBytes(text);

            var UE = new UnicodeEncoding();
            byte[] hashValue;
            var hashString = new SHA512Managed();
            hashValue = hashString.ComputeHash(message);
            return hashValue.Aggregate("", (current, x) => current + String.Format("{0:x2}", x));

        }
    }
}