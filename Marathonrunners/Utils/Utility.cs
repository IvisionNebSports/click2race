﻿using Newtonsoft.Json.Linq;

namespace marathonrunners.Utils {
    public static class Utility {
        public static string AnonymousObjectToString(object obj) {
            return JObject.FromObject(obj).ToString();
        }
    }
}