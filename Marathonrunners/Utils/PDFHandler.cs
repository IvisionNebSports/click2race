﻿using NReco.PdfGenerator;

namespace Marathonrunners.Utils {
    public static class PDFHandler {

        public static byte[] ConvertHtmlStringToPDFBytes(string content) {
            var htmlToPdf = new HtmlToPdfConverter();
            return htmlToPdf.GeneratePdf(content);
        }
    }
}
