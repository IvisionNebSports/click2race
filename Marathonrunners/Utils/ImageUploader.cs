﻿using StorageManager;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace Marathonrunners.Utils {
    public static class ImageUploader {

        public static string UploadImageToAzureStorage(HttpPostedFileBase photo,string imagename) {
            string path;
            StorageHandler.TryUploadImage(StorageConfiguration.ContainerUserImages, photo, imagename, out path);
            return path;
        }

        public static bool IsValid(object value) {
            var file = value as HttpPostedFileBase;
            if (file == null) {
                return false;
            }
            if (file.ContentLength > 750 * 1024) {
                return false;
            }
            try {
                using (var img = Image.FromStream(file.InputStream)) {
                    return img.RawFormat.Equals(ImageFormat.Jpeg);
                }
            } catch { }
            return false;
        }
    }
}