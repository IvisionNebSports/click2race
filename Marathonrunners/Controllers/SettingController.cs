﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.WebPages;
using Marathonrunners.Core;
using Marathonrunners.Models;

namespace Marathonrunners.Controllers {
    public class SettingController : Controller {
        //
        // GET: /Setting/
        private IAccountRepository _accountRepository;
        public SettingController(IAccountRepository accountRepository) {
            _accountRepository = accountRepository;
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult Gender() {
            return PartialView("Gender");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult GenderUpdate(UpdateViewModelGender gendermodel) {
            // TODO: hardcoded here find a better way.
            var title = new string[] { "Male", "Female" };
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "title", title[gendermodel.Title - 1]);
                _accountRepository.UpdateField("Gender", gendermodel.Title.ToString(), User.Identity.Name, 1);
                return PartialView("Success");
            }
            return PartialView("Gender", gendermodel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult FirstName() {
            return PartialView("FirstName");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult FirstNameUpdate(UpdateViewModelFirstName firstNameModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "firstname_view", firstNameModel.FirstName);
                _accountRepository.UpdateField("FirstName", firstNameModel.FirstName, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("FirstName", firstNameModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult LastName() {
            return PartialView("LastName");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LastNameUpdate(UpdateViewModelLastName lastNameModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "lastname_view", lastNameModel.LastName);
                _accountRepository.UpdateField("LastName", lastNameModel.LastName, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("LastName", lastNameModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult DateOfBirth() {
            return PartialView("DateOfBirth");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult DateOfBirthUpdate(UpdateViewModelDateOfBirth dateOfBirthModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "dob_view", dateOfBirthModel.DateOfBirth.Date.ToString("yyyy-MM-dd"));
                _accountRepository.UpdateField("DOB", dateOfBirthModel.DateOfBirth.Date.ToString("yyyy-MM-dd"), User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("DateOfBirth", dateOfBirthModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult Nationality() {
            return PartialView("Nationality");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult NationalityUpdate(UpdateViewModelNationality nationalityModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "nationality_view", nationalityModel.Nationality);
                _accountRepository.UpdateField("Nationaility", nationalityModel.Nationality, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("Nationality", nationalityModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult UserLocation() {
            return PartialView("UserLocation");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult UserLocationUpdate(UpdateViewModelUserLocation userLocationModel) {
            try {
                if (userLocationModel.Country != "India") {
                    bool error = false;
                    if (string.IsNullOrEmpty(userLocationModel.OtherCity)) {
                        error = true;
                        ModelState.AddModelError("OtherCity", "Other City cannot be empty");
                    }
                    if (string.IsNullOrEmpty(userLocationModel.OtherState)) {
                        error = true;
                        ModelState.AddModelError("OtherState", "Other State cannot be empty");
                    }
                    if (error) return PartialView("UserLocation", userLocationModel);
                }
                if (userLocationModel.Country != "India" || ModelState.IsValid) {
                    // TODO: Make a JSON object instead?
                    ViewBag.Location = true;
                    ViewBag.Value = "\'[" +
                                    string.Format("{{\"field\":\"{0}\",\"value\":\"{1}\"}},", "country_view",
                                        userLocationModel.Country) +
                                    string.Format("{{\"field\":\"{0}\",\"value\":\"{1}\"}},", "state_view",
                                        userLocationModel.State) +
                                    string.Format("{{\"field\":\"{0}\",\"value\":\"{1}\"}},", "city_view",
                                        userLocationModel.City) +
                                    string.Format("{{\"field\":\"{0}\",\"value\":\"{1}\"}},", "otherstate_view",
                                        userLocationModel.OtherState) +
                                    string.Format("{{\"field\":\"{0}\",\"value\":\"{1}\"}}", "othercity_view",
                                        userLocationModel.OtherCity) +
                                    "]\'";
                    var values = new List<string>(){
                        userLocationModel.Country,
                        userLocationModel.Country != "India" ? userLocationModel.OtherState: 
                        userLocationModel.State ?? userLocationModel.OtherState,
                        userLocationModel.Country != "India" ? userLocationModel.OtherCity : userLocationModel.City ?? userLocationModel.OtherCity,
                    };
                    _accountRepository.UpdateUserLocation(values, userLocationModel.PostalCode, User.Identity.Name);
                    return PartialView("Success");
                }
            } catch (Exception ex) {
                ModelState.AddModelError("", ex.Message);
            }
            return PartialView("UserLocation", userLocationModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult Address() {
            return PartialView("Address");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AddressUpdate(UpdateViewModelAddress addressModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "address_view", addressModel.Address);
                _accountRepository.UpdateField("Address", Server.HtmlEncode(addressModel.Address), User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("Address", addressModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult Mobile() {
            return PartialView("Mobile");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult MobileUpdate(UpdateViewModelMobile mobileModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "mobile_view", mobileModel.MobileNum);
                _accountRepository.UpdateField("MobileNumber", mobileModel.MobileNum.ToString(), User.Identity.Name, 1);
                return PartialView("Success");
            }
            return PartialView("Mobile", mobileModel);
        }


        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult Occupation() {
            return PartialView("Occupation");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult OccupationUpdate(UpdateViewModelOccupation occupationModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "occupation_view", occupationModel.Occupation);
                _accountRepository.UpdateField("Occupation", occupationModel.Occupation, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("Occupation", occupationModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult BloodGroup() {
            return PartialView("BloodGroup");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult BloodGroupUpdate(UpdateViewModelBloodGroup bloodGroupModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "bloodgroup_view", bloodGroupModel.BloodGroup);
                _accountRepository.UpdateField("BloodGroup", bloodGroupModel.BloodGroup, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("BloodGroup", bloodGroupModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult ShirtSize() {
            return PartialView("ShirtSize");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ShirtSizeUpdate(UpdateViewModelShirtSize shirtSizeModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "shirtsize_view", shirtSizeModel.ShirtSize);
                _accountRepository.UpdateField("ShirtSzie", shirtSizeModel.ShirtSize, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("ShirtSize", shirtSizeModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult IdentityProof() {
            return PartialView("IdentityProof");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult IdentityProofUpdate(UpdateViewModelIdentityProof identityProofModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "identityproof_view", identityProofModel.IdentityProof);
                _accountRepository.UpdateField("IdentityProof", identityProofModel.IdentityProof, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("IdentityProof", identityProofModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult IdNum() {
            return PartialView("IdNum");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult IdNumUpdate(UpdateViewModelIDNum idNumModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "idnum_view", idNumModel.IDNum);
                _accountRepository.UpdateField("IdNumber", idNumModel.IDNum, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("IdNum", idNumModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult RunningClub() {
            return PartialView("RunningClub");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RunningClubUpdate(UpdateViewModelRunningClub runningClubModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "runningclub_view", runningClubModel.RunningClub);
                _accountRepository.UpdateField("RunningClub", runningClubModel.RunningClub, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("RunningClub", runningClubModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult EmergencyContactName() {
            return PartialView("EmergencyContactName");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult EmergencyContactNameUpdate(UpdateViewModelEmergencyContactName emergencyContactNameModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "emergencycontactname_view", emergencyContactNameModel.EmergencyContactName);
                _accountRepository.UpdateField("EmergencyContactName", emergencyContactNameModel.EmergencyContactName, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("EmergencyContactName", emergencyContactNameModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult EmergencyContactNumber() {
            return PartialView("EmergencyContactNumber");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult EmergencyContactNumberUpdate(UpdateViewModelEmergencyContactNumber emergencyContactNumberModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "emergencycontactnumber_view", emergencyContactNumberModel.EmergencyContactNumber);
                _accountRepository.UpdateField("EmergencyContactNumber", emergencyContactNumberModel.EmergencyContactNumber.ToString(), User.Identity.Name, 1);
                return PartialView("Success");
            }
            return PartialView("EmergencyContactNumber", emergencyContactNumberModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult ChronicDisease() {
            return PartialView("ChronicDisease");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ChronicDiseaseUpdate(UpdateViewModelChronicDisease chronicDiseaseModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "chronicdisease_view", chronicDiseaseModel.ChronicDisease);
                _accountRepository.UpdateField("ChronicDisease", chronicDiseaseModel.ChronicDisease, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("ChronicDisease", chronicDiseaseModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult NameOnBib() {
            return PartialView("NameOnBib");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult NameOnBibUpdate(UpdateViewModelNameOnBIB nameOnBIBModel) {
            if (ModelState.IsValid) {
                // TODO: Make a JSON object instead?
                ViewBag.Value = string.Format("\'{{\"field\":\"{0}\",\"value\":\"{1}\"}}\'", "nameonbib_view", nameOnBIBModel.NameOnBIB);
                _accountRepository.UpdateField("NameOnBib", nameOnBIBModel.NameOnBIB, User.Identity.Name, 3);
                return PartialView("Success");
            }
            return PartialView("NameOnBib", nameOnBIBModel);
        }
    }
}