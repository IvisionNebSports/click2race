﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;
using Marathonrunners.Core;
using Marathonrunners.Core.Entities;
using Marathonrunners.Logging;
using marathonrunners.Models;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Marathonrunners.Models;
using Newtonsoft.Json;
using Marathonrunners.Utils;

namespace Marathonrunners.Controllers {
    [Authorize]
    public class AccountController : Controller {
        private IAccountRepository _accountRepository;
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        private ILogger _logger;
        private IEmailRepository _emailRepository;

        public AccountController(IAccountRepository accountRepository, ILogger logger, IEmailRepository emailRepository) {
            _accountRepository = accountRepository;
            _logger = logger;
            _emailRepository = emailRepository;
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager) {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager {
            get {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager {
            get {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        [RequireHttps]
        public ActionResult Login(string returnUrl) {
            if (User.Identity.IsAuthenticated)
                return RedirectToLocal(returnUrl);
            ViewBag.ReturnUrl = returnUrl;
            return View("Login");
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [RequireHttps]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string ReturnUrl) {
            if (ModelState.IsValid) {
                var user = await UserManager.FindAsync(model.EmailAddress, model.Password);
                if (user != null) {
                    await SignInAsync(user, true);
                    Session["user"] = _accountRepository.GetUserById(model.EmailAddress).FirstName;
                    return RedirectToLocal(ReturnUrl);
                } else {
                    ModelState.AddModelError("", "Invalid email id or password.");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl) {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl) {
            if (ControllerContext.HttpContext.Session != null) ControllerContext.HttpContext.Session.RemoveAll();
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null) {
                return RedirectToAction("Login");
            }
            SignInStatus result = SignInStatus.Failure;
            var user = UserManager.FindByEmailAsync(loginInfo.Email);
            if (user.Result != null) {
                // Sign in the user with this external login provider if the user already has a login
                await SignInManager.SignInAsync(user.Result, false, false);
                Session["user"] = _accountRepository.GetUserById(user.Result.Email).FirstName;
                result = SignInStatus.Success;
            }

            switch (result) {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                //case SignInStatus.LockedOut:
                //    return View("Lockout");
                //case SignInStatus.RequiresVerification:
                //    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    ViewBag.LoginEmail = loginInfo.Email;
                    return View("Register");
            }
        }

        // POST: /Account/ExternalLoginConfirmation
        //[HttpPost]
        //[AllowAnonymous]
        ////[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl) {
        //    if (User.Identity.IsAuthenticated) {
        //        return RedirectToAction("Index", "Manage");
        //    }

        //    if (ModelState.IsValid) {
        //        // Get the information about the user from the external login provider
        //        var info = await AuthenticationManager.GetExternalLoginInfoAsync();
        //        if (info == null) {
        //            return View("ExternalLoginFailure");
        //        }
        //        var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //        var result = await UserManager.CreateAsync(user);
        //        if (result.Succeeded) {
        //            result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //            if (result.Succeeded) {
        //                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //                return RedirectToLocal(returnUrl);
        //            }
        //        }
        //        AddErrors(result);
        //    }

        //    ViewBag.ReturnUrl = returnUrl;
        //    return View(model);
        //}

        //
        // GET: /Account/Register
        [AllowAnonymous]
        [RequireHttps]
        public ActionResult Register(string returnUrl = "") {

            if (!returnUrl.IsNullOrWhiteSpace()) {
                string returnurl = HttpUtility.UrlEncode(returnUrl);
                ViewBag.ReturnUrl = returnurl;
                ViewBag.Cwdr = returnurl.Contains("cwdr_2015");
            }
            if (User.Identity.IsAuthenticated && !returnUrl.IsNullOrWhiteSpace()) {
                return RedirectToLocal(returnUrl);
            }
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [RequireHttps]
        //[ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model, string ReturnUrl) {

            if (ModelState.IsValid) {

                try {
                    var user = UserManager.FindByNameAsync(model.EmailAddress);

                    if (_accountRepository.ActivationCodeAlreadySent(model.EmailAddress))
                        throw new Exception("Activation code has already been sent to your email address (" + model.EmailAddress + ").\n" +
                                             "If any problem please get help from support@click2race.in");

                    if (user.Result != null || _accountRepository.IsExistingUser(model.EmailAddress))
                        throw new Exception("You are an existing customer!");

                    //Special validation need to be added fro country specific thingy. 
                    if (model.Country != "India") {
                        if (model.OtherState == null ||
                            model.OtherCity.IsEmpty() ||
                            model.OtherCity == null ||
                            model.OtherCity.IsEmpty())
                            throw new Exception("Other State and Other City cannot be empty");
                    }

                    var activationCode = Guid.NewGuid() + DateTime.Now.ToString("yyMMddhhmmss");
                    var tempdata = new UserTempData() {
                        DateOfRegister = DateTime.Now,
                        Id = activationCode,
                        UserId = model.EmailAddress
                    };
                    var userinfo = new UserInfo() {
                        Active = false,
                        Address = model.Address,
                        BloodGroup = model.BloodGroup,
                        City = model.Country == "India" ? model.City : model.OtherCity,
                        Country = model.Country,
                        DOB = model.DateOfBirth,
                        DateOfRegister = DateTime.Now,
                        EmailId = model.EmailAddress,
                        EmergencyContactName = model.EmergencyContactName,
                        EmergencyContactNumber = model.EmergencyContactNumber,
                        FirstName = model.FirstName,
                        Gender = model.Title,
                        IdNumber = "",
                        IdentityProof = "None",
                        LastName = model.LastName,
                        MobileNumber = model.MobileNum,
                        Nationaility = model.Nationality,
                        Occupation = model.Occupation,
                        //TODO: Call upload images one time i.e pass the list of images to it.
                        //PhotoIdProof = Utils.ImageUploader.UploadImageToAzureStorage(model.PhotoIDProof, string.Format("usersidproof_{0}{1}", model.EmailAddress, Path.GetExtension(model.PhotoIDProof.FileName))),
                        PhotoIdProof = "https://clicktorace.blob.core.windows.net/portaluserimages/NO-IMAGE-AVAILABLE.jpg",
                        Pincode = model.PostalCode,
                        RunningClub = model.RunningClub,
                        ShirtSzie = model.ShirtSize,
                        State = model.Country == "India" ? model.State : model.OtherState,
                        //UserPhoto = Utils.ImageUploader.UploadImageToAzureStorage(model.Photo, string.Format("usersphoto_{0}{1}", model.EmailAddress, Path.GetExtension(model.PhotoIDProof.FileName))),
                        UserPhoto = "https://clicktorace.blob.core.windows.net/portaluserimages/NO-IMAGE-AVAILABLE.jpg",
                        ChronicDisease = model.ChronicDisease,
                        NameOnBib = model.NameOnBIB
                    };
                    _accountRepository.RegisterVerificationData(tempdata);
                    _accountRepository.RegisterUser(userinfo);
                    string returnUrlPath = "";
                    if (!ReturnUrl.IsNullOrWhiteSpace()) {
                        returnUrlPath = "&returnUrl=" + ReturnUrl;
                    }
                    ViewBag.ActivationLink = "https://" + System.Web.HttpContext.Current.Request.Url.Host + "/Account/AccountActivation?activationcode=" + activationCode + returnUrlPath;
                    var mailHtml = ViewToString.RenderViewToString(this.ControllerContext, "AccountActivationLink");
                    var email = new EmailQueue() {
                        EmailTo = model.EmailAddress,
                        EmailFrom = "Click2Race <support@click2race.in>",
                        Subject = "Account Activation Code",
                        Message = mailHtml,
                        EmailDateIn = DateTime.Now,
                        EmailType = EmailTypeEnum.AccountActivation,
                        EmailVerified = true,
                        EmailStatus = EmailStatusEnum.Queued
                    };
                    var ret = _emailRepository.QueueEmail(email);
                    EmailQueueSystem.EmailQueuer.Instance.QueueIt(ret);
                    //var response = Utils.EmailHandler.SendActivationMail(model.EmailAddress, mailHtml);
                    //_logger.Info("Activation mail sent|User:" + model.EmailAddress + "|Response:" + response.ResponseStatus + "|" + response.StatusDescription);
                } catch (Exception exception) {
                    _logger.Info("Registration Failed|User:" + model.EmailAddress + "|Error:" + exception.Message);
                    ModelState.AddModelError("", exception.Message);
                    return View(model);
                }
                return RedirectToAction("ActivationLink", "Account", new { activationmailsent = true });
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        [RequireHttps]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterExternal(RegisterViewModel model, string ReturnUrl) {

            if (ModelState.IsValid) {

                try {
                    var info = AuthenticationManager.GetExternalLoginInfo();
                    if (info == null) {
                        ModelState.AddModelError("login failed", "Your session has expired");
                        return View("Register", model);
                    }


                    if (_accountRepository.IsExistingUser(model.EmailAddress)) {
                        return RedirectToLocal(ReturnUrl);
                    }

                    //Special validation need to be added fro country specific thingy. 
                    if (model.Country != "India") {
                        if (model.OtherState == null ||
                            model.OtherCity.IsEmpty() ||
                            model.OtherCity == null ||
                            model.OtherCity.IsEmpty())
                            ModelState.AddModelError("wrong state or city", "Other State and Other City cannot be empty");
                        return View("Register", model);
                    }

                    var activationCode = Guid.NewGuid().ToString();
                    var tempdata = new UserTempData() {
                        DateOfRegister = DateTime.Now,
                        Id = activationCode,
                        UserId = model.EmailAddress
                    };
                    var userinfo = new UserInfo() {
                        Active = false,
                        Address = model.Address,
                        BloodGroup = model.BloodGroup,
                        City = model.Country == "India" ? model.City : model.OtherCity,
                        Country = model.Country,
                        DOB = model.DateOfBirth,
                        DateOfRegister = DateTime.Now,
                        EmailId = model.EmailAddress,
                        EmergencyContactName = model.EmergencyContactName,
                        EmergencyContactNumber = model.EmergencyContactNumber,
                        FirstName = model.FirstName,
                        Gender = model.Title,
                        IdNumber = "",
                        IdentityProof = "None",
                        LastName = model.LastName,
                        MobileNumber = model.MobileNum,
                        Nationaility = model.Nationality,
                        Occupation = model.Occupation,
                        //TODO: Call upload images one time i.e pass the list of images to it.
                        //PhotoIdProof = Utils.ImageUploader.UploadImageToAzureStorage(model.PhotoIDProof, string.Format("usersidproof_{0}{1}", model.EmailAddress, Path.GetExtension(model.PhotoIDProof.FileName))),
                        PhotoIdProof = "https://clicktorace.blob.core.windows.net/portaluserimages/NO-IMAGE-AVAILABLE.jpg",
                        Pincode = model.PostalCode,
                        RunningClub = model.RunningClub,
                        ShirtSzie = model.ShirtSize,
                        State = model.Country == "India" ? model.State : model.OtherState,
                        //UserPhoto = Utils.ImageUploader.UploadImageToAzureStorage(model.Photo, string.Format("usersphoto_{0}{1}", model.EmailAddress, Path.GetExtension(model.PhotoIDProof.FileName))),
                        UserPhoto = "https://clicktorace.blob.core.windows.net/portaluserimages/NO-IMAGE-AVAILABLE.jpg",
                        ChronicDisease = model.ChronicDisease,
                        NameOnBib = model.NameOnBIB
                    };
                    _accountRepository.RegisterUser(userinfo);
                    //TODO: Send a registration email
                    //var mailHtml = ViewToString.RenderViewToString(this.ControllerContext, "AccountActivationLink");
                    //var response = Utils.EmailHandler.SendActivationMail(model.EmailAddress, mailHtml);
                    var user = UserManager.FindByNameAsync(model.EmailAddress);
                    if (user.Result == null) {
                        var userInfo = new ApplicationUser { UserName = info.Email, Email = info.Email, EmailConfirmed = true };
                        var result = await UserManager.CreateAsync(userInfo);
                        if (result.Succeeded) {
                            result = await UserManager.AddLoginAsync(userInfo.Id, info.Login);
                            if (result.Succeeded) {
                                await SignInManager.SignInAsync(userInfo, isPersistent: false, rememberBrowser: false);
                                return RedirectToLocal(ReturnUrl);
                            }
                        }
                    } else {
                        await SignInManager.SignInAsync(user.Result, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(ReturnUrl);
                    }
                    //_logger.Info("Activation mail sent|User:" + model.EmailAddress + "|Response:" + response.ResponseStatus + "|" + response.StatusDescription);
                } catch (Exception exception) {
                    _logger.Info("Registration Failed|User:" + model.EmailAddress + "|Error:" + exception.Message);
                    ModelState.AddModelError("", exception.Message);
                    return View("Register", model);
                }
            }
            // If we got this far, something failed, redisplay form
            return View("Register", model);
        }

        //
        // GET: /Account/Register
        //[AllowAnonymous]

        public ActionResult Edit() {
            if (!User.Identity.IsAuthenticated) {
                return RedirectToAction("Index", "Home");
            }
            var user = _accountRepository.GetUserById(User.Identity.Name);
            return View(user);
        }

        [HttpPost]
        public ContentResult UploadUserPhoto() {
            var hpf = Request.Files[0] as HttpPostedFileBase;
            _logger.Info("File Upload UserPhoto initiated|User:" + User.Identity.Name);
            if (hpf.ContentLength == 0) {
                _logger.Info("File Upload UserPhoto Fail=>File is zero length|User:" + User.Identity.Name);
                return Content("{\"message\":\"File is zero length\",\"error\":\"true\"}", "application/json");
            }
            if (!Utils.ImageUploader.IsValid(hpf)) {
                _logger.Info("File Upload UserPhoto Fail=>Its not a vaild image file(" + hpf.ContentType + ")|User:" + User.Identity.Name);
                return Content("{\"message\":\"Its not a vaild image file\",\"error\":\"true\"}", "application/json");
            }
            // TODO: Error checking
            string savedFileName = "";
            try {
                savedFileName = Utils.ImageUploader.UploadImageToAzureStorage(hpf, string.Format("usersphoto_{0}{1}", User.Identity.Name, Path.GetExtension(hpf.FileName)));
                _accountRepository.UpdateImage("UserPhoto", User.Identity.Name, savedFileName);
                _logger.Info("File Upload UserPhoto Database entry success|User:" + User.Identity.Name);
            } catch (Exception exception) {
                _logger.Info("File Upload UserPhoto Fail:Exception=>" + exception.Message + "|User:" + User.Identity.Name);
                return Content("{\"message\":\"Something went wrong!!\",\"error\":\"true\"}", "application/json");
            }
            _logger.Info("File Upload UserPhoto success|User:" + User.Identity.Name);
            return Content("{\"name\":\"" + savedFileName + "\",\"error\":\"false\"}", "application/json");
        }

        [HttpPost]
        public ContentResult UploadUserPhotoIdProof() {
            var hpf = Request.Files[0] as HttpPostedFileBase;
            _logger.Info("File Upload UserPhotoIdProof initiated|User:" + User.Identity.Name);
            if (hpf.ContentLength == 0) {
                _logger.Info("File Upload UserPhotoIdProof Fail=>File is zero length|User:" + User.Identity.Name);
                return Content("{\"message\":\"File is zero length\",\"error\":\"true\"}", "application/json");
            }
            if (!Utils.ImageUploader.IsValid(hpf)) {
                _logger.Info("File Upload UserPhotoIdProof Fail=>Its not a vaild image file(" + hpf.ContentType + ")|User:" + User.Identity.Name);
                return Content("{\"message\":\"Its not a vaild image file\",\"error\":\"true\"}", "application/json");
            }
            string savedFileName = "";
            try {
                savedFileName = Utils.ImageUploader.UploadImageToAzureStorage(hpf, string.Format("usersidproof_{0}{1}", User.Identity.Name, Path.GetExtension(hpf.FileName)));
                _accountRepository.UpdateImage("PhotoIdProof", User.Identity.Name, savedFileName);
                _logger.Info("File Upload UserPhotoIdProof Database entry success|User:" + User.Identity.Name);
            } catch (Exception exception) {
                _logger.Info("File Upload UserPhotoIdProof Fail:Exception=>" + exception.Message + "|User:" + User.Identity.Name);
                return Content("{\"message\":\"Something went wrong!!\",\"error\":\"true\"}", "application/json");
            }
            _logger.Info("File Upload UserPhotoIdProof success|User:" + User.Identity.Name);
            return Content("{\"name\":\"" + savedFileName + "\",\"error\":\"false\"}", "application/json");
        }

        ////
        //// POST: /Account/Disassociate
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Disassociate(string loginProvider, string providerKey) {
        //    ManageMessageId? message = null;
        //    IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
        //    if (result.Succeeded) {
        //        message = ManageMessageId.RemoveLoginSuccess;
        //    } else {
        //        message = ManageMessageId.Error;
        //    }
        //    return RedirectToAction("Manage", new { Message = message });
        //}


        //GET: /Account/ChangePassword
        //[HttpPost]
        //[ValidateAntiForgeryToken]

        public ActionResult ChangePassword() {
            return View();
        }


        //POST: /Account/ChangePassword
        [HttpPost]
        [RequireHttps]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ManageUserViewModel model) {
            if (ModelState.IsValid) {
                IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                if (result.Succeeded) {
                    ViewBag.SuccessMessage = "Your password has been changed successfully!!";
                    _logger.Info("Change Password success|User:" + User.Identity.Name);
                    return View("Success");
                } else {
                    AddErrors(result);
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        [RequireHttps]

        public ActionResult ForgotPassword() {
            return View("ForgotPassword ");
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [RequireHttps]

        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model) {
            if (ModelState.IsValid) {
                try {
                    var user = await UserManager.FindByNameAsync(model.Email);
                    if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id))) {
                        ModelState.AddModelError("", "The user either does not exist or is not confirmed.");
                        _logger.Info("ForgotPassword Error:The user either does not exist or is not confirmed|User:" + User.Identity.Name);
                        return View("ForgotPassword ");
                    }

                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(user.Id, "Reset Password", callbackUrl);
                } catch (Exception exception) {
                    ModelState.AddModelError("", exception.Message);
                    _logger.Info("ForgotPassword Exception:" + exception.Message + "|User:" + User.Identity.Name);
                    return View("ForgotPassword ", model);
                }
                ViewBag.SuccessMessage = "Link to reset your password has been sent to your mail. Please check!!";
                _logger.Info("ForgotPassword link sent|User:" + model.Email);
                return View("Success"); ;
            }

            // If we got this far, something failed, redisplay form
            return View("ForgotPassword ", model);
        }



        // GET: /Account/ResetPassword
        [AllowAnonymous]
        [RequireHttps]
        public ActionResult ResetPassword(string code) {
            if (code == null) {
                ViewBag.ErrorMessage = "Email verification code missing!!";
                _logger.Info("ResetPassword Failed:Email verification code missing!!|User:" + User.Identity.Name);
                return View("Error");
            }
            return View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [RequireHttps]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model) {
            if (ModelState.IsValid) {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null) {
                    ModelState.AddModelError("", "No user found. Are you sure with the email address? If any support" +
                                                 "required please contact support@click2race.in");
                    _logger.Info("ResetPassword Failed:No user found|User:" + User.Identity.Name);
                    return View();
                }
                IdentityResult result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
                if (result.Succeeded) {
                    ViewBag.SuccessMessage = "Your password has been reset successfully";
                    _logger.Info("ResetPassword Success|User:" + User.Identity.Name);
                    return View("Success");
                } else {
                    AddErrors(result);
                    _logger.Info("ResetPassword Failed:" + result.Errors + "|User:" + User.Identity.Name);
                    return View();
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        [RequireHttps]
        [AllowAnonymous]
        public async Task<ActionResult> AccountActivation(AccountActivationViewModel model, string ReturnUrl) {
            if (!ModelState.IsValid) return View(model);
            try {
                var userInfo = _accountRepository.GetUserIdFromActivationCode(model.ActivationCode);
                if (userInfo == null) {
                    _logger.Info("AccountActivation Failed:No user found|User:" + User.Identity.Name);
                    throw new Exception("This is an invalid activation code. If any problem then please contact support@click2race.in");
                }
                var user = new ApplicationUser() { UserName = userInfo.UserId, Email = userInfo.UserId, EmailConfirmed = true };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (!result.Succeeded) {
                    _logger.Info("AccountActivation Failed:Errors=>" + result.Errors + "|User:" + User.Identity.Name);
                    throw new Exception(string.Join("\n", result.Errors));
                }
                var deletusertempdata = _accountRepository.DeleteTempData(model.ActivationCode);
                if (!deletusertempdata) {
                    _logger.Fatal("AccountActivation Failed:Failed to delete TempData" + result.Errors + "|User:" + User.Identity.Name);
                }
                ViewBag.SuccessMessage = "Account has been activated successfully! Please Login!";
                _logger.Info("AccountActivation Successfull|User:" + User.Identity.Name);
                await SignInManager.SignInAsync(user, false, false);
                if (!ReturnUrl.IsNullOrWhiteSpace()) {
                    return RedirectToLocal(ReturnUrl);
                }
                return RedirectToLocal("");
            } catch (Exception exception) {
                ModelState.AddModelError("", exception.Message);
                _logger.Info("AccountActivation Failed:Exception=>" + exception.Message + "|User:" + User.Identity.Name);
            }
            ViewBag.ActivationCode = model.ActivationCode;
            return View(model);
        }

        [AllowAnonymous]
        [RequireHttps]
        public ActionResult AccountActivation(string activationcode = null, string returnUrl = "") {
            if (activationcode == null) {
                ViewBag.ErrorMessage = "Please provide an activation key.";
                _logger.Info("AccountActivation Failed:Errors=>Activation key is null");
                return View("Error");
            }
            if (!returnUrl.IsNullOrWhiteSpace())
                ViewBag.ReturnUrl = returnUrl;
            ViewBag.ActivationCode = activationcode;
            _logger.Info("AccountActivation Success");
            return View();
        }

        [AllowAnonymous]
        [RequireHttps]
        public ActionResult ActivationLink(bool activationmailsent) {
            ViewBag.SuccessMessage = "Please check your registered Email for the activation link!. Please check Spam if not found in Inbox";
            _logger.Info("ActivationLink sent =>" + activationmailsent);
            return View("Success");
        }

        // POST: /Account/LogOff
        [HttpPost]
        [RequireHttps]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff() {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }


        public ActionResult MyRace() {
            // TODO: Turn this into a join statement. Super ugly code
            var userRegs = _accountRepository.GetRegistrationsOf(User.Identity.Name);
            var userKTMRegs = _accountRepository.GetKTMRegistrationsOf(User.Identity.Name);
            var usercontourRegs = _accountRepository.GetContourRegistrationsOf(User.Identity.Name);
            var userBMPromoRun2015 = _accountRepository.GetBM2015RegistrationsOf(User.Identity.Name);
            var bookings = new List<MyBookingViewModel>();
            bookings.AddRange(userRegs.Select(x => new MyBookingViewModel() { RegisteredEvent = x.RunCategoryId, RegistratiotnDateTime = x.RegistrationDate, RegistrationId = x.TransactionId == "NOT USED ANYMORE" ? "C2R-" + x.RunRegistrationId : x.TransactionId }));
            bookings.AddRange(userKTMRegs.Select(x => new MyBookingViewModel() { RegisteredEvent = x.RunCategory, RegistratiotnDateTime = x.RegistrationDate, RegistrationId = x.TransactionId }));
            bookings.AddRange(usercontourRegs.Select(x => new MyBookingViewModel() { RegisteredEvent = x.RunCategoryId, RegistratiotnDateTime = x.RegistrationDate, RegistrationId = x.TransactionId }));
            bookings.AddRange(userBMPromoRun2015.Select(x => new MyBookingViewModel() { RegisteredEvent = x.RunCategoryId, RegistratiotnDateTime = x.RegistrationDate, RegistrationId = "C2R-" + x.TransactionId }));
            return View("MyBooking", bookings);
        }

        ////
        //// GET: /Account/ExternalLoginFailure
        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure() {
        //    return View();
        //}

        //[ChildActionOnly]
        //public ActionResult RemoveAccountList() {
        //    var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
        //    ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
        //    return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        //}

        protected override void Dispose(bool disposing) {
            if (disposing && UserManager != null) {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        //// Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfIdC2R";

        private IAuthenticationManager AuthenticationManager {
            get {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent) {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, await user.GenerateUserIdentityAsync(UserManager));
        }

        private void AddErrors(IdentityResult result) {
            foreach (var error in result.Errors) {
                ModelState.AddModelError("", error);
            }
        }

        //private bool HasPassword() {
        //    var user = UserManager.FindById(User.Identity.GetUserId());
        //    if (user != null) {
        //        return user.PasswordHash != null;
        //    }
        //    return false;
        //}

        //public enum ManageMessageId {
        //    ChangePasswordSuccess,
        //    SetPasswordSuccess,
        //    RemoveLoginSuccess,
        //    Error
        //}

        private ActionResult RedirectToLocal(string returnUrl) {
            if (Url.IsLocalUrl(returnUrl)) {
                return Redirect(returnUrl);
            } else {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null) {
            }

            public ChallengeResult(string provider, string redirectUri, string userId) {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context) {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null) {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
