﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Marathonrunners.Controllers {
    public class HomeController : Controller {

        [RequireHttps]
        public ActionResult Index() {
            return View();
        }

        [RequireHttps]
        public ActionResult CustomUrl(string id) {
            return RedirectToAction("Index", "Register", new RouteValueDictionary()
            {
                {"eventid","rfh_2016" }
            });
        }
        public ActionResult Disclaimer()
        {
            ViewBag.Message = "Your disclaimer page.";
            return View();
        }
        public ActionResult Privacy()
        {
            ViewBag.Message = "Your privacy page.";
            return View();
        }
        public ActionResult TermsOfUse()
        {
            ViewBag.Message = "Your TermsOfUse page.";
            return View();
        }
        public ActionResult About() {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult RunnersWallet() {
            ViewBag.Message = "Runners Wallet page.";
            return View();
        }

        public ActionResult Groups() {
            ViewBag.Message = "Your Groups page.";
            return View();
        }

        public ActionResult Racebook() {
            ViewBag.Message = "Racebook";
            return View();
        }

        [ChildActionOnly]
        public ActionResult Carousel() {
            return PartialView("_CarouselPartial");
        }

        [ChildActionOnly]
        public ActionResult Sidebar() {
            return PartialView("_SideBarPartial");
        }

        [ChildActionOnly]
        public ActionResult Search() {
            return PartialView("_SearchPartial");
        }

        [RequireHttps]
        public ActionResult Login() {
            return PartialView("_LoginPartial");
        }

        [HttpPost]
        public ActionResult GetMenuSections(string section) {
            return PartialView(section);
        }

    }
}