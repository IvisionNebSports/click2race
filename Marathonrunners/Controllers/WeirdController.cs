﻿using System.Web.Mvc;
using Marathonrunners.Logging;

namespace Marathonrunners.Controllers {
    public class WeirdController : Controller {
        private readonly ILogger _logger;

        public WeirdController(ILogger logger) {
            _logger = logger;
        }

        public ActionResult Index() {
            _logger.Info("Weird");
            return View();
        }

    }
}