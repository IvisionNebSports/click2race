﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CsvHelper;
using EmailQueueSystem;
using marathonrunners.Models;
using marathonrunners.Models.DiscountService;
using marathonrunners.Utils;
using Marathonrunners.Core;
using Marathonrunners.Core.Entities;
using Marathonrunners.Logging;
using Marathonrunners.Models;
using Marathonrunners.Utils;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using SMSIntegrationSystem;

namespace Marathonrunners.Controllers
{

    [Authorize]
    public class RegisterController : Controller
    {
        private readonly ILogger _logger;
        private readonly IAccountRepository _accountRepository;
        private readonly IEmailRepository _emailRepository;
        private readonly IEnumerable<string> _listOfPreviousYearBMUsers;

        public RegisterController(IAccountRepository accountRepository, ILogger logger, IEmailRepository emailRepository)
        {
            _accountRepository = accountRepository;
            _logger = logger;
            _emailRepository = emailRepository;
            _listOfPreviousYearBMUsers = GeLastYearBmUserFromCsv();

        }

        // GET: /Register/
        [AllowAnonymous]
        public ActionResult Index(string eventid)
        {
            switch (eventid)
            {

                case "IDBI_Training_Run2":
                    return View("IDBITraining2");

                case "SPBM_Promo_Run":
                    return View("SPBMPromoRun2017");

                case "SPBM_Luxor2017":
                    return View("SpbmLuxor");

                case "Hyd_StadRun2017":
                    return View("HUMStadiumRun");

                case "HUM5K_2017":
                    // return View("HUMLaunchRun");
                    return View("RegistrationClosed");

                case "IDBILIMHM_Training_Run":
                    //  return View("IDBITrainingRun17");
                    return View("RegistrationClosed");
                case "IDBI_Promo_Run":
                    return View("RegistrationClosed");

                case "spbm2017":
                    return View("SPBM17");

                case "idbimhm_2017":
                    return View("IDBIMHM2017");
                case "b10k_2017":
                    return View("B10KChallenge");
                case "sfa_2017":
                    return View("RegistrationClosed");
                case "nebrun_2017":
                    return View("NEBTestRun");
                case "tc_2017":
                    return View("RegistrationClosed");
                case "bm_2015":
                    return View("RegistrationClosed");
                case "spice_2015":
                    return View("RegistrationClosed");
                case "mysore_2015":
                    return View("RegistrationClosed");
                case "apr_2015":
                    return View("RegistrationClosed");
                case "spbm_stad_run17":
                    return View("SpbmStadium");
                case "bang1_2015":
                    return View("RegistrationClosed");
                case "bang2_2015":
                    return View("BANGALORE2");
                case "training5_2017":
                    return View("TRAINING5");
                case "sky_2015":
                    return View("RegistrationClosed");
                case "spice_fed_2015":
                    return View("RegistrationClosed");
                case "psnrun_2015":
                    return View("PSNRUN");
                case "bull_2015":
                    return View("Bull");
                case "coorg1_2015":
                    return View("RegistrationClosed");
                case "pedal_2015":
                    return View("RegistrationClosed");
                case "pedal_cxo_2015":
                    return View("RegistrationClosed");
                case "tracs_2016":
                    return View("RegistrationClosed");
                case "run_2015":
                    return View("RegistrationClosed");
                case "kkh_2015":
                    return View("RegistrationClosed");
                case "trot_workshop_2016":
                    return View("RegistrationClosed");
                case "tc_2016":
                    return View("RegistrationClosed");
                case "cwdr_2016":
                    return View("RegistrationClosed");
                case "rfh_2016":
                    return View("RegistrationClosed");
                case "kr_relay_2016":
                    return View("RegistrationClosed");
                case "trot_2016":
                    return View("TROT");
                case "brur_2016":
                    return View("RegistrationClosed");
                case "sfa_2016":
                    return View("RegistrationClosed");
                case "climb_2016":
                    return View("RegistrationClosed");
                default: return View();
            }
        }

        [RequireHttps]
        public ActionResult Enroll(string eventid)
        {
            var user = _accountRepository.GetUserById(User.Identity.Name);

            switch (eventid)
            {
                case "b10k_2017":
                    return View("EnrollB10KChallenge");
                case "sfa_2017":
                    return View("RegistrationClosed");
                case "tc_2017":
                    return View("RegistrationClosed");

                case "bm_2015":
                    ViewBag.EventId = eventid;
                    return View("RegistrationClosed");
                case "spice_2015":
                    return View("RegistrationClosed");
                case "mysore_2015":
                    return View("RegistrationClosed");
                case "run_2015":
                    return View("RegistrationClosed");
                //case "pune_2015":
                //case "mum_2015":
                //case "coch_2015":
                //case "delhi_2015":
                case "bang1_2015":
                    return PaymentPromo(eventid);
                case "bang2_2015":
                    return PaymentPromo(eventid);
                case "training5_2017":
                    return PaymentPromo(eventid);
                case "sky_2015":
                    return View("RegistrationClosed");
                case "psnrun_2015":
                    return View("RegistrationClosed");
                case "bull_2015":
                    return View("RegistrationClosed");
                case "tracs_2015":
                    return View("RegistrationClosed");
                case "coorg1_2015":
                    return View("RegistrationClosed");
                case "sfa_2016":
                    return View("EnrollStridesforAutism", user);
                default: return View("Index");
            }
        }

        //TODO: make it generic
        [RequireHttps]
        public ActionResult PaymentPromo(string eventId)
        {
            var eventNames = new Dictionary<string, string>(){
                {"promo1_2015", "SPBM 2015 - Hyderabad Promo Run"},
                {"pune_2015", "SPBM 2015 - Pune Promo Run"},
                {"mum_2015", "SPBM 2015 - Mumbai Promo Run"},
                {"coch_2015", "SPBM 2015 - Cochin Promo Run"},
                {"gur_2015", "SPBM 2015 - Gurgaon Promo Run"},
                {"bang2_2015", "SPBM 2015 - Taper Down with Yoga"},
                {"training5_2017", "IDBI Federal New Delhi Marathon Adidas Runners X Training Run – 5"},
                  {"bang1_2015", "SPBM 2015 - Family Fun Event"}
            };
            var userid = User.Identity.Name;
            if (userid == null)
            {
                return RedirectToAction("LogOff", "Account");
            }
            var runreg = new BMUserRunRegistration2015()
            {
                FinalPrice = 0,
                Status = true,
                UserId = userid,
                RegistrationDate = DateTime.Now,
                RunCategoryId = eventNames[eventId],
            };
            if (_accountRepository.AlreadyRegisteredBM2015PromoRun(runreg))
            {
                ViewBag.ErrorMessage = "You have already registered for this event!";
                return View("Error");
            }
            int dbid = _accountRepository.RegisterBM2015UserRun(runreg);
            var user = _accountRepository.GetUserById(userid);
            var transactionId = "C2R-" + dbid;
            SendSuccessConfirmationMail(user, eventId, runreg.RunCategoryId, transactionId, 0);
            ViewBag.TransactionId = transactionId;
            return PartialView("TransactionSuccessfull");
        }

        [HttpPost]
        [RequireHttps]
        public ActionResult PaymentInit(PayInitJsonData jsoninfo)
        {
            var eventid = jsoninfo.EventId;
            var user = _accountRepository.GetUserById(User.Identity.Name);
            var payInit = new PayInitViewModel();

            int eventSelection;
            PayInitViewModel payinit2;
            switch (eventid)
            {
                // TODO: Get all these details from the database

                case "b10k_2017":
                    var eventData = EventData.GetEventDetails(eventid);
                    eventSelection = jsoninfo.EventType;
                    payinit2 = new PayInitViewModel()
                    {
                        Title = eventData.EventTitle,
                        DateOfEvent = eventData.DataOfEvent,
                        EmailId = user.EmailId,
                        FullName = user.FirstName + " " + user.LastName,
                        EventId = eventid,
                        Location = eventData.Location,
                        EventType = eventData.EventCategoryList[eventSelection - 1].EventTitle,
                        Fee = Convert.ToInt32(eventData.EventCategoryList[eventSelection - 1].Price)
                    };
                    return PartialView("PayInit", payinit2);

                case "sfa_2017":
                    var eventData17 = EventData.GetEventDetails(eventid);
                    eventSelection = jsoninfo.EventType;
                    payinit2 = new PayInitViewModel()
                    {
                        Title = eventData17.EventTitle,
                        DateOfEvent = eventData17.DataOfEvent,
                        EmailId = user.EmailId,
                        FullName = user.FirstName + " " + user.LastName,
                        EventId = eventid,
                        Location = eventData17.Location,
                        EventType = eventData17.EventCategoryList[eventSelection - 1].EventTitle,
                        Fee = Convert.ToInt32(eventData17.EventCategoryList[eventSelection - 1].Price)
                    };
                    return PartialView("PayInit", payinit2);

                // return PartialView("PayInit", payinit2); break;
                default: return PartialView("Index");


            }
        }

        [HttpPost]
        [RequireHttps]
        [ValidateAntiForgeryToken]
        public ActionResult PaymentConfirm(PaymentViewModel paymodel)
        {
            //TODO: HIGH ALERT take event data from EventData do not take it from client side high chances of hacking
            if (!ModelState.IsValid)
            {
                throw new Exception("Payment module has found a problem!!");
            }
            var user = _accountRepository.GetUserById(paymodel.EmailId);
            return SendToPayUMultiReg(
                new List<EventBooking>() {
                    new EventBooking() {
                        AdditionalItems = Enumerable.Empty<List<AdditionalItemSelection>>().ToList(),
                        Event = paymodel,
                        IsFastLane = false,
                        Users = new List<UserInfo>() {user}
                    }
                }, user);
        }

        // UDF1 = EventID
        //[HttpPost]
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [RequireHttps]
        public ActionResult PaymentSuccess()
        {
            try
            {

                const string hashSeq = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

                if (Request.Form["status"] == "success")
                {
                    string[] mercHashVarsSeq = hashSeq.Split('|');
                    Array.Reverse(mercHashVarsSeq);
                    string mercHashString = ConfigurationManager.AppSettings["SALT"] + "|" + Request.Form["status"];

                    foreach (string mercHashVar in mercHashVarsSeq)
                    {
                        mercHashString += "|";
                        mercHashString = mercHashString + (Request.Form[mercHashVar] ?? "");
                    }
                    string merc_hash = HashGenerator.Generatehash512(mercHashString).ToLower();

                    if (merc_hash != Request.Form["hash"])
                    {
                        ViewBag.ErrorMessage = "Transaction failed: Hash value did not matched. Data seems to be tampered. Report to support@click2race.com";
                        return PartialView("TransactionFailure");
                        //Hash value did not matched
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Transaction failed: For more info report to support@click2race.com";
                    return PartialView("TransactionFailure");
                    // osc_redirect(osc_href_link(FILENAME_CHECKOUT, 'payment' , 'SSL', null, null,true));
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Transaction failed: " + ex.Message;
                return PartialView("TransactionFailure");
            }
            ViewBag.TransactionId = Request.Form["txnid"];
            ViewBag.EventId = Request.Form["udf1"];
            string emaildId = Request.Form["email"];
            long mobileNumber = 0L;
            UserRunRegistration userRunRegistration = _accountRepository.GetRunRegistrationByTransactionId(ViewBag.TransactionId);
            if (userRunRegistration.IsFastLane)
            {
                var fastLaneEntry =
                    _accountRepository.GetFastLaneRegistrationByTransactionId(userRunRegistration.FastLaneId);
                userRunRegistration.Status = true;
                //fastLaneEntry.Status = true; TODO: First fix the auto emailqueuer setting the status to set it for fast lane
                fastLaneEntry.BankRefNumber = Request.Form["bank_ref_num"];
                fastLaneEntry.PayUMoneyId = Request.Form["payuMoneyId"];
                fastLaneEntry.AmountSplit = Request.Form["amount_split"];
                _accountRepository.UpdateRunRegistration(userRunRegistration);
                _accountRepository.UpdateFastLaneRegistration(fastLaneEntry);
                mobileNumber = Convert.ToInt64(fastLaneEntry.MobileNumber);
            }
            else
            {
                userRunRegistration = _accountRepository.GetRunRegistrationByTransactionId(ViewBag.TransactionId);
                userRunRegistration.Status = true;
                userRunRegistration.BankRefNumber = Request.Form["bank_ref_num"];
                userRunRegistration.PayUMoneyId = Request.Form["payuMoneyId"];
                userRunRegistration.AmountSplit = Request.Form["amount_split"];
                userRunRegistration.RunCategoryId = Request.Form["productinfo"];
                _accountRepository.UpdateRunRegistration(userRunRegistration);
                var user = _accountRepository.GetUserById(emaildId);
                mobileNumber = user.MobileNumber;
            }
            TransactionVerification.Instance.SetEntryToBeSuccessfull(ViewBag.TransactionId, new TransactionInfo()
            {
                TransactionId = ViewBag.TransactionId,
                AmountSplit = Request.Form["amount_split"],
                PayuMoneyId = Request.Form["payuMoneyId"],
                BankRefNum = Request.Form["bank_ref_num"]
            });

            if (Request.Form["udf1"] == "spbm_stad_run17" || Request.Form["udf1"] == "Hyd_StadRun2017")
            {
                return PartialView("TransactionSuccessfull");
            }
            else
            {
                SendRegistrationMsg(mobileNumber.ToString(), Request.Form["udf1"], Request.Form["txnid"]);
                return PartialView("TransactionSuccessfull");
            }
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [RequireHttps]
        [AllowAnonymous]
        public ActionResult PaymentSuccessFastLane()
        {
            try
            {
                const string hashSeq = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

                if (Request.Form["status"] == "success")
                {
                    string[] mercHashVarsSeq = hashSeq.Split('|');
                    Array.Reverse(mercHashVarsSeq);
                    string mercHashString = ConfigurationManager.AppSettings["SALT"] + "|" + Request.Form["status"];

                    foreach (string mercHashVar in mercHashVarsSeq)
                    {
                        mercHashString += "|";
                        mercHashString = mercHashString + (Request.Form[mercHashVar] ?? "");
                    }
                    string merc_hash = HashGenerator.Generatehash512(mercHashString).ToLower();

                    if (merc_hash != Request.Form["hash"])
                    {
                        ViewBag.ErrorMessage = "Transaction failed: Hash value did not matched. Data seems to be tampered. Report to support@click2race.com";
                        return PartialView("TransactionFailure");
                        //Hash value did not matched
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Transaction failed: For more info report to support@click2race.com";
                    return PartialView("TransactionFailure");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Transaction failed: " + ex.Message;
                return PartialView("TransactionFailure");
            }
            ViewBag.TransactionId = Request.Form["txnid"];

            TransactionVerification.Instance.SetEntryToBeSuccessfull(ViewBag.TransactionId, new TransactionInfo()
            {
                TransactionId = ViewBag.TransactionId,
                AmountSplit = Request.Form["amount_split"],
                PayuMoneyId = Request.Form["payuMoneyId"],
                BankRefNum = Request.Form["bank_ref_num"]
            });

            if (Request.Form["udf1"] == "spbm_stad_run17" || Request.Form["udf1"] == "Hyd_StadRun2017")
            {
                return PartialView("TransactionSuccessfull");
            }
            else
            {
                SendRegistrationMsg(Request.Form["phone"], Request.Form["udf1"], Request.Form["txnid"]);
                return PartialView("TransactionSuccessfull"); // TODO: Firstname and last name coming wrong from pay guys
            }
        }


        //SMS integration 
        private void SendRegistrationMsg(string mobileNumber, string eventId, string transid)
        {
            string userName = "click2run";
            string passWord = "TMAZ4ZKS";
            string senderId;
            string msg;

            //temporary solution for customized sms ...  repitition need to be changed
            switch (eventId)
            {



                case "IDBI_Training_Run2":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " your registration for IDBI Federal Life Insurance - Adidas Training Run 2 to be held on 28 May'17 at Lokhandwala is now confirmed, ID "
                          + transid + ".";
                    break;

                case "SPBM_Promo_Run":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " your registration for Shriram Properties Bengaluru Marathon - Adidas Training Run 1 to be held on 3 June'17 is now confirmed, ID "
                          + transid + ".";
                    break;

                case "SPBM_Luxor2017":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " your registration for Shriram Properties Bengaluru Marathon - Run in Shriram Luxor to be held on 28 May'17 is now confirmed, ID "
                          + transid + ".";
                    break;

                case "HUM5K_2017":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " your registration for Hyderabad Ultra Marathon 5K Run to be held on 14 May'17 is now confirmed, ID "
                          + transid + ".";
                    break;


                case "IDBILIMHM_Training_Run":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " your registration for IDBI Federal Life Insurance Mumbai Half Marathon Adidas Training Run 1 to be held on 14 May'17 is now confirmed, ID "
                          + transid + ".";
                    break;

                case "IDBI_Promo_Run":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " your registration for IDBI Federal Life Insurance Mumbai Half Marathon Promo Run 2 to be held on 7 May'17 is now confirmed, ID "
                          + transid + ".";
                    break;

                case "spbm2017":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " Your registration for Shriram Properties Bengaluru Marathon to be held on 15th Oct'17 is confirmed,ID "
                          + transid + ". For more info,visit us at http://bit.ly/1PSgSoX";
                    break;

                case "idbimhm_2017":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " Your registration  for IDBI Federal Life Insurance Mumbai Half Marathon to be held on 20 Aug'17 is confirmed, ID "
                          + transid + ". For more info, visit us at goo.gl/LxYsZa";
                    break;
                case "mumb_reg17":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " Your registration for Mumbai Stadium Run to be held on 11 Jun 17 is now confirmed, ID "
                          + transid + ". For more info, visit us at goo.gl/bV68Ha";
                    break;
                case "b10k_2017":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " Your registration for Bengaluru 10K to be held on 9 Jul 17 is now confirmed, ID "
                          + transid + ". For more info, visit us at goo.gl/74L3Si";
                    break;
                case "IDBIMHM_2017":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " Your registration for IDBI Federal Life Insurance Mumbai Half Marathon Inaugural Run to be held on 16 Apr 17 is now confirmed, ID "
                          + transid;
                    break;
                case "SPBM_2017":
                    senderId = "NEBSPT";
                    msg = "Dear Runner,"
                          + " your registration for Shriram Properties Bengaluru Marathon Inaugural Run to be held on 23 Apr 17 is now confirmed, ID "
                          + transid;
                    break;
                case "sfa_2017":
                    senderId = "CLKTRC";
                    msg = "Dear Runner,"
                          + " Your registration for Strides for Autism to be held on 2 Apr 17 is now confirmed, ID "
                          + transid + ". For more info,visit us at goo.gl/nximl8";
                    break;
                case "tc_2017":
                    senderId = "CLKTRC";
                    msg = "Dear Cyclist,"
                          + " Your registration for Trivandrum Cyclathon to be held on 8 Apr 17 is now confirmed, ID "
                          + transid + ". For more info,visit us at goo.gl/5gff1v ";
                    break;
                default:
                    senderId = "CLKTRC";
                    msg = "Dear Runner,"
                          + "Your registration is now confirmed. For further information, kindly contact us at support@click2race.com "
                          + " Regards"
                          + " Team Click2Race";
                    break;
            }
            if (mobileNumber.Length == 10)
            {
                mobileNumber = "91" + mobileNumber;
            }
            var returnResult = SMSIntegration.SendSms(userName, passWord, senderId, mobileNumber, msg);
            if (!returnResult.Item1)
            {
                _logger.Error(string.Format("Failed to send sms to {0} due to error : {1}", mobileNumber, returnResult.Item2));
            }
        }

        private EmailQueue SendSuccessConfirmationMail(UserInfo user, string eventid, string productinfo, string transid, double amount, double? donationAmount = null, string donationCause = null, bool justaddentry = false)
        {
            return SendSuccessConfirmationMail(new List<UserInfo> { user }, eventid, productinfo, transid, amount, donationAmount,
                donationCause, justaddentry);
        }

        private EmailQueue SendSuccessConfirmationMail(List<UserInfo> users, string eventid, string productinfo, string transid,
            double amount, double? donationAmount = null, string donationCause = null, bool justaddentry = false)
        {
            var user = users.First();
            var succeeModel = new TransactionSuccessfullModel()
            {
                Name = user.FirstName + " " + user.LastName,
                Address = user.Address,
                EventName = productinfo,
                Amount = Math.Abs(amount) <= 0.00 ? "Free" : amount + " INR",
                OriginalAmountAsDouble = amount,
                Mobile = user.MobileNumber.ToString(),
                TransactionId = transid,
                DateAndTime = DateTime.Now,
                EventId = eventid,
                Donation = donationAmount != null && donationAmount != 0.0,
                DonationAmount = donationAmount,
                DonationCause = donationCause,
                Participants = users,
                EmergencyContactName = user.EmergencyContactName,
                EmergencyContactNumber = user.EmergencyContactNumber.ToString(),
                APRSubCategory = ""
            };
            string mailHtml;
            mailHtml = ViewToString.RenderViewToString(this.ControllerContext, eventid == "cwdr_2016" ? "SuccessfulEmailConfirmationCWDR" : "SuccessfulEmailConfirmation", succeeModel);
            var email = new EmailQueue()
            {
                EmailTo = user.EmailId,
                EmailFrom = eventid == "b10k_2017" ? "Team SPBM <team@bengalurumarathon.in>" : "Click2Race <support@click2race.com>",
                Subject = "Registration Confirmation",
                Message = mailHtml,
                EmailDateIn = DateTime.Now,
                EmailType = EmailTypeEnum.RegistrationConfirmation,
                EmailVerified = true,
                EmailStatus = EmailStatusEnum.Queued
            };
            if (!justaddentry)
            {
                var emailRef = _emailRepository.QueueEmail(email);
                EmailQueuer.Instance.QueueIt(emailRef);
            }

            return email;

        }

        [HttpPost]
        [AllowAnonymous]
        [RequireHttps]
        public ActionResult PaymentError()
        {
            ViewBag.ErrorMessage = "Transaction failed: " + Request.Form["error_Message"] + " For more info report to support@click2race.com";
            return PartialView("TransactionFailure");

        }

        [HttpPost]
        [RequireHttps]
        public bool CheckDuplicateRegistration(PayInitJsonData jsoninfo)
        {
            var eventid = jsoninfo.EventId;
            var generatedRunCategoryId = "";
            //TODO: Fix this stupid ugly stuff
            switch (eventid)
            {
                case "bc_10k":
                    {
                        var events = new[] { "10K", "5K" };
                        generatedRunCategoryId = "Bengaluru Challenge" + " " + events[jsoninfo.EventType - 1];
                        return _accountRepository.IsDuplicateRegistrationBM2015(User.Identity.Name, generatedRunCategoryId);
                    }
                case "trot_2015":
                    {
                        var events = new[] { "10k", "21.1k", "42.19k" };
                        generatedRunCategoryId = "TRORT" + " " + events[jsoninfo.EventType - 1];
                        break;
                    }
                case "pedal_2015":
                    {
                        var eventDetails = EventData.GetEventDetails(eventid);
                        var user = _accountRepository.GetUserById(User.Identity.Name);
                        return IsMultipleRegistration(user.EmailId, "", "", eventDetails);
                    }
                default: { return false; }
            }

            return _accountRepository.IsDuplicateRegistration(User.Identity.Name, generatedRunCategoryId);
        }

        [AllowAnonymous]
        public ActionResult FastLane(string eventId)
        {
            var eventData = EventData.GetEventDetails(eventId);
            if (eventData == null)
            {
                ViewBag.ErrorMessage = "Hmm! Seems like we could not find the event with Id " + eventId;
                return View("Error");
            }
            else if (eventId == "IDBIMHM_2017" || eventId == "tc_2017" || eventId == "sfa_2017" || eventId == "SPBM_2017" || eventId == "IDBI_Promo_Run" || eventId == "HUM5K_2017" || eventId == "IDBILIMHM_Training_Run")
            {
                ViewBag.ErrorMessage = "Event Registration is closed!!!";
                return View("RegistrationClosed");
            }
            else
            {
                ViewBag.EventDetails = eventData;
                return View("FastLanePayInitNew");
            }
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult MultiForms(string eventId, int eventCategory, int numberofforms)
        {
            var eventData = EventData.GetEventDetails(eventId);
            if (eventData == null)
            {
                ViewBag.ErrorMessage = "Hmm! Seems like we could not find the event with Id " + eventId;
                return View("Error");
            }
            ViewBag.EventDetails = eventData;
            ViewBag.NumberOfForms = numberofforms;
            ViewBag.EventCategoryIndex = eventCategory;
            return View("FastLanePartial");
        }

        [HttpPost]
        [RequireHttps]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PaymentConfirmFastLane()
        {
            try
            {
                int numberOfForms = Convert.ToInt32(Request.Form["NumberOfForms"]);
                string eventId = Request.Form["EventId"];
                int eventCategory = Convert.ToInt32(Request.Form["EventCategory"]);
                var eventData = EventData.GetEventDetails(eventId);
                if (eventData == null)
                {
                    ViewBag.ErrorMessage = "Hmm! Seems like we could not find the event with Id " + eventId;
                    return View("Error");
                }
                var eventCategoryData = eventData.EventCategoryList[eventCategory];
                var userInfo = new UserInfo();
                var eventbooking = new EventBooking();
                var paymentVM = new PaymentViewModel();
                paymentVM.EventId = eventData.EventId;
                paymentVM.EventTitle = eventData.EventTitle;
                paymentVM.EventType = eventCategoryData.EventTitle;
                paymentVM.EventDate = Convert.ToDateTime(eventData.DataOfEvent);
                double actualAmount = 0.0;
                if (eventData.IsDonationEvent)
                {
                    // In case of Donation we assume that there will be always one form
                    var donationAmount = Convert.ToInt32(Request.Form["DonationAmount 0"]);
                    if (donationAmount < eventData.MinDonationAmount)
                    {
                        ViewBag.ErrorMessage = "Minimum donation amount is " + eventData.MinDonationAmount + " INR";
                        return View("Error");
                    }
                    if (donationAmount > eventData.MaxDonationAmount)
                    {
                        ViewBag.ErrorMessage = "Maximum donation amount is " + eventData.MaxDonationAmount + " INR";
                        return View("Error");
                    }
                    paymentVM.Amount = "-1";
                    paymentVM.Donation = donationAmount;
                    paymentVM.DonationTo = eventData.DonationTo;
                }
                else
                {
                    // Calculation if price is per category then we use the same amoutn else we take the price to be per user and multiply by number of users
                    actualAmount = eventCategoryData.Price * (eventCategoryData.PricePerUser ? numberOfForms : 1);
                    //Discount Logic Here
                    var promocode = Request.Form["PromoCode"] != "" ? Request.Form["PromoCode"] : "NONE";
                    if (promocode != "NONE" && eventCategoryData.Discount != null)
                    {
                        var eventPromo = eventCategoryData.Discount.First();
                        var eventPromoCode = eventPromo.CouponCode;
                        if (promocode != eventPromoCode)
                        {
                            ViewBag.ErrorMessage = "PROMO code " + promocode + " incorrect! Please go back and try again!";
                            return View("Error");
                        }
                        if (DateTime.Now < eventPromo.StartDateUtc || DateTime.Now > eventPromo.EndDateUtc)
                        {
                            ViewBag.ErrorMessage = "PROMO code " + promocode + " has expired! Please go back and try again!";
                            return View("Error");
                        }
                        actualAmount = actualAmount - (double)eventCategoryData.Discount.First().DiscountAmount;
                        paymentVM.PromoCode = eventPromoCode;
                    }

                }
                eventbooking.Event = paymentVM;
                eventbooking.Users = new List<UserInfo>();
                eventbooking.AdditionalItems = new List<List<AdditionalItemSelection>>();
                //  for (int index = 0; index < numberOfForms; index++) {
                var user = new UserInfo();
                user.Gender = !eventCategoryData.HiddenFields.Contains(Fields.title) ? Convert.ToInt32(Request.Form["Title"]) : 0;
                user.FirstName = Request.Form["FirstName"];
                user.LastName = Request.Form["LastName"];
                user.DOB = !eventCategoryData.HiddenFields.Contains(Fields.DateOfBirth) ? Convert.ToDateTime(Request.Form["DateOfBirth"]) : DateTime.Parse("1970/01/01");
                //TODO: Check the format of the date
                user.EmailId = Request.Form["EmailAddress"];
                user.MobileNumber = !eventCategoryData.HiddenFields.Contains(Fields.MobileNum) ? Convert.ToInt64(Request.Form["MobileNum"]) : 0;
                user.Address = Request.Form["Address"];
                user.ShirtSzie = Request.Form["ShirtSize"];
                user.BloodGroup = Request.Form["BloodGroup"];
                user.EmergencyContactName = Request.Form["EmergencyContactName"];
                user.EmergencyContactNumber = !eventCategoryData.HiddenFields.Contains(Fields.EmergencyContact) ? Convert.ToInt64(Request.Form["EmergencyContactNumber"]) : 0;
                user.Occupation = Request.Form["OrganizationName"];
                user.NameOnBib = Request.Form["NameOnBib"];
                if (eventId == "cwdr_2016")
                {
                    user.ChronicDisease = Request.Form["BibCollect"] == "NOT DEFINED" ? "" : Request.Form["BibCollect"];
                }
                //  if (index == 0) {
                userInfo = user; // We add the first user as the guy who pays the bill
                // }
                if (eventCategoryData.AdditionalItems != null)
                {
                    var addItems = new List<AdditionalItemSelection>();
                    for (int i = 0; i < eventCategoryData.AdditionalItems.Count(); i++)
                    {
                        var addItem = new AdditionalItemSelection();
                        var item = Request.Form["AdditionalItem" + " " + i];
                        if (item == null || item != "on")
                        {
                            addItem.ItemIndex = -1;
                        }
                        else
                        {
                            addItem.ItemIndex = i;
                            int quantity = Convert.ToInt32(Request.Form["AdditionalItemQuantity" + " " + i]);
                            addItem.Quantity = quantity;
                            actualAmount += eventCategoryData.AdditionalItems.ToList()[i].Cost * quantity;
                        }
                        addItems.Add(addItem);
                    }
                    eventbooking.AdditionalItems.Add(addItems);
                }

                eventbooking.Users.Add(user);
                // }
                eventbooking.IsFastLane = true;
                paymentVM.Amount = actualAmount.ToString(CultureInfo.InvariantCulture);
                var eventBookingList = new List<EventBooking>() { eventbooking };
                return SendToPayUMultiReg(eventBookingList, userInfo, isFastLane: true);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                throw ex;
            }
        }

        /// <summary>
        /// Sends to payU multi reg.
        /// </summary>
        /// <param name="eventBooking">The event booking.</param>
        /// <param name="payeeUser">The payee user.</param>
        /// <param name="isFastLane">if set to <c>true</c> [is fast lane].</param>
        /// <returns>Transaction Succssfull in case of Free event or Payment View </returns>
        /// <exception cref="System.Exception">Transaction ID was ZERO. Should never happen</exception>
        private ActionResult SendToPayUMultiReg(List<EventBooking> eventBooking, UserInfo payeeUser, bool isFastLane = false)
        {
try{
            var donationEvent = eventBooking.Any(x => EventData.GetEventDetails(x.Event.EventId).IsDonationEvent);
            string totalAmount = "";
            totalAmount = donationEvent ? eventBooking.First().Event.Donation.ToString() :
                eventBooking.Select(z => z.Event.Amount).Aggregate((x, y) => x + y);
            UserInfo user = payeeUser;

            string productinfo = "Click2race event registration";

            List<UserRunRegistration> runRegs = new List<UserRunRegistration>();
            List<FastLaneRegistration> fastLaneRegs = new List<FastLaneRegistration>();
            var registrationDate = DateTime.Now;
            var isfreeEvent = eventBooking.Any(x => x.Event.Amount.Equals("0"));
            foreach (var booking in eventBooking)
            {
                var eventDetails = EventData.GetEventDetails(booking.Event.EventId);
                var eventCategory = eventDetails.EventCategoryList.FirstOrDefault(x => x.EventTitle == booking.Event.EventType);
                string runCategoryId = booking.Event.EventTitle + " " + booking.Event.EventType;
                double regAmount = eventDetails.IsDonationEvent ? booking.Event.Donation : Convert.ToDouble(booking.Event.Amount);
                Discount discount = booking.Event.PromoCode == null || booking.Event.PromoCode == "NONE" ? null : eventCategory.Discount.FirstOrDefault(x => x.CouponCode == booking.Event.PromoCode);
                double discountAmount = discount == null ? 0.0 : discount.DiscountAmount;
                bool isGroupReg = booking.Users.Count > 1;
                string groupRegId = "";
                if (isGroupReg)
                {
                    groupRegId = GenerateUniqueTransactionId(booking.Users.First().EmailId, payeeUser.FirstName);
                }
                int outerIndex = 0;
                foreach (var usr in booking.Users)
                {
                    var userRun = new UserRunRegistration()
                    {
                        TransactionId = "NOT USED ANYMORE",
                        UserId = usr.EmailId,
                        RunCategoryId = runCategoryId,
                        RegistrationDate = registrationDate,
                        FinalPrice = regAmount,
                        IsGroupReg = isGroupReg,
                        GroupRegId = groupRegId,
                        Discounts = discountAmount,
                        PromoCode = booking.Event.PromoCode,
                        SubCategory = "NOT APPLICABLE",
                        ExtraField1 = "NOT APPLICABLE",
                        BibCollectionLocation = eventDetails.EventId == "cwdr_2016" ? usr.ChronicDisease : "NOT APPLICABLE",
                    };
                    if (isfreeEvent)
                        userRun.Status = true;

                    if (booking.AdditionalItems.Count > outerIndex)
                    {
                        var addtionalItems = booking.AdditionalItems[outerIndex];
                        if (addtionalItems.Count > 0)
                        {
                            IList<UserAdditionalItemSelection> listOfAddItems = new List<UserAdditionalItemSelection>();
                            foreach (var addtionalItem in addtionalItems)
                            {
                                if (addtionalItem.ItemIndex >= 0)
                                {
                                    var item = eventCategory.AdditionalItems.ToList()[addtionalItem.ItemIndex];
                                    listOfAddItems.Add(new UserAdditionalItemSelection()
                                    {
                                        Name = item.Name,
                                        Quantity = addtionalItem.Quantity,
                                        RunCategory = runCategoryId
                                    });
                                }
                            }
                            userRun.UserAdditionalItemSelection = listOfAddItems;
                        }
                        outerIndex++;
                    }

                    if (booking.IsFastLane)
                    {
                        var fastLaneId = GenerateUniqueTransactionId(usr.EmailId, usr.FirstName + "FASTLANE");
                        userRun.IsFastLane = true;
                        userRun.FastLaneId = fastLaneId;
                        var fastLaneReg = new FastLaneRegistration()
                        {
                            FirstName = usr.FirstName,
                            LastName = usr.LastName,
                            EmailId = usr.EmailId,
                            Address = usr.Address,
                            Amount = eventDetails.IsDonationEvent ?
                            booking.Event.Donation.ToString(CultureInfo.InvariantCulture) : regAmount.ToString(CultureInfo.InvariantCulture),
                            BloodGroup = usr.BloodGroup,
                            Category = runCategoryId,
                            DateOfRegistration = registrationDate,
                            DOB = usr.DOB,
                            EmergencyContactNumber = usr.EmergencyContactNumber,
                            Gender = usr.Gender,
                            MobileNumber = usr.MobileNumber.ToString(),
                            Organization = usr.Occupation,
                            ShirtSize = usr.ShirtSzie,
                            TransactionId = fastLaneId,
                            IsDonation = eventDetails.IsDonationEvent,
                            NameOnBib = user.NameOnBib,
                            EmergencyContactName = user.EmergencyContactName,
                            City = user.City,
                            Pincode = user.Pincode,
                            IdentityProof = user.IdentityProof,
                            IdNumber = user.IdNumber,
                            PhotoIdProof = user.PhotoIdProof


                        };
                        fastLaneRegs.Add(fastLaneReg);
                    }
                    regAmount = 0.0; // We make the reg amount to be 0 for other group members
                    discountAmount = 0.0;
                    runRegs.Add(userRun);
                }
            }

            int transacId = _accountRepository.RegisterUserRunMultiReg(runRegs, fastLaneRegs);
            if (transacId == 0)
            {
                throw new Exception("Transaction ID was ZERO. Should never happen");
            }
            string c2Rcustomertransactionid = "C2R-" + transacId;



            if (isfreeEvent)
            {
                ViewBag.TransactionId = c2Rcustomertransactionid;


                SendConfirmationMailWithoutTransactionVerification(c2Rcustomertransactionid, eventBooking);
                SendRegistrationMsg(fastLaneRegs.First().MobileNumber, eventBooking.First().Event.EventId, c2Rcustomertransactionid);

                if (eventBooking.First().Event.EventId == "IDBIMHM_2017" || eventBooking.First().Event.EventId == "SPBM_2017")
                {

                    return PartialView("RegistrationSuccessfull");
                }
                else
                {
                    return PartialView("TransactionSuccessfull");
                }
            }

            var transModel = new TransactionViewModel();
            transModel.TxnId = c2Rcustomertransactionid;

            transModel.EventId = eventBooking.First().Event.EventId;
            transModel.EventDate = DateTime.Now;
            //transModel.TxnId = GenerateUniqueTransactionId(user.EmailId, user.FirstName);
            transModel.Key = ConfigurationManager.AppSettings["MERCHANT_KEY"];
            transModel.FirstName = user.FirstName;
            transModel.LastName = user.LastName;
            transModel.ProductInfo = productinfo;
            transModel.Address = user.Address;
            transModel.Amount = totalAmount;
            transModel.EmailId = user.EmailId;
            transModel.City = user.City;
            transModel.State = user.State;
            transModel.Country = user.Country;
            transModel.Phone = user.MobileNumber;
            transModel.ServiceProvider = ConfigurationManager.AppSettings["SERVICE_PROVIDER"];
            transModel.Surl = ConfigurationManager.AppSettings[isFastLane ? "TRANS_SUCCESS_FASTLANE_URL" : "TRANS_SUCCESS_URL"];
            transModel.Furl = ConfigurationManager.AppSettings["TRANS_FAILURE_URL"];
            transModel.Hash = HashGenerator.Generatehash512(transModel.Key + "|" + transModel.TxnId + "|" + transModel.Amount + "|" +
                                        productinfo + "|" + transModel.FirstName + "|" + transModel.EmailId + "|" +
                                        transModel.EventId + "|" + transModel.EventDate + "|||||||||" +
                                        ConfigurationManager.AppSettings["SALT"]);
            if (transModel.EventId == "spbm_stad_run17" || transModel.EventId == "Hyd_StadRun2017")
            {
                return PartialView("PostPayU", transModel);
            }
            else
            {
                SendConfirmationMail(transModel.TxnId, eventBooking);
                return PartialView("PostPayU", transModel);
            }
        }
catch (Exception ex)
{
    ViewBag.ErrorMessage = ex.Message;
    throw ex;
}
        }
        [HttpGet]
        [RequireHttps]
        [AllowAnonymous]
        // Returns {isError:true/false, errorMessage:"", isDuplicate:true/false}
        public string IsDuplicateRegistrationFastLane(string userid, string eventId, int eventCategory)
        {
            try
            {
                if (userid.IsNullOrWhiteSpace() || userid.IsNullOrWhiteSpace() || eventCategory < 0)
                    throw new Exception("Arguments cannot be null or empty!");
                var eventData = EventData.GetEventDetails(eventId);
                var eventTitle = eventData.EventTitle;// to get the main title 

                var eventCategoryTitle = EventData.GetEventDetails(eventId)
                    .EventCategoryList.ElementAt(eventCategory)
                    .EventTitle;

                bool isDuplicate = _accountRepository.IsDuplicateRegistrationFastLane(userid, eventId, eventCategoryTitle);
                return Utility.AnonymousObjectToString(new
                {
                    IsError = false,
                    IsDuplicate = isDuplicate,
                    ErrorMessage = isDuplicate ?
                    "You are already registered for the event " + eventTitle +
                    ". Please check your inbox for an email confirmation." +
                    " If you wish to register another participant, please use a different email ID." +
                    " For any queries, write to us at support@click2race.com." : ""
                });
            }
            catch (Exception exception)
            {
                return Utility.AnonymousObjectToString(new
                {
                    IsError = true,
                    ErrorMessage = "System Error:" + exception.Message,
                    IsDuplicate = false
                });
            }
        }

        private void SendConfirmationMail(string transactionId, List<EventBooking> eventBookings)
        {
            var listOfEmail = Utils.EmailHandler.GetListOfEmailQueueFromEvents(this.ControllerContext,
                transactionId, eventBookings);
            TransactionVerification.Instance.AddEntry(transactionId, new TransactionMetadata()
            {
                TimeOfEntry = DateTime.Now,
                Operation = _accountRepository.UpdateTransactionStatus, //TODO: May be pass it as parameter so that other operations can be done?
                EmailQueueEntries = listOfEmail
            });
        }

        private void SendConfirmationMailWithoutTransactionVerification(string transactionId, List<EventBooking> eventBookings)
        {
            var listOfEmail = Utils.EmailHandler.GetListOfEmailQueueFromEvents(this.ControllerContext,
                transactionId, eventBookings);
            Utils.EmailHandler.AddMailsToTheSendingQueue(_emailRepository, listOfEmail);
        }

        private bool IsMultipleRegistration(string emailId, string firstname, string lastname, EventDetails eventData, bool fastLane = false)
        {
            return fastLane ? _accountRepository.IsMultipleRegistrationForFastLane(emailId, firstname, lastname, eventData.EventTitle) :
                _accountRepository.IsMultipleRegistrationForRegisteredUser(emailId, firstname, lastname, eventData.EventTitle);
        }

        public JsonResult CheckPremiumMember(string organiser, string emailid)
        {
            JObject o1 = JObject.Parse(System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/PartnerData/RFLPremium/PremiumMembers.json")));
            var IspremiumMember =
                o1.GetValue("premiummembers")
                    .Values<string>()
                    .Any(x => x.Equals(emailid, StringComparison.InvariantCultureIgnoreCase));
            if (IspremiumMember)
            {
                return Json(new
                {
                    Success = true,//error
                    StatusMessage = "You are a registered premium member."
                });
            }

            return Json(new
            {
                Success = false,//error
                StatusMessage = "You are not a registered premium member."
            });
        }

        private bool IsLastYearBMUser(string emailId)
        {
            return _listOfPreviousYearBMUsers != null && _listOfPreviousYearBMUsers.Contains(emailId);
        }

        private IEnumerable<string> GeLastYearBmUserFromCsv()
        {
            TextReader reader = System.IO.File.OpenText(HttpContext.Server.MapPath("~/PartnerData/BMDiscount/listOfPreviousBMUsers.csv"));
            var parser = new CsvParser(reader);
            while (true)
            {
                var row = parser.Read();
                if (row == null)
                    break;
                yield return row[0].ToLowerInvariant();
            }
        }

        private string GenerateUniqueTransactionId(string emailId, string name)
        {
            var rnd = new Random();
            var stringhash = HashGenerator.Generatehash512(emailId + rnd.Next() + name + DateTime.Now + Guid.NewGuid());
            return "C2R-" + stringhash.Substring(0, 33).ToUpperInvariant();
        }
    }
}
