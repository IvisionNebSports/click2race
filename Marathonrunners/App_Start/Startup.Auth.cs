﻿using System;
using Marathonrunners.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;

namespace Marathonrunners {
    public partial class Startup {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app) {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseYahooAuthentication(
            //    consumerKey:
            //        "dj0yJmk9alE3RHJ2ZWVSM1lwJmQ9WVdrOVZVRnFabFZ4TkRnbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD01ZQ--",
            //    consumerSecret: "6f69923cb6d0606eb20fb32ed19af904078f298a");

            //Testing
            //app.UseYahooAuthentication(
            //   consumerKey:
            //       "dj0yJmk9RkxycjlNYXRCTTAzJmQ9WVdrOVRVaGlZWFJ0TkcwbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1mYQ--",
            //   consumerSecret: "c0476cea4c840e390c498c8bff035a303135b79b");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");
            //TODO: Put it in more secure place
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions() {
                ClientId = "70799403967-tgu2d49kea3mh1qldhvd3ooev05anf0l.apps.googleusercontent.com",
                ClientSecret = "wvToQJrA7P2AL8cMuYh7m9nb"
            });
        }
    }
}
