﻿using System.Web;
using System.Web.Optimization;

namespace Marathonrunners
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/scripts/jquery-{version}.js",
                        "~/Content/scripts/jquery.slides.min.js",
                        "~/Content/scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/scripts/jquery.validate*",
                        "~/Content/scripts/jquery.validate.unobtrusive.min.js",
                        "~/Content/scripts/additional-methods.js",
                        "~/Content/scripts/jquery.passstrength.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryajax").Include(
                        "~/Content/scripts/jquery.unobtrusive-ajax*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/scripts/bootstrap.js",
                      "~/Content/scripts/respond.js",
                      "~/Content/scripts/jquery.bpopup.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/multiforms").Include(
                      //"~/Content/scripts/jquery-2.1.0.js",
                      //"~/Content/scripts/jquery.validate.js",
                      "~/Content/scripts/jquery.validate.unobtrusive.min.js",
                      "~/Content/scripts/base.js"));

            bundles.Add(new ScriptBundle("~/bundles/util").Include(
                      "~/Content/scripts/base.js",
                      "~/Content/scripts/countries.js",
                      "~/Content/scripts/conditional-validation.js",
                      "~/Content/scripts/jquery.fileupload.js",
                      "~/Content/scripts/jquery.ui.widget.js",
                      "~/Content/scripts/updater.js",
                      "~/Content/scripts/transaction.js",
                      "~/Content/scripts/mailgun_validator.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.css",
                     "~/Content/css/base.css",
                      "~/Content/css/bootstrap-overrides.css",
                      "~/Content/css/jquery-ui.css",
                      "~/Content/font-awesome-4.1.0/css/font-awesome.css",
                      "~/Content/font-icons/flaticon.css",
                      "~/Content/css/jquery.fileupload.css",
                      "~/Content/css/bootstrap-social.css"));
        
         bundles.Add(new StyleBundle("~/Content/base").Include(
                      "~/Content/css/base.min.css"));
        }
    }
}
