﻿using System;
using Hangfire;
using Hangfire.SqlServer;
using JobScheduler;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Marathonrunners.Startup))]
namespace Marathonrunners {
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureJobScheduler(app);
            JobSchedulerManager.InitiateDailyReportJob();
            ConfigureAuth(app);
        }

        public void ConfigureJobScheduler(IAppBuilder app) {
            // configure HangFire to use SqlServer for persistent storage of jobs.
            // Set polling to every minute
          

            // enable HangFire dashboard to display information about job and server status
            app.UseHangfireDashboard();

            // ensures processing of HangFire jobs in ASP.NET web app using OWIN + IIS
            app.UseHangfireServer();
        }


    }
}
