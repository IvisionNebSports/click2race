﻿
$(document).ready(function () {
    var oH = $('header').outerHeight();
    var ww = $(window).width();
    if (ww > 768) {
        $('.owl-carousel .owl-item').css('margin-top', oH);
    } else {
        $('.owl-carousel .owl-item').css('margin-top', '0');
    }

    //***************Browser detection******************
    var BrowserDetect = {
        init: function () {
            this.browser = this.searchString(this.dataBrowser) || "Other";
            this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
        },
        searchString: function (data) {
            for (var i = 0; i < data.length; i++) {
                var dataString = data[i].string;
                this.versionSearchString = data[i].subString;

                if (dataString.indexOf(data[i].subString) !== -1) {
                    return data[i].identity;
                }
            }
        },
        searchVersion: function (dataString) {
            var index = dataString.indexOf(this.versionSearchString);
            if (index === -1) {
                return;
            }

            var rv = dataString.indexOf("rv:");
            if (this.versionSearchString === "Trident" && rv !== -1) {
                return parseFloat(dataString.substring(rv + 3));
            } else {
                return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
            }
        },

        dataBrowser: [
            { string: navigator.userAgent, subString: "Chrome", identity: "Chrome" },
            { string: navigator.userAgent, subString: "MSIE", identity: "Explorer" },
            { string: navigator.userAgent, subString: "Trident", identity: "Explorer" },
            { string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
            { string: navigator.userAgent, subString: "Safari", identity: "Safari" },
            { string: navigator.userAgent, subString: "Opera", identity: "Opera" }
        ]

    };

    BrowserDetect.init();
    if ((BrowserDetect.browser === "Explorer" && BrowserDetect.version < 9) ||
        (BrowserDetect.browser === "Chrome" && BrowserDetect.version < 27) ||
        (BrowserDetect.browser === "Firefox" && BrowserDetect.version < 20))
        alert("Your browser version is not supported please upgrade!.");


    //**************************************************

    $('#courosel').slidesjs({
        //width: 900,
        height: 300,
        navigation: false,
        play: {
            active: false,
            auto: true,
            interval: 4000,
            swap: true
        }
    });

    $('#password').passStrengthify({
        minimum: 4, levels: ['', 'Bad', 'Okay', 'Good'],
        colours: ['black', 'pink', '#A26FFF', 'orange']
    });

    //TODO: Move these validations in separate file

    //$("#reg_form").validate({ rules: { other_city: { required: "#other_city_div:visible" } } });

    $(function () {
        load_datepicker();
    });

    $('#uploadid').bind('change', function () {

        //this.files[0].size gets the size of your file.
        if (this.files[0].size > 768000) {
            this.value = '';
            $('#lbluploadid').html('File must not be more than 750kb');
        } else {
            $('#lbluploadid').html('');
        }
    });
    $('#uploadphoto').bind('change', function () {

        //this.files[0].size gets the size of your file.
        if (this.files[0].size > 768000) {
            this.value = '';
            $('#lbluploadphoto').html('File must not be more than 750kb');
        } else {
            $('#lbluploadphoto').html('');
        }
    });

    $('#term_link').bind('click', function (e) {
        e.preventDefault();
        $('#popup').bPopup();
    });

    //$('#term_link_home').bind('click', function (e) {
    //    e.preventDefault();
    //    $('#popup_home').bPopup();
    //});

    load_events();

    menu_section("_UpcomingEvents");
   
   

    $("#accordion").accordion(({ active: false, collapsible: true }));

    $("#input_email").focusout(function () {

    });

    $('#input_email').mailgun_validator({
        api_key: 'pubkey-213c7aa80a0b1f9f655555081b9c81be',  //'pubkey-8sl9iiwioac6w423u42ki6toubu8df48', // replace this with your Mailgun public API key
        in_progress: validation_in_progress,
        success: validation_success,
        error: validation_error,
    });

    //Multiple forms
    $("#EventCategoryFastLane").change(function () {
        $("#wrapper").html("");
        $('#NumberOfParticipantsDiv').html('');
        var selectedItem = $('option:selected', this);
        var selectedValue = selectedItem.attr('value');
       
        if (selectedValue === "") {
            $('#NumberOfParticipantsDiv').html('');
        }
        var userselectiondisabled = selectedItem.attr('user_selection_disabled');
        //var numberOfForms = selectedItem.attr('data-numberofforms');
        var numberOfForms = 1;
        if (userselectiondisabled) {
            var eventId = $('#EventId').val();
            //var selectedCategory = $("#EventCategoryFastLane option:selected").attr('value');
            multiple_forms(eventId, selectedValue, numberOfForms);
            return;
        }
        var html = "";
        //html += "<label class=\"control-label\">Select Participants</label>";
        //html += "<select data-val=\"true\" style=\"display: inline\" data-val-required=\"Number of participants\" id=\"NumberOfParticipants\" name=\"NumberOfParticipantsChange\" onchange=\"numberOfParticipantsChange(this)\">";
        //html += "<option value=\"\">--No. of Participants--</option>";
       

        //<label class="control-label" for="EventCategoryFastLane">Select Category</label>
        //              <div class="controls">
        //                  @*<select data-val="true" data-val-required="Please select the Event Category" id="EventCategoryFastLane" name="EventCategory">
        //                      <option value="">--Select--</option>
        //                    @foreach (var eventd in eventDetails.EventCategoryList.Select((x, index) => new {
        //                        Index = index, x.EventTitle, x.Price, x.NumberOfRegistrtionsAllowed, x.NumberOfUserSelectionDisabled
        //                    })) {
        //                        <option value="@eventd.Index" data-numberofforms="@eventd.NumberOfRegistrtionsAllowed"
        //                        user_selection_disabled="@eventd.NumberOfUserSelectionDisabled">@eventd.EventTitle @eventd.Price INR</option>
        //}
        //</select>

        //var html = "";
        //html += "<label class=\"control-label\">Select Category</label>";
        //html += "<select data-val=\"true\" style=\"display: inline\" data-val-required=\"Please select the Event Category\" id=\"EventCategoryFastLane\" name=\"EventCategory\" onchange=\"EventCategoryFastLaneChange(this)\">";
        //html += "<option value=\"\">--NumberOfUserSelectionDisabled--</option>";


        for (var i = 1; i <= numberOfForms; i++) {
            html += "<option value=\""+ i +"\">"+ i +"</option>";
        }
        html += "</select>";
        $('#NumberOfParticipantsDiv').html(html);
        $('#NumberOfParticipantsDiv').css('display', 'block');
    });


$("[name='spice_coast_family_fun']").change(function () {
    $('#spice_bnk_dtls').val('');
    $('#spice_bank_details').css('visibility', 'hidden');
    $('#spice_bank_details').css('display', 'none');
    var selectedItem = $('option:selected', this);
    var selectedValue = selectedItem.attr('value');
    if (selectedValue == "2") {
        $('#spice_bank_details').css('visibility', 'visible');
        $('#spice_bank_details').css('display', 'initial');
    }
});
});

$.validator.addMethod("mustbetrue", function (value, element, params) {
    return value == true;
});
$.validator.unobtrusive.adapters.add("mustbetrue", function (options) {
    if (options.element.tagName.toUpperCase() == "INPUT" && options.element.type.toUpperCase() == "CHECKBOX") {
        options.rules["required"] = true;
        if (options.message) {
            options.messages["required"] = options.message;
        }
    }
});

//function numberOfParticipantsChange(self) {
//    var eventId = $('#EventId').val();
//    var numberOfForms = $('option:selected', self).attr('value');
//    var selectedCategory = $("#EventCategoryFastLane option:selected").attr('value');
//    if (numberOfForms === "") {
//        $('#wrapper').html('');
//        return;
//    };
//    multiple_forms(eventId, selectedCategory, numberOfForms);
//}
function EventCategoryFastLaneChange()
{
    var eventId = $('#EventId').val();
    var numberOfForms = 1;
    var selectedCategory = $("#EventCategoryFastLane option:selected").attr('value');
    if (numberOfForms === "") {
        $('#wrapper').html('');
        return;
    };
    multiple_forms(eventId, selectedCategory, numberOfForms);
}

//Email validation utils

// while the lookup is performing
function validation_in_progress() {
    $('#status').html("Validating email address!!");
}

// if email successfull validated
function validation_success(data) {
    $('#status').html(get_suggestion_str(data['is_valid'], data['did_you_mean']));
}

// if email is invalid
function validation_error(error_message) {
    $('#status').html(error_message);
}

// suggest a valid email
function get_suggestion_str(is_valid, alternate) {
    if (alternate) {
        return '<span class="warning">Did you mean <em>' + alternate + '</em>?</span>';
    } else if (is_valid) {
        return '<span class="success">Address is valid!</span>';
    } else {
        return '<span class="error">Address is invalid!</span>';
    }
}

//Email validation utils end here

function menu_section(section) {
    $.ajax({
        url: '/Home/GetMenuSections?section=' + section,
        type: "POST",
        dataType: "html",
        //data: $("#FormId").serialize(),
        success: function (data) {
            //Fill div with results
            $("#maincontainer").html(data);
            runOwlCarousel();
        },
        error: function () {
            $("#maincontainer").html("<span style='text-align:center'><h2>" +
                "Hmmm something went wrong. Its time to fire someone :(</h2></span>");;
        }
    });
}
function runOwlCarousel() {
    var theme_slider = $("#owl-demo");
    $("#owl-demo").owlCarousel({
        navigation: false,
        slideSpeed: 800,
        paginationSpeed: 400,
        autoPlay: 6000,
        addClassActive: true,
        transitionStyle: "fade",
        singleItem: true
    });
    $("#owl-demo2").owlCarousel({
        slideSpeed: 300,
        autoPlay: true,
        navigation: true,
        navigationText: ["&#xf007", "&#xf006"],
        pagination: false,
        singleItem: true
    });

    // Custom Navigation Events
    $(".next-arrow").click(function () {
        theme_slider.trigger('owl.next');
    });
    $(".prev-arrow").click(function () {
        theme_slider.trigger('owl.prev');
    });
}
function multiple_forms(eventid, eventCategory, numberOfForms) {
    //alert("eventID " + eventid + " numberofforms " + numberOfForms); // Debugging purposes
    $.ajax({
        url: '/Register/MultiForms?eventId=' + eventid + '&eventCategory=' + eventCategory + '&numberofforms=' + numberOfForms,
        type: "POST",
        dataType: "html",
        //data: $("#FormId").serialize(),
        beforeSend: function () {
            $("#wrapper").html("<img src='https://clicktorace.blob.core.windows.net/portalimages/loading.gif' style='margin-top: 15%;margin-left:42%' />");
        },
        success: function (data) {
            //Fill div with results
            $("#wrapper").html(data);
        },
        error: function () {
            $("#wrapper").html("<span style='text-align:center'><h2>" +
                "Hmmm something went wrong. Its time to fire someone :(</h2></span>");;
        }
    });
}

function popup(link) {
    $('#popup').bPopup({
        contentContainer: '.content',
        loadUrl: '/PartnersPureHtml/BengaluruMarthon.html' //Uses jQuery.load()
    });
}

function generic_popup(link) {
    $('#popup').bPopup({
        contentContainer: '.content',
        loadUrl: link, //Uses jQuery.load()
        transition: 'slideDown',
        easing: 'easeOutBack', //uses jQuery easing plugin
        speed: 450,
        transition: 'slideDown'
    });
}

function load_datepicker() {
    var DisplayDate = new Date();
    var year = DisplayDate.getFullYear() - 117;
    DisplayDate.setFullYear(year);
    $("#datebirth").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: DisplayDate,
        yearRange: "1900:2011",
        showOn: "button",
        buttonImage: "https://click2race.blob.core.windows.net/portalimages/datePicker.gif",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {
            if (selectedDate) {
                var date1 = selectedDate.split('-');
                if (date1[2] === 2017) {
                    $("#datebirth").val(date1[0] + '-' + date1[1] + '-' + '1990');
                }
            }
        }
    });
}

function load_datepicker_generic(minAge) {
    var DisplayDate = new Date();
    var year = DisplayDate.getFullYear() - 117;
    DisplayDate.setFullYear(year);
    var currentYear = new Date().getFullYear();
    var minimumYear = currentYear - minAge;
    $(".datebirth").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: DisplayDate,
        yearRange: "1900:" + minimumYear,
        showOn: "button",
        buttonImage: "https://click2race.blob.core.windows.net/portalimages/datePicker.gif",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {

            if (selectedDate) {
                var date1 = selectedDate.split('-');
                if (date1[2] === 2017) {
                    $("#datebirth").val(date1[0] + '-' + date1[1] + '-' + '1990');
                }
            }
        }
    });
}

function load_datepicker_b5k() {
    var DisplayDate = new Date();
    var year = DisplayDate.getFullYear() - 117;
    DisplayDate.setFullYear(year);
    $(".datebirth").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: DisplayDate,
        yearRange: "1900:2012",
        showOn: "button",
        buttonImage: "https://click2race.blob.core.windows.net/portalimages/datePicker.gif",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {

            if (selectedDate) {
                var date1 = selectedDate.split('-');
                if (date1[2] === 2017) {
                    $(".datebirth").val(date1[0] + '-' + date1[1] + '-' + '1990');
                }
            }
        }
    });
}
function load_datepicker_b10k() {
    var DisplayDate = new Date();
    var year = DisplayDate.getFullYear() - 117;
    DisplayDate.setFullYear(year);
    $(".datebirth").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: DisplayDate,
        yearRange: "1900:2003",
        showOn: "button",
        buttonImage: "https://click2race.blob.core.windows.net/portalimages/datePicker.gif",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {

            if (selectedDate) {
                var date1 = selectedDate.split('-');
                if (date1[2] === 2017) {
                    $(".datebirth").val(date1[0] + '-' + date1[1] + '-' + '1990');
                }

            }
        }
    });
}


function load_datepicker_tc_kids() {

    $(".datebirth").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: "2005:2011",
        showOn: "button",
        buttonImage: "https://click2race.blob.core.windows.net/portalimages/datePicker.gif",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {
            if (selectedDate) {
                var date1 = selectedDate.split('-');
                if (date1[2] === 2017) {
                    $(".datebirth").val(date1[0] + '-' + date1[1] + '-' + '1990');
                }
            }
        }
    });
}

function load_datepicker_tc_champ() {
    var DisplayDate = new Date();
    var year = DisplayDate.getFullYear() - 117;
    DisplayDate.setFullYear(year);
    $(".datebirth").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: DisplayDate,
        yearRange: "1900:2002",
        showOn: "button",
        buttonImage: "https://click2race.blob.core.windows.net/portalimages/datePicker.gif",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {
            if (selectedDate) {
                var date1 = selectedDate.split('-');
                if (date1[2] === 2017) {
                    $(".datebirth").val(date1[0] + '-' + date1[1] + '-' + '1990');
                }
            }
        }
    });
}

function load_datepicker_tc_fit() {
    var DisplayDate = new Date();
    var year = DisplayDate.getFullYear() - 117;
    DisplayDate.setFullYear(year);
    $(".datebirth").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: DisplayDate,
        yearRange: "1900:2005",
        showOn: "button",
        buttonImage: "https://click2race.blob.core.windows.net/portalimages/datePicker.gif",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {
            if (selectedDate) {
                var date1 = selectedDate.split('-');
                if (date1[2] === 2017) {
                    $(".datebirth").val(date1[0] + '-' + date1[1] + '-' + '1990');
                }
            }
        }
    });
}

// duplicated the code just to avoid any early breakage. FIX ME
function load_datepicker_multiforms() {
    var DisplayDate = new Date();
    var year = DisplayDate.getFullYear() - 117;
    DisplayDate.setFullYear(year);
    $(".datebirth").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        defaultDate: DisplayDate,
        yearRange: "1900:2011",
        showOn: "button",
        buttonImage: "https://click2race.blob.core.windows.net/portalimages/datePicker.gif",
        buttonImageOnly: true,
        onSelect: function (selectedDate) {
            if (selectedDate) {
                var date1 = selectedDate.split('-');
                if (date1[2] === 2017) {
                    $(".datebirth").val(date1[0] + '-' + date1[1] + '-' + '1990');
                }
            }
        }
    });
}

function load_events() {
    $('#Address').keypress(function (event) {
        if ((event.keyCode || event.which) == 13) {
            event.preventDefault();
            return false;
        }
    });

    $('.nums').keypress(function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    });



    $('.chars').keypress(function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        var letters = new RegExp('^[A-Za-z]+$');
        if (String.fromCharCode(charCode).match(letters))
            return true;
        return false;
    });

    $('#address').keyup(function () {
        var keyed = $(this).val().replace(/\n/g, '<br/>');
        $(this).html(keyed);
    });
}