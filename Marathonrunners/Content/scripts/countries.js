/*
	*	Original script by: Shafiul Azam
	*	ishafiul@gmail.com
	*	Version 3.0
	*	Modified by: Luigi Balzano

	*	Description:
	*	Inserts Countries and/or States as Dropdown List
	*	How to Use:

		In Head section:
		<script type= "text/javascript" src = "countries.js"></script>
		In Body Section:
		Select Country:   <select onchange="print_state('state',this.selectedIndex);" id="country" name ="country"></select>
		<br />
		City/District/State: <select name ="state" id ="state"></select>
		<script language="javascript">print_country("country");</script>	

	*
	*	License: OpenSource, Permission for modificatin Granted, KEEP AUTHOR INFORMATION INTACT
	*	Aurthor's Website: http://shafiul.progmaatic.com
	*
*/
var nationality_arr = new Array("Afghan", "Albanian", "Algerian", "American", "Andorran", "Angolan", "Antiguans", "Argentinean", "Armenian", "Australian", "Austrian", "Azerbaijani", "Bahamian", "Bahraini", "Bangladeshi", "Barbadian", "Barbudans", "Batswana", "Belarusian", "Belgian", "Belizean", "Beninese", "Bhutanese", "Bolivian", "Bosnian", "Brazilian", "British", "Bruneian", "Bulgarian", "Burkinabe", "Burmese", "Burundian", "Cambodian", "Cameroonian", "Canadian", "Cape Verdean", "Central African", "Chadian", "Chilean", "Chinese", "Colombian", "Comoran", "Congolese", "Costa Rican", "Croatian", "Cuban", "Cypriot", "Czech", "Danish", "Djibouti", "Dominican", "Dutch", "East Timorese", "Ecuadorean", "Egyptian", "Emirian", "Equatorial Guinean", "Eritrean", "Estonian", "Ethiopian", "Fijian", "Filipino", "Finnish", "French", "Gabonese", "Gambian", "Georgian", "German", "Ghanaian", "Greek", "Grenadian", "Guatemalan", "Guinea-Bissauan", "Guinean", "Guyanese", "Haitian", "Herzegovinian", "Honduran", "Hungarian", "Icelander", "Indian", "Indonesian", "Iranian", "Iraqi", "Irish", "Israeli", "Italian", "Ivorian", "Jamaican", "Japanese", "Jordanian", "Kazakhstani", "Kenyan", "Kittian and Nevisian", "Kuwaiti", "Kyrgyz", "Laotian", "Latvian", "Lebanese", "Liberian", "Libyan", "Liechtensteiner", "Lithuanian", "Luxembourger", "Macedonian", "Malagasy", "Malawian", "Malaysian", "Maldivan", "Malian", "Maltese", "Marshallese", "Mauritanian", "Mauritian", "Mexican", "Micronesian", "Moldovan", "Monacan", "Mongolian", "Moroccan", "Mosotho", "Motswana", "Mozambican", "Namibian", "Nauruan", "Nepalese", "Netherlander", "New Zealander", "Ni-Vanuatu", "Nicaraguan", "Nigerian", "Nigerien", "North Korean", "Northern Irish", "Norwegian", "Omani", "Pakistani", "Palauan", "Panamanian", "Papua New Guinean", "Paraguayan", "Peruvian", "Polish", "Portuguese", "Qatari", "Romanian", "Russian", "Rwandan", "Saint Lucian", "Salvadoran", "Samoan", "San Marinese", "Sao Tomean", "Saudi", "Scottish", "Senegalese", "Serbian", "Seychellois", "Sierra Leonean", "Singaporean", "Slovakian", "Slovenian", "Solomon Islander", "Somali", "South African", "South Korean", "Spanish", "Sri Lankan", "Sudanese", "Surinamer", "Swazi", "Swedish", "Swiss", "Syrian", "Taiwanese", "Tajik", "Tanzanian", "Thai", "Togolese", "Tongan", "Trinidadian or Tobagonian", "Tunisian", "Turkish", "Tuvaluan", "Ugandan", "Ukrainian", "Uruguayan", "Uzbekistani", "Venezuelan", "Vietnamese", "Welsh", "Yemenite", "Zambian", "Zimbabwean");

var country_arr = new Array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czeck Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia, The", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcaim Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romainia", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Spratly Islands", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

var s_a = new Array();
s_a[0] = "";
s_a[105] = "Andaman and Nicobar Islands|Andhra Pradesh|Arunachal Pradesh|Assam|Bihar|Chandigarh|Chhattisgarh|Dadra and Nagar Haveli|Daman and Diu|Delhi|Goa|Gujarat|Haryana|Himachal Pradesh|Jammu and Kashmir|Jharkhand|Karnataka|Kerala|Lakshadweep|Madhya Pradesh|Maharashtra|Manipur|Meghalaya|Mizoram|Nagaland|Orissa|Pondicherry|Punjab|Rajasthan|Sikkim|Tamil Nadu|Tripura|Uttar Pradesh|Uttaranchal|West Bengal";

var c_a = new Array();
c_a[0] = "";
c_a[1] = "Port Blair|Others";
c_a[2] = "Adilabad|Anantapur|Chittoor|Eluru|Kakinada|Guntur|Hyderabad|Kadapa|Karimnagar|Khammam|Machilipatnam|Kurnool|Mahabubnagar|Sangareddi|Srikakulam|Nalgonda|Nellore|Nizamabad|Ongole|Tirupati|Vijayawada|Visakhapatnam|Vizianagaram|Warangal|Others";
c_a[3] = "Itanagar|Others";
c_a[4] = "Dispur|Guwahati|Haflong|Jorhat|Nagoan|Sibsagar|Silchar|Tezpur|Others";
c_a[5] = "Ara|Begusarai|Bhagalpur|Darbhanga|Gaya|Katihar|Muzaffarpur|Patna|Purnia|Others";
c_a[6] = "Chandigarh|Others";
c_a[7] = "Ambikapur|Bhilai|Bilaspur|Dhamtari|Jagdalpur|Korba|Raigarh|Raipur|Raj Nandgaon|Others";
c_a[8] = "Dadra|Masat|Naroli|Samarvarni|Silvassa|Others";
c_a[9] = "Daman|Diu|Others";
c_a[10] = "Delhi|Others";
c_a[11] = "Madgaon|Mormugao|Panaji|Others";
c_a[12] = "Ahmedabad|Bhavnagar|Gandhinagar|Jamnagar|Junagadh|Kutchh|Surat|Vadodara|Others";
c_a[13] = "Ambala|Bhiwani|Faridabad|Fatehabad|Gurgaon|Hisar|Jhajjar|Jind|Kaithal|Karnal|Kurukshetra|Mahendragarh|Palwal|Panchkula|Panipat|Rewari|Rohtak|Sirsa|Sonipat|Yamunagar|Others";
c_a[14] = "Bilaspur|Chamba|Hamirpur|Kangra|Kinnaur|Kullu Manali|Lahaul|Mandi|Shimla|Sirmaur|Solan|Spiti|Una|Others";
c_a[15] = "Anantnag|Jammu|Kashmir|Others";
c_a[16] = "Bokaro|Chirkunda|Deoghar|Dhanbad|Giridih|Hazaribag|Jamshedpur|Medininagar|Phusro|Ramgarh|Ranchi|Others";
c_a[17] = "Bagalkot|Bangalore|Belgaum|Bellary|Bidar|Bijapur|Chamarajanagar|Chikballapur|Chikmagalur|Chitradurga|Davanagere|Dharwad|Gadag|Gulbarga|Hassan|Haveri|Kolar|Koppal|Karwar|Madikeri|Mandya|Mangalore|Mysore|Raichur|Ramanagara|Shimoga|Tumakuru|Udupi|Yadgir|Others";
c_a[18] = "Calicut|Cannanore|Ernakulam|Kochi|Kollam|Thiruvananthapuram|Others";
c_a[19] = "Lakshadweep|Others";
c_a[20] = "Bhopal|Dewas|Gwalior|Indore|Jabalpur|Ratlam|Rewa|Satna|Sagar|Ujjain|Others";
c_a[21] = "Amravati|Aurangabad|Bhiwandi|Jalgaon|Mumbai|Nagpur|Nanded|Pune|Ratnagiri|Solapur|Thane|Others";
c_a[22] = "Imphal|Kakching|Thoubal|Ukhrul|Others";
c_a[23] = "Baghmara|Cherrapunji|Jowai|Mairang|Nongthymmai|Shillong|Sohra|Tura|Others";
c_a[24] = "Aizawl|Champhai|Kolosib|Lawngtlai|Lunglei|Saiha|Serchhip|Others";
c_a[25] = "Chumukedima|Dimapur|Kohima|Mokokchung|Mon|Tuensang|Wokha|Zunheboto|Others";
c_a[26] = "Balasore|Baripada|Bhadrak|Brahmapur|Bhubaneswar|Cuttack|Puri|Rourkela|Sambalpur|Others";
c_a[27] = "Pondicherry|Others";
c_a[28] = "Amritsar|Bathinda|Ferozepur|Jalandhar|Ludhiana|Mohali|Patiala|Phagwara|Others";
c_a[29] = "Ajmer|Alwar|Amer|Barmer|Bharatpur|Bikaner|Jaipur|Jaisalmer|Jodhpur|Kota|Nagaur|Pali|Sikar|Udaipur|Others";
c_a[30] = "Gangtok|Jorethang|Namchi|Rangpo|Others";
c_a[31] = "Ariyalur|Chennai|Coimbatore|Cuddalore|Dharmapuri|Dindigul|Erode|Kanchipuram|Kanniyakumari|Karur|Krishnagiri|Madurai|Nagapattinam|Namakkal|The Nilgiris|Perambalur|Pudukkottai|Ramanathapuram|Salem|Sivaganga|Thanjavur|Theni|Thoothukudi|Thiruvarur|Thirunelveli|Tiruchirappalli|Thiruvallur|Tiruppur|Tiruvannamalai|Vellore|Villupuram|Virudhunagar|Others";
c_a[32] = "Agartala|Belonia|Bishalgarh|Dharmanagar|Kailashahar|Teliamura|Udaipur|Others";
c_a[33] = "Agra|Aligarh|Allahabad|Ambedkar Nagar|Azamgarh|Baghpat|Budaun|Chitrakoot|Etah|Etawah|Faizabad|Farrukhabad|Fatehpur|Firozabad|Gautam Buddha Nagar|Ghaziabad|Ghazipur|Gorakhpur|Hamirpur|Jalaun|Jaunpur|Jhansi|Kabir|Kannauj|Kanpur|Lucknow|Mathura|Meerut|Mirzapur|Moradabad|Muzaffarnagar|Rae Bareli|Rampur|Saharanpur|Siddharth Nagar|Sitapur|Sultanpur|Unnao|Varanasi|Others";
c_a[34] = "Dehradun|Haldwani|Haridwar|Kashipur|Rishikesh|Roorkee|Rudrapur|Others";
c_a[35] = "Asansol|Baharampur|Bardhaman|Bazar|Durgapur|Habra|Ingraj|Kharagpur|Kolkata|Shiliguri|Santipur|Others";

function print_nationality(nationality_id) {
    // given the id of the <select> tag as function argument, it inserts <option> tags
    var option_str = document.getElementById(nationality_id);
    if (option_str == null) return;
    option_str.length = 0;
    option_str.options[0] = new Option('Select Nationality', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < nationality_arr.length; i++) {
        option_str.options[option_str.length] = new Option(nationality_arr[i], nationality_arr[i]);
    }
    var hiddennationality_val = $('#hidden_nationality').val();
    var nationality_val = $('#nationality :selected').val();
    if (hiddennationality_val.length > 0) {
        $('#nationality').val(hiddennationality_val);
    } else {
        $('#hidden_nationality').val(nationality_val);
    }
}

function nationality_onchange() {
    var nationality_val = $('#nationality :selected').val();
    $('#hidden_nationality').val(nationality_val);
}

function print_country(country_id) {
    // given the id of the <select> tag as function argument, it inserts <option> tags
    var option_str = document.getElementById(country_id);
    if (option_str == null) return;
    option_str.length = 0;
    option_str.options[0] = new Option('Select Country', '');
    option_str.selectedIndex = 0;
    for (var i = 0; i < country_arr.length; i++) {
        option_str.options[option_str.length] = new Option(country_arr[i], country_arr[i]);
    }
    var hiddencountry_val = $('#hidden_country').val();
    var country_val = $('#country :selected').val();
    if (hiddencountry_val.length > 0) {
        $('#country').val(hiddencountry_val);
    } else {
        $('#hidden_country').val(country_val);
    }
    var hiddenstate_val = $('#hidden_state').val();
    if (hiddenstate_val.length > 0) {
        print_state('state', $('#country').val() != "India" ? 106 : 105);
    } else if ($('#country').val() != 'India') {
        print_state('state', 106);
    } else {
        print_state('state', 105);
    }
    var hiddencity_val = $('#hidden_city').val();
    if (hiddencity_val.length > 0) {
        print_city('city', $('#state')[0].selectedIndex);
    }
}

//called on country selection change
function print_state(stateId, stateIndex) {
    $('#hidden_country').val($('#country :selected').val());
    var optionStr = document.getElementById(stateId);
    if (stateIndex != 105) {
        $("#other_state_div").removeAttr('style');
        $('#other_city_div').removeAttr('style');
        $("#state_div").css('display', 'none');
        $("#city_div").css('display', 'none');
        $('#othercity').val('true');
    }
    else if (stateIndex == 105) {
        $("#other_state_div").css('display', 'none');
        $('#other_city_div').css('display', 'none');
        $("#state_div").removeAttr('style');
        $("#city_div").removeAttr('style');
        $('#othercity').val('false');
        optionStr.length = 0;	// Fixed by Julian Woods
        optionStr.options[0] = new Option('Select State', '');
        optionStr.selectedIndex = 0;
        var stateArr = s_a[stateIndex].split("|");
        for (var i = 0; i < stateArr.length; i++) {
            optionStr.options[optionStr.length] = new Option(stateArr[i], stateArr[i]);
        }
        var hiddenstate_val = $('#hidden_state').val();
        var state_val = $('#state :selected').val();
        if (hiddenstate_val.length > 0) {
            $('#state').val(hiddenstate_val);
        } else {
            $('#hidden_state').val(state_val);
        }
    }
}

//called on state selection change
function print_city(city_id, city_index) {
    $('#hidden_state').val($('#state :selected').val());
    var option_str = document.getElementById(city_id);
    option_str.length = 0;	// Fixed by Julian Woods
    option_str.options[0] = new Option('Select City', '');
    option_str.selectedIndex = 0;
    var city_arr = c_a[city_index].split("|");
    for (var i = 0; i < city_arr.length; i++) {
        option_str.options[option_str.length] = new Option(city_arr[i], city_arr[i]);
    }
    var hiddencity_val = $('#hidden_city').val();
    var city_val = $('#city :selected').val();
    if (hiddencity_val.length > 0) {
        $('#city').val(hiddencity_val);
    } else {
        $('#hidden_city').val(city_val);
    }
}
