﻿$(document).ready(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        url: '/Account/UploadUserPhoto',
        autoUpload: true,
        basic: true,
        maxNumberOfFiles: 1,
        maxFileSize: 500,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        done: function (e, data) {
            if (data.result.error == 'true') {
                alert(data.result.message);
            }
            $('#facebook').css('display', 'none');
            $('#userphoto').attr('src', "");
            $('#userphoto').attr('src', data.result.name + '?' + new Date().getTime());/*This is done to refresh the image since we are updating the same URL*/
        },
        fail: function (e, data) {
            alert("Somethig has gone wrong!!" + data.result.error);
            $('#facebook').css('display', 'none');
        }
    }).on('fileuploadprogress', function (e, data) {
        $('#facebook').css('display', 'inline');
    }).on('fileuploadsend', function (e, data) {
        if (data.files[0].size == 0) {
            alert("Image seems to be 0 byte");
            return false;
        }
        if (data.files[0].size > 750000) {
            alert("Image is bigger than 750KB");
            return false;
        }
    });

    $('#fileuploadidproof').fileupload({
        dataType: 'json',
        url: '/Account/UploadUserPhotoIdProof',
        autoUpload: true,
        basic: true,
        maxNumberOfFiles: 1,
        maxFileSize: 500,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        done: function (e, data) {
            if (data.result.error == 'true') {
                alert(data.result.message);
            }
            $('#facebook2').css('display', 'none');
            $('#useridproof').attr('src', '');
            $('#useridproof').attr('src', data.result.name + '?' + new Date().getTime());/*This is done to refresh the image since we are updating the same URL*/
        },
        fail: function (e, data) {
            alert("Oopss Somethig has gone wrong!!" + data.result.error);
            $('#facebook2').css('display', 'none');
        }
    }).on('fileuploadprogress', function (e, data) {
        $('#facebook2').css('display', 'inline');
    }).on('fileuploadsend', function (e, data) {
        if (data.files[0].size == 0) {
            alert("Image seems to be 0 byte");
            return false;
        }
        if (data.files[0].size > 750000) {
            alert("Image is bigger than 750KB");
            return false;
        }
    });
});

function toggle_field_section(self, field) {

    if ($(self).attr('id') == 'linksection' && ($(self).css('display') == 'block' || $(self).css('visibility') == 'visible')) {
        // link section is visible hide it and show #editsection but don hide it if the field is Image
        if (field == 'UserLocation') self = $('#userlocation').children('#linksection');
        if (field != 'Image') {
            $(self).css('display', 'none');
            $(self).next('#editsection').css('display', 'block');
        } else if (field == 'Image') {
            if ($(self).next('#editsection').css('display') == 'none') {
                $(self).next('#editsection').css('display', 'block');
                $(self).children('.clk2raceSettingsListEdit').children('#edit').html('Done');
            } else {
                $(self).next('#editsection').css('display', 'none');
                $(self).children('.clk2raceSettingsListEdit').children('#edit').html('Edit');
            }
            return;
        }

        if (field != undefined && field != 'Image') {
            $(self).next('#editsection').html("<div style='text-align:center'><i class='fa fa-refresh fa-spin'></i></div>");
            $.ajax({
                url: '/Setting/' + field,
                type: "POST",
                dataType: "html",
                //data: $("#FormId").serialize(),
                success: function (data) {
                    //Fill div with results
                    if (field == 'UserLocation')
                        $('#userlocation').children('#editsection').html(data);
                    else
                        $(self).next('#editsection').html(data);

                    if (field == 'DateOfBirth') load_datepicker();
                    if (field == 'Nationality') load_nationality();
                    if (field == 'UserLocation') load_userlocation();
                    //rebind events since the elements are loaded thru ajax
                    load_events();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(self).next('#editsection').html("<span style='text-align:center'><h2>" +
                        "Oops something went wrong :(</h2></span>");
                }
            });
        }
    } else if ($(self).closest('#editsection').attr('id') == 'editsection' && ($(self).closest('#editsection').css('display') == 'block' || $(self).closest('#editsection').css('visibility') == 'visible')) {
        if (field == 'UserLocation') self = $('#userlocation').children('#editsection');
        $(self).closest('#editsection').css('display', 'none');
        update_view_field(self);
        $(self).closest('#editsection').prev('#linksection').css('display', 'block');
        if (field == 'UserLocation') $('.userlocation').css('display', 'block');
    }
}

function update_view_field(self) {
    // hidden field success is visible that means the update was successfull
    var success = $(self).closest('#editsection').find('#success');
    if (success.val() != undefined) {
        var json = jQuery.parseJSON(success.val());
        if (json instanceof Array) {
            $.each(json, function (i, item) {
                $('#' + json[i].field).html((json[i].value == undefined) ? "" : json[i].value);
            });
        } else {
            $('#' + json.field).html(json.value);
        }
    }
}

function update_ended(data) {
    //var response = ajaxContext.get_response();
    //var statusCode = response.get_statusCode();
    return false;
}

function load_nationality() {
    print_nationality("nationality");
}

function load_userlocation() {
    var isEditing = $('.userlocation').css('display') == 'none';
    var country = '';
    if (isEditing) {
        country = $('#hidden_country').val();
    } else {
        $('.userlocation').css('display', 'none');
        country = $('#country_view').text();
    }
    $('input[id=hidden_country]').val(country);
    if (country != 'India') {
        if (!isEditing) {
            $('#other_state').val($('#state_view').text());
            $('#other_city').val($('#city_view').text());
        }
        //$("#other_state_div").css('display', 'block');
        //$('#other_city_div').css('display', 'block');
        //$("#state_div").css('display', 'none');
        //$("#city_div").css('display', 'none');
    } else {
        if (!isEditing) {
            $('input[id=hidden_state]').val($('#state_view').text());
            $('input[id=hidden_city]').val($('#city_view').text());
        }
    }
    $('#postcode').val($('#pincode_view').text());
    print_country('country');
}