﻿$('#pay').click(function(e) {
 
    var postData = $(this).serializeArray();
    var formURL = "https://test.payu.in/_payment";
    $.ajax(
    {
        url: formURL,
        type: "POST",
        data: postData,
        success: function (data, textStatus, jqXHR) {
            //data: return data from server
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
    e.preventDefault(); //STOP default action
    e.unbind(); //unbind. to stop multiple form submit.
});