﻿$(document).ready(function () {

    $("#pay").click(function () {
        $('#popup>.content').html("");
        var jObject = new Object();
        jObject.EventId = $('#pay').attr('data-product');
        jObject.EventType = $('#typeofrun').val();
        var content = $('.content');
        $.ajax({
            url: "/Register/CheckDuplicateRegistration",
            type: "POST",
            dataType: 'text',
            data: JSON.stringify(jObject),
            contentType: "application/json; charset=utf-8",
            //beforeSend: function () { $("#saveStatus").html("Saving").show(); },
            success: function (result) {
                if (result == 'True') {
                    $('#popup').bPopup({
                        onOpen: function () {
                            $('#popup').css("width", "20%");
                            content.html("<h3 style=\"text-align:center\">You have already registered for this event</h3>");
                        },
                        onClose: function () {
                            $('#popup').css("width", "80%");
                            content.empty();
                        },
                        easing: 'easeOutBack', //uses jQuery easing plugin
                        speed: 450,
                        transition: 'slideDown'
                    });
                } else {
                    if ($('#spice_bank_details').css('visibility') == "visible" && $('#spice_bnk_dtls').val() == "") {
                        alert("You have to enter the bank details!");
                    } else {
                        jObject.Student = false;
                        var student = $("#student");
                        if (!student.isNaN) {
                            jObject.Student = student.is(":checked");
                        }
                        var donation = $("#donation");
                        if (!donation.isNaN) {
                            jObject.Donation = $('#donation').val();
                        }
                        if ($('#spice_bank_details').css('visibility') == "visible") {
                            jObject.FedDetails = $('#spice_bnk_dtls').val();
                        }
                        ajaxPayInit(jObject);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Something has gone wrong. Please report to support@click2race.in");
            }
        });
    });

});

function isValidTime(time) {
    return /^[0-9][0-9]:[0-9][0-9]:[0-9][0-9]/i.test(time);
}

function isValidLink(link) {
    return /^(https?)/i.test(link);
}

function OnConfirmFailure(parameters) {
    alert('failure');
}

function OnConfirmSuccess(parameters) {
    alert('success');
}
function closeBPopup() { $('#popup').bPopup().close(); }

function ajaxPayInit(jObject) {
    $.ajax({
        url: "/Register/PaymentInit",
        type: "POST",
        dataType: 'text',
        data: JSON.stringify(jObject),
        contentType: "application/json; charset=utf-8",
        //beforeSend: function () { $("#saveStatus").html("Saving").show(); },
        success: function (result) {
            $('#popup>.content').html(result);
            $('#popup').bPopup({
                easing: 'easeOutBack', //uses jQuery easing plugin
                speed: 450,
                transition: 'slideDown'
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something has gone wrong. Please report to support@click2race.in");
        }
    });
}

function checkpremium_begin() {
    $('#message').css("display", "none");
    $('#validdata').val('false');
    $('#validemail').val('');
    if (!validateEmail($('#emailid').val())) {
        $('#message').css("display", "block").attr("class", "alert alert-error");
        $('#message').text('Not a valid email address');
        return false;
    } else {
        $('#spinimage').css("visibility", "visible");
    }
    return true;
}

function checkpremium_success(data, status, xhr) {
    $('#spinimage').css("visibility", "hidden");
    if (data.responseJSON.Success == true) {
        $('#message').css("display", "block").attr("class", "alert alert-success");
        $('#message').text('Nice! You are a premium member');
        $('#validdata').val('true');
        $('#validemail').val($('#emailid').val());
    } else {
        $('#message').css("display", "block").attr("class", "alert alert-error");
        $('#message').text(data.responseJSON.StatusMessage);
        $('#validdata').val('false');
        $('#validemail').val('');
    }
}

function validateEmail(emailAddress) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(emailAddress)) {
        return true;
    }
    else {
        return false;
    }
}