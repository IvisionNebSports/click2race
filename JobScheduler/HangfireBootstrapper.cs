﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.SqlServer;

namespace JobScheduler {
    public class HangfireBootstrapper {
        public static readonly HangfireBootstrapper Instance = new HangfireBootstrapper();

        private readonly object _lockObject = new object();
        private bool _started;

        private BackgroundJobServer _backgroundJobServer;

        private HangfireBootstrapper() {
        }

        public void Start() {
            lock (_lockObject) {
                if (_started) return;
                _started = true;

                //HostingEnvironment.RegisterObject(this);

                GlobalConfiguration.Configuration.UseSqlServerStorage(
              "PortalConnectionString",
              new SqlServerStorageOptions {
                  // set to true to have HangFire create your schema
                // PrepareSchemaIfNecessary = true,
                  // set to false and run HangFire.sql to create your sql.
                  PrepareSchemaIfNecessary = false, 
                  QueuePollInterval = TimeSpan.FromSeconds(1)
              });
                // Specify other options here

                _backgroundJobServer = new BackgroundJobServer();
            }
        }

        public void Stop() {
            lock (_lockObject) {
                if (_backgroundJobServer != null) {
                    _backgroundJobServer.Dispose();
                }

                //HostingEnvironment.UnregisterObject(this);
            }
        }
    }
}
