﻿using System;
using Hangfire;
using Marathonrunners.Core;
using ExportToExcel;
using System.IO;
using StorageManager;
using Marathonrunners.Core.Entities;
using EmailQueueSystem;

namespace JobScheduler
{
    public static class JobSchedulerManager
    {
        private static IJobSchedulerRepository _jobRepository;

        // This method should be called before any property of method in this class
        public static void Instantiate(IJobSchedulerRepository repository)
        {
            _jobRepository = repository;
        }
        public static void InitiateDailyReportJob()
        {
            RecurringJob.AddOrUpdate(() => FastLaneReport(), Cron.Daily);
            //FastLaneReport();
        }

        public static void FastLaneReport()
        {
            try
            {
                var list = _jobRepository.GetFastLaneRegisterationReportFor(numberOfDays: 1);
                var appDomain = AppDomain.CurrentDomain;
                var basePath = appDomain.BaseDirectory;
                string fileName = "FastLaneReport" + DateTime.Now.ToString("ddMMMyyyy-HHmmssff") + ".xlsx";
                string path = Path.Combine(basePath, fileName);
                CreateExcelFile.CreateExcelDocument(list, path);
                string storedPath;
                StorageHandler.TryUploadFile(StorageConfiguration.ContainerReports, path, fileName, StorageConfiguration.ContentTypeExcel, out storedPath);
                File.Delete(path);
                SendMailReportToSupport(storedPath);
            }
            catch (Exception ex)
            {
                EmailQueuer.SendErrorLog(ex.Message);
            }
        }

        private static void SendMailReportToSupport(string storedPath)
        {
            var email = new EmailQueue();
            email.EmailTo = "support@click2race.in";
            email.EmailFrom = "Report Generator <support@click2race.in>";
            email.Message = "Daily Report: " + storedPath;
            email.Subject = "FasteLane Registration Report for date " + DateTime.Now.Subtract(TimeSpan.FromDays(1)).ToShortDateString();
            EmailQueuer.Instance.QueueIt(email);
        }
    }

}
