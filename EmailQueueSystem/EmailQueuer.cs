﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Marathonrunners.Core;
using Marathonrunners.Core.Entities;

namespace EmailQueueSystem {

    public class EmailVerify {

        public EmailQueue EmailQueue { get; set; }

        public EmailVerification EmailVerifyStatus { get; set; }

        public DateTime FirstTimeVerified { get; set; }
    }

    public class EmailQueuer {
        private static readonly Lazy<EmailQueuer> _instance = new Lazy<EmailQueuer>(() => new EmailQueuer());
        private ConcurrentQueue<EmailQueue> emailQueue = new ConcurrentQueue<EmailQueue>();
        private ConcurrentQueue<int> emailQueueIds = new ConcurrentQueue<int>();
        private ConcurrentQueue<EmailVerify> emailVerifyingQueue = new ConcurrentQueue<EmailVerify>();
        private Task _emailPollingTask;
        private CancellationTokenSource _emailPollingCancellationToken;
        private Task _emailVerifyingTask;
        private CancellationTokenSource _emailVerifyingCancellationToken;
        private IEmailRepository _emailRepository;
        private bool _exceptionRaised;
        private Stopwatch _timerStopwatch = new Stopwatch();
        private DateTime ZeroTime = Convert.ToDateTime("1/1/0001 12:00:00 AM");

        private EmailQueue CurrentProcessingId { get; set; }

        private EmailQueuer() {
        }

        public static EmailQueuer Instance {
            get { return _instance.Value; }
        }

        public void QueueIt(int emailqueueentry) {
            emailQueueIds.Enqueue(emailqueueentry);
        }

        public void QueueIt(EmailQueue queue) {
            emailQueue.Enqueue(queue);
        }

        public void Start(EmailRepository emailRepository) {
            // Query database to get the email list
            _emailRepository = emailRepository;
            //LoadEmailQueue();
            StartEmailPolling();
            StartVerifying();
        }

        private void StartEmailPolling() {
            _emailPollingCancellationToken = new CancellationTokenSource();
            _emailPollingTask = Task.Run(async () => {
                while (true) {
                    try {
                        if (_emailPollingTask.IsCanceled) break;
                        if (emailQueue.IsEmpty) {
                            // load more data from the database if not then sleep and continue
                            if (!LoadEmailQueueFromEmailQueueIds()) {
                                await Task.Delay(500);
                                continue;
                            }
                        }
                        EmailQueue currentProcessingId;
                        emailQueue.TryDequeue(out currentProcessingId);
                        CurrentProcessingId = currentProcessingId;

                        // Check if its a valid email address if not then log it as
                        // invalid , send a mail to support and move on
                        if (!CurrentProcessingId.EmailVerified && !EmailHandler.IsValidEmail(CurrentProcessingId.EmailTo)) {
                            CurrentProcessingId.EmailStatus = EmailStatusEnum.Invalid;
                            UpdateEmailQueue(CurrentProcessingId);
                            SendFailedValidationMail(CurrentProcessingId.EmailTo);
                            continue;
                        }

                        //send mail and mark it as Verifying
                        if (EmailHandler.Dispatch(CurrentProcessingId).StatusCode != System.Net.HttpStatusCode.OK) {
                            throw new Exception("Sending of mail to " + CurrentProcessingId.EmailTo + " failed. With HTTP status code OK");
                        }

                        var confirmationUpdation = CurrentProcessingId;
                        confirmationUpdation.EmailDateOut = DateTime.Now;
                        confirmationUpdation.EmailStatus = EmailStatusEnum.Verifying;
                        UpdateEmailQueue(confirmationUpdation);
                        CurrentProcessingId = null;
                    } catch (Exception ex) {
                        emailQueue.Enqueue(CurrentProcessingId);
                        CurrentProcessingId = null;
                        //Log Error
                        if (!IsSentTooManyTimes())
                            SendErrorLog("Exception occured in Email Dispatcher loop<br>" + ex.Message + "<br><br>" + ex.StackTrace + "<br><br>" + (ex.InnerException != null ? ex.InnerException.Message : ""));
                    }
                    await Task.Delay(500);
                }
            }, _emailPollingCancellationToken.Token);
        }

        public void StartVerifying() {
            _emailVerifyingCancellationToken = new CancellationTokenSource();
            _emailVerifyingTask = Task.Run(async () => {
                while (true) {
                    try {
                        //throw new Exception("somthing");
                        if (_emailVerifyingTask.IsCanceled) break;
                        if (emailVerifyingQueue.IsEmpty) {
                            LoadVerifyingQueue();
                            if (emailVerifyingQueue.IsEmpty) {
                                await Task.Delay(500);
                                continue;
                            }
                        }
                        EmailVerify email = null;

                        if (emailVerifyingQueue.TryDequeue(out email)) {
                            var verification = EmailHandler.Verify(email.EmailQueue);
                            if (verification == EmailVerification.Failed) {
                                email.EmailQueue.EmailStatus = EmailStatusEnum.Failed;
                                UpdateEmailQueue(email.EmailQueue);
                                //SendingMailFailed(email.EmailQueue.EmailTo);
                            }

                            if (verification == EmailVerification.Sent) {
                                email.EmailQueue.EmailStatus = EmailStatusEnum.Sent;
                                UpdateEmailQueue(email.EmailQueue);
                            }

                            if (verification == EmailVerification.Retry ||
                              verification == EmailVerification.Unknown) {
                                if (email.FirstTimeVerified == ZeroTime) {
                                    email.FirstTimeVerified = DateTime.Now;
                                    emailVerifyingQueue.Enqueue(email);
                                } else if (DateTime.Now.Subtract(email.FirstTimeVerified).Minutes > 10) {
                                    var emailItem = email.EmailQueue;
                                    emailItem.EmailStatus = email.EmailVerifyStatus == EmailVerification.Retry
                                        ? EmailStatusEnum.Failed
                                        : EmailStatusEnum.Unknown;
                                    UpdateEmailQueue(emailItem);
                                    //SendingMailFailed(email.EmailQueue.EmailTo);
                                } else {
                                    emailVerifyingQueue.Enqueue(email);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        //Log error
                        //if (!IsSentTooManyTimes(ex))
                        //    SendErrorLog(ex.Message + "<br><br>" + ex.StackTrace + "<br><br>" + (ex.InnerException != null ? ex.InnerException.Message : ""));
                        if (!IsSentTooManyTimes())
                            SendErrorLog("Exception occured in Email Verify loop<br>" + ex.Message + "<br><br>" + ex.StackTrace + "<br><br>" + (ex.InnerException != null ? ex.InnerException.Message : ""));
                    }
                    await Task.Delay(500);
                }
            });
        }

        public static void SendErrorLog(string message) {
            var target_build = ConfigurationManager.AppSettings["TARGET_BUILD"];
            var failedValidationMail = new EmailQueue() {
                EmailTo = "support@click2race.in",
                EmailFrom = "EmailQueuer <support@click2race.in>",
                EmailStatus = EmailStatusEnum.Invalid,
                Message = "Exception has occured:<br><br>" + message,
                Subject = target_build + ": ALERT: Exception occured!!",
                EmailDateIn = DateTime.Now,
                EmailDateOut = DateTime.Now,
                EmailVerified = true,
                EmailType = EmailTypeEnum.ErrorMail
            };
            EmailHandler.Dispatch(failedValidationMail);
        }

        public void Stop() {
            try {
                if (_emailPollingTask != null && _emailPollingCancellationToken != null &&
                    !_emailPollingCancellationToken.IsCancellationRequested &&
                    !_emailPollingTask.IsCanceled && !_emailPollingTask.IsFaulted)
                    _emailPollingCancellationToken.Cancel();
                if (_emailVerifyingTask != null && _emailVerifyingCancellationToken != null &&
                    !_emailVerifyingCancellationToken.IsCancellationRequested &&
                    !_emailVerifyingTask.IsCanceled && !_emailVerifyingTask.IsFaulted)
                    _emailVerifyingCancellationToken.Cancel();
                if (_emailPollingTask != null) _emailPollingTask.Wait();
                if (_emailVerifyingTask != null) _emailVerifyingTask.Wait();
            } catch (Exception ex) {
                // log error
                if (!IsSentTooManyTimes())
                    SendErrorLog(ex.Message + "\n\n" + ex.StackTrace);
            }
        }

        private void SendFailedValidationMail(string email) {
            var target_build = ConfigurationManager.AppSettings["TARGET_BUILD"];
            var failedValidationMail = new EmailQueue {
                EmailTo = "support@click2race.in",
                EmailFrom = "EmailQueuer <support@click2race.in>",
                EmailStatus = EmailStatusEnum.Invalid,
                Message = "Suspicious mail Id: " + email,
                Subject = target_build + ": Suspicious email Id",
                EmailDateIn = DateTime.Now,
                EmailDateOut = DateTime.Now,
                EmailVerified = true,
                EmailType = EmailTypeEnum.ErrorMail
            };
            EmailHandler.Dispatch(failedValidationMail);
        }

        private void SendingMailFailed(string email) {
            var target_build = ConfigurationManager.AppSettings["TARGET_BUILD"];
            var failedMail = new EmailQueue() {
                EmailTo = "support@click2race.in",
                EmailFrom = "EmailQueuer <support@click2race.in>",
                EmailStatus = EmailStatusEnum.Invalid,
                Message = "Probably this email has failed to deliver: " + email,
                Subject = target_build + ": Probable email failure",
                EmailDateIn = DateTime.Now,
                EmailDateOut = DateTime.Now,
                EmailVerified = true,
                EmailType = EmailTypeEnum.ErrorMail
            };
            EmailHandler.Dispatch(failedMail);
        }

        private void LoadEmailQueue() {
            var list = _emailRepository.GetEmailQueue()
                .Where(x => x.EmailStatus == EmailStatusEnum.Queued)
                .OrderByDescending(y => y.EmailDateIn);

            foreach (var emailid in list) {
                emailQueue.Enqueue(emailid);
            }
        }

        private bool LoadEmailQueueFromEmailQueueIds() {
            if (emailQueueIds.IsEmpty) return false;
            var list = new List<int>();
            for (var i = 0; i < emailQueueIds.Count; i++) {
                int result;
                if (emailQueueIds.TryDequeue(out result))
                    list.Add(result);
            }
            var listofemails = _emailRepository.FindByIds(list);
            foreach (var emailid in listofemails) {
                emailQueue.Enqueue(emailid);
            }
            return true;
        }


        private void UpdateEmailQueue(EmailQueue emailQueue) {
            //lock (_objectLock) {
            _emailRepository.UpdateEmailQueue(emailQueue);
            //}
        }

        private bool IsSentTooManyTimes() {
            if (_exceptionRaised) {
                if (_timerStopwatch.Elapsed.Minutes >= 15) {
                    _exceptionRaised = false;
                    _timerStopwatch.Stop();
                    return false;
                }
                return true;
            }
            _exceptionRaised = true;
            _timerStopwatch.Restart();
            return false;
        }

        private void LoadVerifyingQueue() {
            var listOfMails = _emailRepository.GetMailsWithVerifyingStatus();
            foreach (var listOfMail in listOfMails) {
                emailVerifyingQueue.Enqueue(new EmailVerify() { EmailQueue = listOfMail });
            }
        }
    }

    public enum EmailVerification {
        Retry,
        Sent,
        Failed,
        Unknown
    }
}