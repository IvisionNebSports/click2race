﻿namespace EmailQueueSystem {
    public class TransactionStatus {
        public bool IsTransactionSuccessfull { get; set; }
        public string TransactionId { get; set; }
        public string Amount { get; set; }
        public string PayuMoneyId { get; set; }
        public string AmountSplit { get; set; }
        public string BankRefNumber { get; set; }
    }
}
