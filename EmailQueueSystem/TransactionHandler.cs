﻿using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;

namespace EmailQueueSystem {
    public static class TransactionHandler {

        public static TransactionStatus GeTransactionStatus(string transactionId) {
            //return new TransactionStatus() { IsTransactionSuccessfull = true, Amount = "500", PayuMoneyId = "453334" , TransactionId = transactionId};

            string URI = "https://test.payumoney.com/payment/payment/chkMerchantTxnStatus?";

            var merchankey = ConfigurationManager.AppSettings["MERCHANT_KEY"];
            string authorizationHeader = ConfigurationManager.AppSettings["Authorization"];
            string myParameters = string.Format("merchantKey={0}&merchantTransactionIds={1}", merchankey, transactionId);
            //  string myParameters = "merchantKey=0yW16s&merchantTransactionIds=TSN3000056599_2062014173214";

            // This has to be done becase one module works for staged and not for prod
            if (ConfigurationManager.AppSettings["TARGET_BUILD"] == "STAGED") {
                using (WebClient wc = new WebClient()) {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    wc.Headers.Add("Authorization", authorizationHeader);
                    string responseData_3 = wc.UploadString(URI, myParameters);
                    var json = JObject.Parse(responseData_3);
                    if (json == null) return new TransactionStatus() { IsTransactionSuccessfull = false };
                    var status = json.Value<int>("status");
                    if (status == -1) return new TransactionStatus() { IsTransactionSuccessfull = false }; ;
                    var messagestatus = json.Value<JArray>("result");
                    var statusstr = messagestatus[0].Value<string>("status");
                    if (status == 0 && statusstr == "Money with payupaisa")
                        return new TransactionStatus() {
                            IsTransactionSuccessfull = true,
                            Amount = messagestatus[0].Value<string>("amount"),
                            PayuMoneyId = messagestatus[0].Value<string>("paymentId"),
                            TransactionId = transactionId,
                            AmountSplit = "WE DO NOT GET THIS FROM PAYU TRANSACTION VERIFICATION",
                            BankRefNumber = "WE DO NOT GET THIS FROM PAYU TRANSACTION VERIFICATION"
                        };
                    return new TransactionStatus() { IsTransactionSuccessfull = false };
                }
            } else {
                string data = myParameters; //replace <value>
                byte[] dataStream = Encoding.UTF8.GetBytes(data);

                var webRequest = (HttpWebRequest)WebRequest.Create("https://www.payumoney.com/payment/payment/chkMerchantTxnStatus?");
                webRequest.Headers.Add("Authorization", authorizationHeader);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = dataStream.Length;
                webRequest.Method = "POST";
                Stream newStream = webRequest.GetRequestStream();
                // Send the data.
                newStream.Write(dataStream, 0, dataStream.Length);
                newStream.Close();
                WebResponse webResponse = webRequest.GetResponse();
                var responseReader = new StreamReader(webResponse.GetResponseStream());
                string responseData = responseReader.ReadToEnd();
                var jsonparsed = JObject.Parse(responseData);
                responseReader.Close();
                if (jsonparsed == null) return new TransactionStatus() { IsTransactionSuccessfull = false };
                var payustatus = jsonparsed.Value<int>("status");
                if (payustatus == -1) return new TransactionStatus() { IsTransactionSuccessfull = false };
                var messagestatus2 = jsonparsed.Value<JArray>("result");
                var payustatusstr = messagestatus2[0].Value<string>("status");
                if (payustatus == 0 && payustatusstr == "Failed") return new TransactionStatus() { IsTransactionSuccessfull = false };
                if (payustatus == 0 && payustatusstr == "Settlement in process")
                    return new TransactionStatus() {
                        IsTransactionSuccessfull = true,
                        Amount = messagestatus2[0].Value<string>("amount"),
                        PayuMoneyId = messagestatus2[0].Value<string>("paymentId"),
                        TransactionId = transactionId,
                        AmountSplit = "WE DO NOT GET THIS FROM PAYU TRANSACTION VERIFICATION",
                        BankRefNumber = "WE DO NOT GET THIS FROM PAYU TRANSACTION VERIFICATION"
                    };
                return new TransactionStatus() { IsTransactionSuccessfull = false };
            }

        }
    }
}
