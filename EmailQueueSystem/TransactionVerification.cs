﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Marathonrunners.Core;
using Marathonrunners.Core.Entities;


namespace EmailQueueSystem {

    public class TransactionInfo {
        public string TransactionId { get; set; }
        public string PayuMoneyId { get; set; }
        public string AmountSplit { get; set; }
        public string BankRefNum { get; set; }
    }

    public class TransactionMetadata {
        public DateTime TimeOfEntry { get; set; }
        public Action<string, string, string, string, bool> Operation { get; set; }
        public bool IsSuccessfull { get; set; }
        public List<EmailQueue> EmailQueueEntries { get; set; }
        public TransactionInfo Transaction { get; set; }
    }

    public class TransactionVerification {

        private static readonly Lazy<TransactionVerification> _instance = new Lazy<TransactionVerification>(() => new TransactionVerification());

        private ConcurrentDictionary<string, TransactionMetadata> _transactionQueue = new ConcurrentDictionary<string, TransactionMetadata>();
        private CancellationTokenSource _transactionVerificationCancellationToken;
        private Task _transactionVerificationPollingTask;
        private string _oldExceptionMessage = "";
        private Stopwatch _timerStopwatch = new Stopwatch();

        private AccountRepository _accountRepository { get; set; }
        private EmailRepository _emailRepository { get; set; }
        //private EmailQueue CurrentProcessingId { get; set; }

        private TransactionVerification() { }

        public static TransactionVerification Instance {
            get { return _instance.Value; }
        }

        public void Start(AccountRepository accountRepository, EmailRepository emailRepository) {
            _accountRepository = accountRepository;
            _emailRepository = emailRepository;
            StartTransactionVerification();
        }

        public void AddEntry(string transactionid, TransactionMetadata meta) {
            _transactionQueue.TryAdd(transactionid, meta);
        }

        public void RemoveEntry(string transactionid) {
            object syncLock = new object();
            lock (syncLock) {
                TransactionMetadata meta;
                _transactionQueue.TryRemove(transactionid, out meta);
            }
        }
        public void SetEntryToBeSuccessfull(string transactionId, TransactionInfo transInfo) {
            if (!_transactionQueue.ContainsKey(transactionId)) return;
            var meta = _transactionQueue[transactionId];
            meta.IsSuccessfull = true;
            meta.Transaction = transInfo;
            _transactionQueue[transactionId] = meta;
        }

        private void StartTransactionVerification() {
            _transactionVerificationCancellationToken = new CancellationTokenSource();
            _transactionVerificationPollingTask = Task.Run(async () => {
                while (true) {
                    try {
                        if (_transactionVerificationPollingTask.IsCanceled) break;
                        if (!_transactionQueue.IsEmpty) {
                            var transaction = _transactionQueue.LastOrDefault();
                            var meta = transaction.Value;
                            if (DateTime.Now.Subtract(meta.TimeOfEntry).Minutes >= 15 || meta.IsSuccessfull) {
                                if (meta.IsSuccessfull) {
                                    meta.Operation(meta.Transaction.TransactionId, meta.Transaction.AmountSplit,
                                          meta.Transaction.PayuMoneyId, meta.Transaction.BankRefNum,false);
                                    foreach (var email in meta.EmailQueueEntries) {
                                        var emailRef = _emailRepository.QueueEmail(email);
                                        EmailQueuer.Instance.QueueIt(emailRef);
                                    }
                                } else {
                                    TransactionStatus transactionStatus = TransactionHandler.GeTransactionStatus(transaction.Key);
                                    if (transactionStatus.IsTransactionSuccessfull) {
                                        meta.Operation(transactionStatus.TransactionId, transactionStatus.AmountSplit,transactionStatus.PayuMoneyId, transactionStatus.BankRefNumber,true);
                                        foreach (var email in meta.EmailQueueEntries) {
                                            var emailRef = _emailRepository.QueueEmail(email);
                                            EmailQueuer.Instance.QueueIt(emailRef);
                                        }
                                        // Update the table with true
                                    } else {
                                        // update the table with false
                                        SendingFailedTransaction(transaction.Key);
                                    }
                                }
                                TransactionMetadata tempout;
                                _transactionQueue.TryRemove(transaction.Key, out tempout);
                            }

                        }

                    } catch (Exception ex) {
                        //Log Error
                        if (!IsSentTooManyTimes(ex))
                            EmailQueuer.SendErrorLog(ex.Message + "<br><br>" + ex.StackTrace + "<br><br>" + (ex.InnerException != null ? ex.InnerException.Message : ""));
                    }
                    await Task.Delay(1000);
                }
            }, _transactionVerificationCancellationToken.Token);
        }


        private bool IsSentTooManyTimes(Exception exception) {
            if (_oldExceptionMessage == exception.Message) {
                if (_timerStopwatch.Elapsed.Minutes >= 15) {
                    ResetRepeatedErrorLogger(exception);
                    return false;
                }
                return true;
            }
            ResetRepeatedErrorLogger(exception);
            return false;
        }

        private void ResetRepeatedErrorLogger(Exception exception) {
            _timerStopwatch.Stop();
            _timerStopwatch.Start();
            _oldExceptionMessage = exception.Message;
        }

        private void SendingFailedTransaction(string transactionId) {
            var failedMail = new EmailQueue() {
                EmailTo = "support@click2race.in",
                EmailFrom = "EmailQueuer <support@click2race.in>",
                EmailStatus = EmailStatusEnum.Invalid,
                Message = "Probably this transaction has failed : " + transactionId,
                Subject = "Probable transaction failure",
                EmailDateIn = DateTime.Now,
                EmailDateOut = DateTime.Now,
                EmailVerified = true,
                EmailType = EmailTypeEnum.ErrorMail
            };
            EmailHandler.Dispatch(failedMail);
        }



    }
}
