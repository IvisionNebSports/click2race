﻿using System;
using System.Linq;
using Marathonrunners.Core.Entities;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace EmailQueueSystem {
    public static class EmailHandler {

        private static readonly RestClient client = new RestClient {
            BaseUrl = new Uri("https://api.mailgun.net/v3"),
            Authenticator = new HttpBasicAuthenticator("api", "key-a4c8d1aceeff8f9ed1ea5186958a3d89")
        };

        public static IRestResponse Dispatch(EmailQueue singleEmailEntity) {
            var request = new RestRequest();
            request.AddParameter("domain",
                "click2race.in", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", singleEmailEntity.EmailFrom);
            request.AddParameter("to", singleEmailEntity.EmailTo);
            if (!string.IsNullOrEmpty(singleEmailEntity.CC))
                request.AddParameter("cc", singleEmailEntity.CC);
            if (!string.IsNullOrEmpty(singleEmailEntity.BCC))
                request.AddParameter("bcc", singleEmailEntity.BCC);
            request.AddParameter("subject", singleEmailEntity.Subject);
            request.AddParameter("html", singleEmailEntity.Message);
            request.Method = Method.POST;
            var returnVal = client.Execute(request);
            return returnVal;
            //return null;
        }

        public static bool IsValidEmail(string emailId) {
            var clientin = new RestClient {
                BaseUrl = new Uri("https://api.mailgun.net/v3"),
                Authenticator = new HttpBasicAuthenticator("api", "pubkey-213c7aa80a0b1f9f655555081b9c81be")
            };
            var request = new RestRequest { Resource = "/address/validate" };
            request.AddParameter("address", emailId);
            var response = clientin.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed) return true;
            JToken value;
            return JObject.Parse(response.Content).TryGetValue("is_valid", out value) && value.Value<bool>();
        }

        public static EmailVerification Verify(EmailQueue item) {
            RestRequest request = new RestRequest();
            request.AddParameter("domain",
            "click2race.in", ParameterType.UrlSegment);
            request.Resource = "{domain}/events";
            request.AddParameter("event", "rejected OR failed OR delivered");
            request.AddParameter("recipient", item.EmailTo.ToLowerInvariant());
            request.AddParameter("subject", item.Subject);
            if (item.EmailDateIn != null) {
                var ist = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                var datebeginINdia = TimeZoneInfo.ConvertTime(item.EmailDateIn.Value, ist);
                var dateendINdia = TimeZoneInfo.ConvertTime(item.EmailDateIn.Value.AddHours(3), ist);
                request.AddParameter("begin", ConvertISTtoGMT(datebeginINdia).ToString("R"));
                request.AddParameter("end", ConvertISTtoGMT(dateendINdia).ToString("R"));
            }
            var response = client.Execute(request);
            JArray items;
            try {
                if (response.ResponseStatus != ResponseStatus.Completed) return EmailVerification.Retry;
                items = JObject.Parse(response.Content).Value<JArray>("items");
            } catch (Exception) {
                return EmailVerification.Retry;
            }
            if (items == null || items.Count == 0)
                return EmailVerification.Unknown;
            var events = items.Select(itemx => itemx.Value<string>("event")).ToList();
            var severity = items[0].Value<string>("severity");
            if (events.Contains("delivered"))
                return EmailVerification.Sent;
            if (events[0] == "failed" && (severity != null && severity == "temporary"))
                return EmailVerification.Retry;
            if (events[0] == "failed" && (severity != null && severity == "permanent"))
                return EmailVerification.Failed;
            return EmailVerification.Failed;
        }

        private static DateTime ConvertISTtoGMT(DateTime datetime) {
            return datetime.AddHours(-5).AddMinutes(-30);
        }
    }
}