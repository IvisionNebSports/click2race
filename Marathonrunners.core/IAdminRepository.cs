﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marathonrunners.Core.Entities;

namespace Marathonrunners.Core {
    public interface IAdminRepository {
        void AddForceEmail(ForceEmail model);
    }
}
