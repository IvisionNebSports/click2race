﻿using NHibernate;

namespace Marathonrunners.Core {
    public class AdminRespository : IAdminRepository {
        private readonly ISession _session;

        public AdminRespository(ISession session) {
            _session = session;
        }

        public void AddForceEmail(Entities.ForceEmail forceEmail) {
            using (var transaction = _session.BeginTransaction()) {
                _session.Save(forceEmail);
                transaction.Commit();
            }
        }
    }
}
