﻿using System.Collections.Generic;
using Marathonrunners.Core.Entities;

namespace Marathonrunners.Core {
    public interface IEmailRepository {
        IEnumerable<EmailQueue> GetEmailQueue();

        int QueueEmail(EmailQueue email);

        void UpdateEmailQueue(EmailQueue currentProcessingId);

        List<EmailQueue> FindByEmailId(string emailid);

        EmailQueue FindById(int emailqueueentry);

        List<EmailQueue> FindByIds(List<int> list);

        IEnumerable<EmailQueue> GetMailsWithVerifyingStatus();
    }
}
