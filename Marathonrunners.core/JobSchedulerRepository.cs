﻿using System;
using System.Linq;
using Marathonrunners.Core.Entities;
using Marathonrunners.Core.Reporting;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using System.Collections.Generic;
using NHibernate.Util;

namespace Marathonrunners.Core
{
    public class JobSchedulerRepository : IJobSchedulerRepository
    {

        private readonly ISession _session;

        public JobSchedulerRepository(ISession session)
        {
            _session = session;
        }
        public List<Report2> GetFastLaneRegisterationReportFor(int numberOfDays = 1)
        {
            using (ISession session = _session.SessionFactory.OpenSession())
            {
                var now = DateTime.Now;
                var dayBefore = now.Subtract(TimeSpan.FromDays(numberOfDays));
                var list = session.Query<UserRunRegistration>().Where(x => x.Status && x.IsFastLane && x.RegistrationDate >= dayBefore && x.RegistrationDate <= now).OrderBy(x => x.RegistrationDate);
                var listOfFastLaneIds = list.Select(x => x.FastLaneId).ToList();
                var fastLaneReg = session.Query<FastLaneRegistration>().Where(x => listOfFastLaneIds.Contains(x.TransactionId)).OrderBy(y => y.DateOfRegistration);
                list.ForEach(x => x.UserAdditionalItemSelection.ToList());
                //return new Report() {
                //    RunRegistrations = list.ToList(),
                //    FastLaneRegs = fastLaneReg.ToList()
                //};
                var runregList = list.ToList();
                var fastLaneRegList = fastLaneReg.ToList();
                List<Report2> report = new List<Report2>();
                string[] gender = new string[] { "Male", "Female", "Female" };
                for (int index = 0; index < list.Count(); index++)
                {
                    var userrunreg = runregList[index];
                    var fastLaneRegInner = fastLaneRegList[index];

                    var rep = new Report2()
                    {
                        RunRegistrationId = string.Format("C2R-{0}", userrunreg.RunRegistrationId),
                        GroupRegId = userrunreg.GroupRegId,
                        UserId = userrunreg.UserId,
                        RunCategoryId = userrunreg.RunCategoryId,
                        FirstName = fastLaneRegInner.FirstName,
                        LastName = fastLaneRegInner.LastName,
                        Gender = gender[fastLaneRegInner.Gender - 1],
                        ShirtSize = fastLaneRegInner.ShirtSize,
                        DOB = fastLaneRegInner.DOB,
                        MobileNumber = fastLaneRegInner.MobileNumber,
                        EmergencyContactName = fastLaneRegInner.EmergencyContactName,
                        EmergencyContactNumber = fastLaneRegInner.EmergencyContactNumber,
                        FinalPrice = userrunreg.FinalPrice,
                        UserAdditionalItemSelection = string.Join(",", userrunreg.UserAdditionalItemSelection.Select(x => x.Name).ToArray()),
                        BloodGroup = fastLaneRegInner.BloodGroup,
                        NameOnBib = fastLaneRegInner.NameOnBib,
                        RegistrationDate = userrunreg.RegistrationDate,
                        Status = userrunreg.Status,
                        FastLaneTransactionId = fastLaneRegInner.TransactionId
                    };
                    report.Add(rep);
                }
                return report;
            }
        }

        private static int Days(UserRunRegistration x)
        {
            return (DateTime.Now - x.RegistrationDate).Days;
        }
    }
}
