﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Marathonrunners.Core.Entities;
using NHibernate;
using NHibernate.Cache;
using NHibernate.Tool.hbm2ddl;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;

namespace Marathonrunners.Core {
    public class RepositoryModule : NinjectModule {
        public override void Load() {
            var config = MsSqlConfiguration.MsSql2012.ConnectionString(c =>
                c.FromConnectionStringWithKey("PortalConnectionString"));
            config.DefaultSchema("dbo");

            Bind<ISessionFactory>()
            .ToMethod
            (
                e =>
                    Fluently.Configure()
                    .Database(config)
                    .Cache(c => c.UseQueryCache().ProviderClass<HashtableCacheProvider>())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UserInfo>())
                    //.ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                    .BuildConfiguration()
                    .BuildSessionFactory()
            )
            .InSingletonScope();

            Bind<ISession>()
                .ToMethod((ctx) => ctx.Kernel.Get<ISessionFactory>().OpenSession())
                .InRequestScope();
        }
    }
}
