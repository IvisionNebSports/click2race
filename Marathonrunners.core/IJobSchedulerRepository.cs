﻿using System.Collections.Generic;
using Marathonrunners.Core.Reporting;

namespace Marathonrunners.Core {
    public interface IJobSchedulerRepository {

        List<Report2> GetFastLaneRegisterationReportFor(int numberOfDays);
    }
}
