﻿using System;
using System.Collections.Generic;
using System.Linq;
using Marathonrunners.Core.Entities;
using NHibernate;
using NHibernate.Linq;

namespace Marathonrunners.Core {
    public class EmailRepository : IEmailRepository {
        private ISession _session;

        public EmailRepository(ISession session) {
            _session = session;
        }

        public IEnumerable<EmailQueue> GetEmailQueue() {
            using (var session = _session.SessionFactory.OpenSession()) {
                return session.Query<EmailQueue>().Select(x => x).ToList();
            }
        }

        public int QueueEmail(EmailQueue email) {
            //var email = new EmailQueue() {
            //    EmailTo = to,
            //    EmailFrom = from,
            //    Subject = subject,
            //    Message = body,
            //    EmailDateIn = DateTime.Now,
            //    EmailType = type,
            //    EmailVerified = verified,
            //    EmailStatus = EmailStatusEnum.Queued
            //};
            int returnValue;
            using (var session = _session.SessionFactory.OpenSession()) {
                using (var tran = session.BeginTransaction()) {
                    returnValue = Convert.ToInt32(session.Save(email));
                    tran.Commit();
                }
            }
            return returnValue;
        }

        //[MethodImpl(MethodImplOptions.Synchronized)]
        public void UpdateEmailQueue(EmailQueue currentProcessingId) {
            using (var session = _session.SessionFactory.OpenSession()) {
                using (var tran = session.BeginTransaction()) {
                    session.SaveOrUpdate(currentProcessingId);
                    tran.Commit();
                }
            }
        }

        public List<EmailQueue> FindByEmailId(string emailid) {
            using (var session = _session.SessionFactory.OpenSession()) {
                return session.Query<EmailQueue>()
                    .Where(x => x.EmailTo.Equals(emailid, StringComparison.InvariantCultureIgnoreCase)
                                && x.EmailStatus == EmailStatusEnum.Queued).ToList();
            }
        }

        public EmailQueue FindById(int emailqueueentry) {
            using (var session = _session.SessionFactory.OpenSession()) {
                return session.Query<EmailQueue>().FirstOrDefault(x => x.Id == emailqueueentry);
            }
        }

        public List<EmailQueue> FindByIds(List<int> list) {
            //ValidateOrReinstantiateSession();
            using (var session = _session.SessionFactory.OpenSession()) {
                return session.Query<EmailQueue>().Where(x => list.Contains(x.Id)).ToList();
            }
        }


        //[MethodImpl(MethodImplOptions.Synchronized)]
        private Object _objectLock = new Object();
        public IEnumerable<EmailQueue> GetMailsWithVerifyingStatus() {
            using (var session = _session.SessionFactory.OpenSession()) {
                //ValidateOrReinstantiateSession();
                return
                    session.Query<EmailQueue>()
                        .Where(x => x.EmailStatus == EmailStatusEnum.Verifying)
                        .OrderByDescending(x => x.EmailDateIn)
                        .ToList();
            }
        }

        private void ValidateOrReinstantiateSession() {
            if (_session != null && !_session.IsOpen) {
                _session = _session.SessionFactory.OpenSession();
            }
        }
    }
}
