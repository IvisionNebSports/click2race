﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Marathonrunners.Core.Entities;

namespace Marathonrunners.Core.Reporting {
    public class Report {
        public IList<UserRunRegistration> RunRegistrations { get; set; }
        public IList<FastLaneRegistration> FastLaneRegs { get; set; }

    }
    
    public class Report2
    {
        public virtual string RunRegistrationId
        {
            get;
            set;
        }


      
        /// <summary>
        /// There are no comments for RunCategoryId in the schema.
        /// </summary>
        public virtual string RunCategoryId
        {
            get;
            set;
        }


        /// <summary>
        /// There are no comments for UserId in the schema.
        /// </summary>
        public virtual string UserId
        {
            get;
            set;
        }


        ///// <summary>
        ///// There are no comments for ServiceCharge in the schema.
        ///// </summary>
        //public virtual System.Nullable<double> ServiceCharge
        //{
        //    get;
        //    set;
        //}


        ///// <summary>
        ///// There are no comments for Discounts in the schema.
        ///// </summary>
        //public virtual System.Nullable<double> Discounts
        //{
        //    get;
        //    set;
        //}


        /// <summary>
        /// There are no comments for FinalPrice in the schema.
        /// </summary>
        public virtual double FinalPrice
        {
            get;
            set;
        }


        /// <summary>
        /// There are no comments for Status in the schema.
        /// </summary>
        public virtual bool Status
        {
            get;
            set;
        }


        ///// <summary>
        ///// There are no comments for BankRefNumber in the schema.
        ///// </summary>
        //public virtual string BankRefNumber
        //{
        //    get;
        //    set;
        //}


        ///// <summary>
        ///// There are no comments for AmountSplit in the schema.
        ///// </summary>
        //public virtual string AmountSplit
        //{
        //    get;
        //    set;
        //}


        ///// <summary>
        ///// There are no comments for PayUMoneyId in the schema.
        ///// </summary>
        //public virtual string PayUMoneyId
        //{
        //    get;
        //    set;
        //}


        /// <summary>
        /// There are no comments for RegistrationDate in the schema.
        /// </summary>
        public virtual System.DateTime RegistrationDate
        {
            get;
            set;
        }


        ///// <summary>
        ///// There are no comments for ForceEmail in the schema.
        ///// </summary>
        //public virtual bool ForceEmail
        //{
        //    get;
        //    set;
        //}


        ///// <summary>
        ///// There are no comments for Student in the schema.
        ///// </summary>
        //public virtual bool Student
        //{
        //    get;
        //    set;
        //}

        //public virtual double? Donation
        //{
        //    get;
        //    set;
        //}

        //public virtual string DonationTo
        //{
        //    get;
        //    set;
        //}

        //public virtual bool IsFastLane
        //{
        //    get;
        //    set;
        //}

        //public virtual string FastLaneId
        //{
        //    get;
        //    set;
        //}

        //public virtual bool IsGroupReg
        //{
        //    get;
        //    set;
        //}

        public virtual string GroupRegId
        {
            get;
            set;
        }

        //public virtual string PromoCode
        //{
        //    get;
        //    set;
        //}

        //public virtual string SubCategory
        //{
        //    get;
        //    set;
        //}

        //public virtual string ExtraField1
        //{
        //    get;
        //    set;
        //}

        //public virtual string BibCollectionLocation
        //{
        //    get;
        //    set;
        //}

        public virtual string UserAdditionalItemSelection { get; set; }

        public virtual string FastLaneTransactionId
        {
            get;
            set;
        }


        ///// <summary>
        ///// There are no comments for EmailId in the schema.
        ///// </summary>
        //public virtual string EmailId
        //{
        //    get;
        //    set;
        //}


        /// <summary>
        /// There are no comments for FirstName in the schema.
        /// </summary>
        public virtual string FirstName
        {
            get;
            set;
        }


        /// <summary>
        /// There are no comments for LastName in the schema.
        /// </summary>
        public virtual string LastName
        {
            get;
            set;
        }


        ///// <summary>
        ///// There are no comments for Category in the schema.
        ///// </summary>
        //public virtual string Category
        //{
        //    get;
        //    set;
        //}


        /// <summary>
        /// There are no comments for MobileNumber in the schema.
        /// </summary>
        public virtual string MobileNumber
        {
            get;
            set;
        }


        ///// <summary>
        ///// There are no comments for Address in the schema.
        ///// </summary>
        //public virtual string Address
        //{
        //    get;
        //    set;
        //}


        ///// <summary>
        ///// There are no comments for Amount in the schema.
        ///// </summary>
        //public virtual string Amount
        //{
        //    get;
        //    set;
        //}


     
        /// <summary>
        /// There are no comments for Gender in the schema.
        /// </summary>
        public virtual string Gender
        {
            get;
            set;
        }


        ///// <summary>
        ///// There are no comments for DateOfRegistration in the schema.
        ///// </summary>
        //public virtual System.DateTime DateOfRegistration
        //{
        //    get;
        //    set;
        //}

        public virtual System.DateTime DOB
        {
            get;
            set;
        }

        public virtual string ShirtSize
        {
            get;
            set;
        }

        public virtual string BloodGroup
        {
            get;
            set;
        }

        public virtual long EmergencyContactNumber
        {
            get;
            set;
        }

        //public virtual string Organization
        //{
        //    get;
        //    set;
        //}

        //public virtual bool IsDonation
        //{
        //    get;
        //    set;
        //}

        public virtual string NameOnBib
        {
            get;
            set;
        }

        public virtual string EmergencyContactName { get; set; }
       
    }
}
