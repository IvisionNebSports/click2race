﻿using System.Collections.Generic;
using Marathonrunners.Core.Entities;

namespace Marathonrunners.Core {
    public interface IAccountRepository {
        void RegisterUser(UserInfo user);
        void RegisterVerificationData(UserTempData userTempData);
        void ActivateUserAccount(string emailid, bool flag);

        bool IsExistingUser(string p);

        UserTempData GetUserIdFromActivationCode(string activationcode);

        UserInfo GetUserById(string userid);

        bool ActivationCodeAlreadySent(string emailAddress);

        bool DeleteTempData(string activationcode);

        void UpdateImage(string field, string id, string imageUrl);

        void UpdateField(string field, string value, string id, int type);

        void UpdateUserLocation(IList<string> values, int postalcode, string id);

        void RegisterUserRun(UserRunRegistration runRegistration);

        int RegisterUserRunEx(UserRunRegistration runRegistration);

        UserRunRegistration GetRunRegistrationByTransactionId(string transactionId);

        void UpdateRunRegistration(UserRunRegistration userRunRegistration);

        bool IsDuplicateRegistration(string user, string generatedId);

        void RegisterRFLKTMRun(RFLKTMUserRegistration ktmrfluser);

        RFLKTMUserRegistration GetRFLKTMUserRegistrationByTransactionId(string transactionId);

        void UpdateRFLKTMUserRegistration(RFLKTMUserRegistration userRunRegistration);

        IList<UserRunRegistration> GetRegistrationsOf(string emailId);
        IList<RFLKTMUserRegistration> GetKTMRegistrationsOf(string emailId);

        void RegisterUserRun(RandonneurRegistrations runReg);
        RandonneurRegistrations GetFastLaneRunRegistrationByTransactionId(string transactionId);
        FastLaneRegistration GetFastLaneRegistrationByTransactionId(string transactionId);
        void UpdateFastLaneRunRegistration(RandonneurRegistrations userRunRegistration);

        void DonationEntry(DonationTransaction donation);
        DonationTransaction GetDonationEntryByTransactionId(string transactionId);

        void UpdateDonationEntry(DonationTransaction donationTransaction);

        void RegisterUserRun(FastLaneRegistration fastLaneReg);

        void UpdateFastLaneRegistration(FastLaneRegistration userRunRegistration);

        void RegisterContourUserRun(ContoursRunRegistration userRegistration);
        ContoursRunRegistration GetContourUserRegistrationByTransactionId(string transactionId);

        void UpdateContourUserRegistration(ContoursRunRegistration userRunRegistration);
        IList<ContoursRunRegistration> GetContourRegistrationsOf(string name);

        int RegisterBM2015UserRun(BMUserRunRegistration2015 runreg);

        BMUserRunRegistration2015 GetBM2015UserRegistrationByTransactionId(int transactionId);

        void UpdateBM2015UserRegistration(BMUserRunRegistration2015 userRunRegistration);

        void UpdateTransactionStatus(string transactionId, string amountsplit, string payumoneyId, string bankrefnum,
            bool forcemail = false);

        void UpdateBM2015UserTransactionStatus(string transactionId, string amountsplit, string payumoneyId, string bankrefNum, bool forceEmail = false);

        bool AlreadyRegisteredBM2015PromoRun(BMUserRunRegistration2015 runreg);

        IEnumerable<BMUserRunRegistration2015> GetBM2015RegistrationsOf(string emaild);

        void AddOrUpdateTimingData(RunTiming timingData);

        BMUserRunRegistration2015 GetBMUserRunRegistration2015ByTransactionId(string transactionId);

        void RegisterFastLane(FastLaneRegistration reg);
        bool IsDuplicateRegistrationBM2015(string name, string generatedRunCategoryId);
        bool IsMultipleRegistrationForFastLane(string emailId, string firstname, string lastname, string eventId);
        bool IsMultipleRegistrationForRegisteredUser(string emailId, string firstname, string lastname, string eventId);
        int RegisterUserRunMultiReg(List<UserRunRegistration> runRegs, List<FastLaneRegistration> fastLaneRegs);
        bool IsDuplicateRegistrationFastLane(string userid, string eventId, string eventTitle);
    }
}
