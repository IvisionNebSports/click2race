﻿namespace Marathonrunners.Core.Entities {

    public enum EmailTypeEnum {
        AccountActivation = 1,
        RegistrationConfirmation = 2,
        ForceEmail = 3,
        PromoMailer = 4,
        ErrorMail = 5
    }

    public enum EmailStatusEnum {
        Queued = 1,
        Sent = 2,
        Invalid = 3,
        Failed = 4,
        Verifying = 5,
        Unknown = 6
    }
}
