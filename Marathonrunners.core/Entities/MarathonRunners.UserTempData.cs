﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 4/11/2015 3:05:30 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Marathonrunners.Core.Entities
{

    /// <summary>
    /// There are no comments for Marathonrunners.Core.Entities.UserTempData, Marathonrunners.Core in the schema.
    /// </summary>
    public partial class UserTempData {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for UserTempData constructor in the schema.
        /// </summary>
        public UserTempData()
        {
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        public virtual string Id
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for UserId in the schema.
        /// </summary>
        public virtual string UserId
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for DateOfRegister in the schema.
        /// </summary>
        public virtual System.DateTime DateOfRegister
        {
            get;
            set;
        }
    }

}
