﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 4/11/2015 3:05:31 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using FluentNHibernate.Mapping;
using FluentNHibernate.MappingModel.Collections;

namespace Marathonrunners.Core.Entities
{
    /// <summary>
    /// There are no comments for ForceEmailMap in the schema.
    /// </summary>
    public partial class ForceEmailMap : ClassMap<ForceEmail>
    {
        /// <summary>
        /// There are no comments for ForceEmailMap constructor in the schema.
        /// </summary>
        public ForceEmailMap()
        {
              Table(@"ForceEmails");
              LazyLoad();
              Id(x => x.Id)
                .Column("Id")
                .CustomType("Int64")
                .Access.Property()                
                .GeneratedBy.Identity();
              Map(x => x.EmailId)    
                .Column("EmailId")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.EventName)    
                .Column("EventName")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Amount)    
                .Column("Amount")
                .CustomType("Double")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Mobile)    
                .Column("Mobile")
                .CustomType("Int64")
                .Access.Property()
                .Generated.Never();
              Map(x => x.TransactionId)    
                .Column("TransactionId")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.DateAndTime)    
                .Column("DateAndTime")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never();
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
