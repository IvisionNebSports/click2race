﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 4/11/2015 3:05:31 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using FluentNHibernate.Mapping;
using FluentNHibernate.MappingModel.Collections;

namespace Marathonrunners.Core.Entities
{
    /// <summary>
    /// There are no comments for UserInfoMap in the schema.
    /// </summary>
    public partial class UserInfoMap : ClassMap<UserInfo>
    {
        /// <summary>
        /// There are no comments for UserInfoMap constructor in the schema.
        /// </summary>
        public UserInfoMap()
        {
              Schema(@"dbo");
              Table(@"UserInfo");
              DynamicUpdate();
              LazyLoad();
              Id(x => x.EmailId)
                .Column("EmailId")
                .CustomType("String")
                .Access.Property()
                .GeneratedBy.Assigned();
              Map(x => x.FirstName)    
                .Column("FirstName")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.LastName)    
                .Column("LastName")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Gender)    
                .Column("Gender")
                .CustomType("Int32")
                .Access.Property()
                .Generated.Never();
              Map(x => x.DOB)    
                .Column("DOB")
                .CustomType("Date")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Nationaility)    
                .Column("Nationaility")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Country)    
                .Column("Country")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.State)    
                .Column("State")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.City)    
                .Column("City")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Pincode)    
                .Column("Pincode")
                .CustomType("Int32")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Address)    
                .Column("Address")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.MobileNumber)    
                .Column("MobileNumber")
                .CustomType("Int64")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Occupation)    
                .Column("Occupation")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.BloodGroup)    
                .Column("BloodGroup")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.ShirtSzie)    
                .Column("ShirtSzie")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.IdentityProof)    
                .Column("IdentityProof")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.IdNumber)    
                .Column("IdNumber")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.PhotoIdProof)    
                .Column("PhotoIdProof")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.UserPhoto)    
                .Column("UserPhoto")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.RunningClub)    
                .Column("RunningClub")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.EmergencyContactName)    
                .Column("EmergencyContactName")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.EmergencyContactNumber)    
                .Column("EmergencyContactNumber")
                .CustomType("Int64")
                .Access.Property()
                .Generated.Never();
              Map(x => x.Active)    
                .Column("Active")
                .CustomType("Boolean")
                .Access.Property()
                .Generated.Never();
              Map(x => x.DateOfRegister)    
                .Column("DateOfRegister")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never();
              Map(x => x.ChronicDisease)    
                .Column("ChronicDisease")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              Map(x => x.NameOnBib)    
                .Column("NameOnBib")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
