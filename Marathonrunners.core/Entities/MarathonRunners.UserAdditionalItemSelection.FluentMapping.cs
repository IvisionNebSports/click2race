﻿using FluentNHibernate.Mapping;

namespace Marathonrunners.Core.Entities {
    public partial class UserAdditionalItemSelectionMap : ClassMap<UserAdditionalItemSelection> {


        public UserAdditionalItemSelectionMap() {
            Table(@"UserAdditionalItemSelection");
            LazyLoad();
            Id(x => x.Id)
              .Column("Id")
              .CustomType<int>()
              .Access.Property()
              .GeneratedBy.Identity();
            Map(x => x.Name)
               .Column("Name")
               .CustomType<string>()
               .Access.Property()
               .Generated.Never();
            Map(x => x.RunCategory)
               .Column("RunCategory")
               .CustomType<string>()
               .Access.Property()
               .Generated.Never();
            Map(x => x.Quantity)
              .Column("Quantity")
              .CustomType<int>()
              .Access.Property()
              .Generated.Never();
            //Map(x => x.RunRegistrationId)
            // .Column("RunRegistrationId").CustomType<int>()
            // .Access.Property()
            // .Generated.Never();
            References(x => x.RunRegistrationId).Column("RunRegistrationId");
            ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }
}
