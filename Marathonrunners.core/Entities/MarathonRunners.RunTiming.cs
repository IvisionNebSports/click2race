﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 4/11/2015 11:11:10 AM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Marathonrunners.Core.Entities
{

    /// <summary>
    /// There are no comments for Marathonrunners.Core.Entities.RunTiming, Marathonrunners.Core in the schema.
    /// </summary>
    public partial class RunTiming {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for RunTiming constructor in the schema.
        /// </summary>
        public RunTiming()
        {
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for UserId in the schema.
        /// </summary>
        public virtual string UserId
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for BestRunTiming in the schema.
        /// </summary>
        public virtual TimeSpan? BestRunTiming
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for TimingHyperLink in the schema.
        /// </summary>
        public virtual string TimingHyperLink
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for DistanceInKm in the schema.
        /// </summary>
        public virtual System.Nullable<double> DistanceInKm
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for EventName in the schema.
        /// </summary>
        public virtual string EventName
        {
            get;
            set;
        }
    }

}
