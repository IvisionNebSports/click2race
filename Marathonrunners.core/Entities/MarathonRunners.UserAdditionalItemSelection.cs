﻿namespace Marathonrunners.Core.Entities {
    public partial class UserAdditionalItemSelection {
        #region Extensibility Method Definitions

        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();

        #endregion

        public UserAdditionalItemSelection() {
            OnCreated();
        }

        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string RunCategory { get; set; }

        public virtual int Quantity { get; set; }

        public virtual UserRunRegistration RunRegistrationId { get; set; }
    }
}
