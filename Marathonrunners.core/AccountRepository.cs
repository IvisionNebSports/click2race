﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using FluentNHibernate.Conventions;
using Marathonrunners.Core.Entities;
using NHibernate;
using NHibernate.Linq;

namespace Marathonrunners.Core {
    public class AccountRepository : IAccountRepository {

        private readonly ISession _session;

        public AccountRepository(ISession session) {
            _session = session;
        }

        public void RegisterUser(UserInfo user) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                session.Save(user);
                tran.Commit();
            }
        }

        public void RegisterVerificationData(UserTempData userTempData) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                session.Save(userTempData);
                tran.Commit();
            }
        }

        public void ActivateUserAccount(string emailid, bool flag) {
            throw new NotImplementedException();
        }

        public bool IsExistingUser(string emailid) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<UserInfo>().Any(x => x.EmailId == emailid);
        }

        public UserTempData GetUserIdFromActivationCode(string activationcode) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<UserTempData>().FirstOrDefault(x => x.Id == activationcode);
        }

        public UserInfo GetUserById(string userid) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<UserInfo>().FirstOrDefault(x => x.EmailId == userid);
        }

        public bool ActivationCodeAlreadySent(string emailAddress) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<UserTempData>().Any(x => x.UserId == emailAddress.Trim());
        }

        public bool DeleteTempData(string activationcode) {
            var success = 0;
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                var queryString = string.Format("delete {0} where id = :id", typeof(UserTempData));
                success = session.CreateQuery(queryString)
                       .SetParameter("id", activationcode)
                       .ExecuteUpdate();

                transaction.Commit();
            }
            return success == 1;
        }

        public void UpdateImage(string field, string id, string imageUrl) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                var queryString = string.Format("update {0} set {1}='{2}' where EmailId = :id", typeof(UserInfo), field, imageUrl);
                session.CreateQuery(queryString)
                       .SetParameter("id", id)
                       .ExecuteUpdate();
                transaction.Commit();
            }
        }

        public void UpdateField(string field, string value, string id, int type) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                var queryString = "";
                switch (type) {
                    /*Integer*/
                    case 1: queryString = string.Format("update {0} set {1}={2} where EmailId = :id", typeof(UserInfo), field, value); break;
                    /*String*/
                    case 3: queryString = string.Format("update {0} set {1}='{2}' where EmailId = :id", typeof(UserInfo), field, value); break;
                    /*Date*/
                    case 4: queryString = string.Format("update {0} set {1}=CAST('{2}' AS DATETIME) where EmailId = :id", typeof(UserInfo), field, value); break;
                }
                session.CreateQuery(queryString)
                       .SetParameter("id", id)
                       .ExecuteUpdate();
                transaction.Commit();
            }
        }

        public void UpdateUserLocation(IList<string> values, int postalcode, string id) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                var queryString = string.Format("update {0} set Country='{1}', State='{2}', City='{3}', Pincode={4} where EmailId = :id",
                    typeof(UserInfo), values[0], values[1], values[2], postalcode);
                session.CreateQuery(queryString)
                       .SetParameter("id", id)
                       .ExecuteUpdate();
                transaction.Commit();
            }
        }

        public void RegisterUserRun(UserRunRegistration runRegistration) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Save(runRegistration);
                transaction.Commit();
            }
        }

        public int RegisterUserRunEx(UserRunRegistration runRegistration) {
            int returnValue;
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                returnValue = Convert.ToInt32(session.Save(runRegistration));
                tran.Commit();
            }
            return returnValue;
        }

        public UserRunRegistration GetRunRegistrationByTransactionId(string transactionId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<UserRunRegistration>().FirstOrDefault(x => x.RunRegistrationId == Convert.ToInt32(transactionId.Substring("C2R-".Length)));
        }

        public void UpdateRunRegistration(UserRunRegistration userRunRegistration) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Update(userRunRegistration);
                transaction.Commit();
            }
        }

        public bool IsDuplicateRegistration(string user, string generatedRunCategoryId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<UserRunRegistration>().Any(x => x.UserId == user && x.RunCategoryId == generatedRunCategoryId && x.Status);
        }

        public void RegisterRFLKTMRun(RFLKTMUserRegistration ktmrfluser) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Save(ktmrfluser);
                transaction.Commit();
            }
        }

        public RFLKTMUserRegistration GetRFLKTMUserRegistrationByTransactionId(string transactionId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<RFLKTMUserRegistration>().FirstOrDefault(x => x.TransactionId == transactionId);
        }

        public void UpdateRFLKTMUserRegistration(RFLKTMUserRegistration userRunRegistration) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Update(userRunRegistration);
                transaction.Commit();
            }
        }

        public IList<UserRunRegistration> GetRegistrationsOf(string emailId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<UserRunRegistration>()
                .Where(x => x.UserId == emailId && (x.Status || x.ForceEmail))
                .ToList();
        }

        public IList<RFLKTMUserRegistration> GetKTMRegistrationsOf(string emailId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<RFLKTMUserRegistration>()
                .Where(x => x.UserId == emailId && (x.Status || x.ForceEmail))
                .ToList();
        }

        public void RegisterUserRun(RandonneurRegistrations runReg) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Save(runReg);
                transaction.Commit();
            }
        }

        public RandonneurRegistrations GetFastLaneRunRegistrationByTransactionId(string transactionId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<RandonneurRegistrations>().FirstOrDefault(x => x.TransactionId == transactionId);
        }

        public FastLaneRegistration GetFastLaneRegistrationByTransactionId(string transactionId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<FastLaneRegistration>().FirstOrDefault(x => x.TransactionId == transactionId);
        }

        public void UpdateFastLaneRunRegistration(RandonneurRegistrations userRunRegistration) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Update(userRunRegistration);
                transaction.Commit();
            }
        }

        public void DonationEntry(DonationTransaction donation) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Save(donation);
                transaction.Commit();
            }
        }

        public DonationTransaction GetDonationEntryByTransactionId(string transactionId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<DonationTransaction>().FirstOrDefault(x => x.TransactionId == transactionId);
        }

        public void UpdateDonationEntry(DonationTransaction donationTransaction) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Update(donationTransaction);
                transaction.Commit();
            }
        }

        public void RegisterUserRun(FastLaneRegistration fastLaneReg) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Save(fastLaneReg);
                transaction.Commit();
            }
        }

        public void UpdateFastLaneRegistration(FastLaneRegistration userRunRegistration) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Update(userRunRegistration);
                transaction.Commit();
            }
        }

        public void RegisterContourUserRun(ContoursRunRegistration userRegistration) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Save(userRegistration);
                transaction.Commit();
            }
        }

        public ContoursRunRegistration GetContourUserRegistrationByTransactionId(string transactionId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<ContoursRunRegistration>().FirstOrDefault(x => x.TransactionId == transactionId);
        }

        public void UpdateContourUserRegistration(ContoursRunRegistration contoursRunRegistration) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction()) {
                session.Update(contoursRunRegistration);
                transaction.Commit();
            }
        }

        IList<ContoursRunRegistration> IAccountRepository.GetContourRegistrationsOf(string name) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<ContoursRunRegistration>().Where(x => x.UserId == name && (x.Status || x.ForceEmail)).ToList();
        }

        public int RegisterBM2015UserRun(BMUserRunRegistration2015 runreg) {
            int returnValue;
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                returnValue = Convert.ToInt32(session.Save(runreg));
                tran.Commit();
            }
            return returnValue;
        }

        public BMUserRunRegistration2015 GetBM2015UserRegistrationByTransactionId(int transactionId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<BMUserRunRegistration2015>().FirstOrDefault(x => x.TransactionId == transactionId);
        }

        public void UpdateBM2015UserRegistration(BMUserRunRegistration2015 userRunRegistration) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                session.Update(userRunRegistration);
                tran.Commit();
            }
        }

        public void UpdateTransactionStatus(string transactionId, string amountsplit, string payumoneyId, string bankrefnum, bool forcemail = false) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                int runregId = Convert.ToInt32(transactionId.Substring("C2R-".Length));
                var reg = session.Query<UserRunRegistration>().FirstOrDefault(x => x.RunRegistrationId == runregId);
                List<UserRunRegistration> groupregs = new List<UserRunRegistration>();
                reg.AmountSplit = amountsplit;
                reg.Status = true;
                reg.PayUMoneyId = payumoneyId;
                reg.BankRefNumber = bankrefnum;
                reg.ForceEmail = forcemail;
                session.SaveOrUpdate(reg);
                if (reg.IsFastLane) {
                    var fastLaneReg =
                        session.Query<FastLaneRegistration>().FirstOrDefault(x => x.TransactionId == reg.FastLaneId);
                    fastLaneReg.AmountSplit = amountsplit;
                    fastLaneReg.Status = true;
                    fastLaneReg.PayUMoneyId = payumoneyId;
                    fastLaneReg.BankRefNumber = bankrefnum;
                    session.SaveOrUpdate(fastLaneReg);
                }

                if (reg.IsGroupReg) {
                    groupregs =
                        session.Query<UserRunRegistration>()
                            .Where(x => x.GroupRegId == reg.GroupRegId && x.RunRegistrationId != reg.RunRegistrationId)
                            .ToList();
                }
                foreach (var regin in groupregs) {
                    regin.AmountSplit = amountsplit;
                    regin.Status = true;
                    regin.PayUMoneyId = payumoneyId;
                    regin.BankRefNumber = bankrefnum;
                    regin.ForceEmail = forcemail;
                    session.SaveOrUpdate(regin);
                    if (regin.IsFastLane) {
                        var fastLaneReg =
                        session.Query<FastLaneRegistration>().FirstOrDefault(x => x.TransactionId == regin.FastLaneId);
                        fastLaneReg.AmountSplit = amountsplit;
                        fastLaneReg.Status = true;
                        fastLaneReg.PayUMoneyId = payumoneyId;
                        fastLaneReg.BankRefNumber = bankrefnum;
                        session.SaveOrUpdate(fastLaneReg);
                    }
                }
                tran.Commit();
            }
        }

        public void UpdateBM2015UserTransactionStatus(string transactionId, string amountsplit, string payumoneyId, string bankrefNum, bool forceEmail = false) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                var query = session.CreateQuery(string.Format(
                    "UPDATE BMUserRunRegistration2015 SET" +
                    " Status = 1," +
                    "BankRefNumber = '{1}'," +
                    "AmountSplit = '{0}'," +
                    "PayUMoneyId = '{2}'," +
                    "ForceEmail = {3} WHERE TransactionId = {4}", bankrefNum, amountsplit, payumoneyId, forceEmail, Convert.ToInt32(transactionId.Substring("C2R-".Length))));
                query.ExecuteUpdate();
                tran.Commit();
            }
        }

        public bool AlreadyRegisteredBM2015PromoRun(BMUserRunRegistration2015 runreg) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<BMUserRunRegistration2015>().Any(x => x.UserId == runreg.UserId && x.RunCategoryId == runreg.RunCategoryId);
        }

        public IEnumerable<BMUserRunRegistration2015> GetBM2015RegistrationsOf(string emaild) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<BMUserRunRegistration2015>().Where(x => x.UserId == emaild && (x.Status || x.ForceEmail)).ToList();
        }

        public void AddOrUpdateTimingData(RunTiming timingData) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                session.SaveOrUpdate(timingData);
                tran.Commit();
            }
        }

        public BMUserRunRegistration2015 GetBMUserRunRegistration2015ByTransactionId(string transactionId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<BMUserRunRegistration2015>().FirstOrDefault(x => x.TransactionId == Convert.ToInt32(transactionId.Substring("C2R-".Length)));
        }

        public void RegisterFastLane(FastLaneRegistration reg) {
            using (var session = _session.SessionFactory.OpenSession())
            using (var tran = session.BeginTransaction()) {
                session.SaveOrUpdate(reg);
                tran.Commit();
            }
        }

        public bool IsDuplicateRegistrationBM2015(string user, string generatedRunCategoryId) {
            using (var session = _session.SessionFactory.OpenSession())
                return session.Query<BMUserRunRegistration2015>().Any(x => x.UserId == user && x.RunCategoryId == generatedRunCategoryId && x.Status);
        }

        public bool IsMultipleRegistrationForFastLane(string emailId, string firstname, string lastname, string eventTitle) {
            using (var session = _session.SessionFactory.OpenSession()) {
                var q = session.Query<UserRunRegistration>().Where(x => x.UserId == emailId && x.RunCategoryId.Contains(eventTitle) && x.Status && x.IsFastLane).ToList();
                return q.Count != 0 && q.Any(v1 => session.Query<FastLaneRegistration>().Any(x => x.TransactionId == v1.FastLaneId && x.FirstName == firstname && x.LastName == lastname));
            }
        }

        public bool IsMultipleRegistrationForRegisteredUser(string emailId, string firstname, string lastname, string eventTitle) {
            using (var session = _session.SessionFactory.OpenSession()) {
                var q = session.Query<UserRunRegistration>().Where(x => x.UserId == emailId && x.RunCategoryId.Contains(eventTitle) && x.Status && !x.IsFastLane).ToList();
                return q.Count != 0 && q.Any(v1 => session.Query<UserInfo>().Any(x => x.EmailId == v1.UserId));
            }
        }

        public int RegisterUserRunMultiReg(List<UserRunRegistration> runRegs, List<FastLaneRegistration> fastLaneRegs) {
            int returnValue = 0;
            using (var session = _session.SessionFactory.OpenSession()) {
                using (var tran = session.BeginTransaction()) {
                    foreach (var run in runRegs) {
                        if (returnValue == 0) {
                            returnValue = Convert.ToInt32(session.Save(run));
                        } else {
                            session.Save(run);
                        }
                    }
                    foreach (var fastLane in fastLaneRegs) {
                        session.Save(fastLane);
                    }
                    tran.Commit();
                }

            }
            return returnValue;
        }

        public bool IsDuplicateRegistrationFastLane(string userid, string eventId, string eventTitle) {
            using (var session = _session.SessionFactory.OpenSession()) {
                return session.Query<FastLaneRegistration>().Any(x => x.EmailId == userid && x.Category.Contains(eventTitle));
            }
        }
    }
}
